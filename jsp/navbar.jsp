<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="service/defines.jsp"%>

<!DOCTYPE html>

<html>

<div id="gshadow" style="display: none; position: absolute; width: 100%; height: 1000%;
	background-color: #001111; opacity: 0.90; z-index: 3333"></div>

<nav class="navbar navbar-default testerNav" role="navigation" style="margin-bottom: -20px; z-index: 4444;">
	<div class="container-fluid">
		<ul class="nav navbar-nav navbar-left">
			<c:choose>
				<c:when test="${sessionScope.user ne null && sessionScope.startingTime eq null}">
					<li><a style="margin-left: -20px; margin-right: -15px;" href="menu?id=1">
						На главную</a></li>
				</c:when>
			</c:choose>
		</ul>
		<%--<ul class="nav navbar-nav navbar-left">--%>
			<%--<li><a style="margin-left: 0" href="http://www.mius.info">MIUS</a></li>--%>
		<%--</ul>--%>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<c:choose>
			<c:when test="${sessionScope.user ne null && sessionScope.startingTime eq null}">
				<ul class="nav navbar-nav navbar-right">
					<c:choose>
						<c:when test="${sessionScope.isModer}">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
								   aria-expanded="false"><i class="glyphicon glyphicon-th-list"></i> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu" id="navbarHeaderMenu1">
									<!--
									<form class="navbar-form navbar-center" action="editUser" method="get">
										<button type="submit" name="newUser" style="border: 0px;
										background-color: #ffffff;">Add New User</button>
									</form>-->
									<li><a href="questionlist">Управление Тестами</a></li>
									<li><a href="questionlist?testId=1">Управление Вопросами</a></li>
									<li><a href="#">Статистика по Тестам</a></li>
									<li><a href="#">Статистика по Вопросам</a></li>
									<li><a href="#">Помощь</a></li>
								</ul>
							</li>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${sessionScope.isAdmin}">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
								   aria-expanded="false"><i class="glyphicon glyphicon-star"></i> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu" id="navbarHeaderMenu2">
									<!--
									<form class="navbar-form navbar-center" action="editUser" method="get">
										<button type="submit" name="newUser" style="border: 0px;
										background-color: #ffffff;">Add New User</button>
									</form>-->
									<li><a href="#">Добавление/Редактирование Пользователей</a></li>
									<!--<li class="divider"></li>-->
									<li><a href="#">...</a></li>
									<li><a href="#">...</a></li>
									<li><a href="#">...</a></li>
								</ul>
							</li>
						</c:when>
					</c:choose>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
						   aria-expanded="false">${sessionScope.user.login} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" id="navbarHeaderMenu3">
							<li><a href="editUser">Настройки Аккаунта</a></li>
							<li><a href="showPersonalStats">Мои Результаты</a></li>
							<li class="divider"></li>
							<li><a href="logout" class="logoutBtn" style="font-weight: bold;">Выйти из системы</a></li>
					<%--	<form class="navbar-form navbar-center" action="logout" method="get">
								<button type="submit" class="logoutBtn"></button>
							</form>
					--%>
						</ul>
					</li>
				</ul>
			</c:when>
		</c:choose>
		</div>
	</div>
</nav>


<ul id="timerNav" class="nav navbar-nav timerNav" style="z-index: 6666;">
	<li><span id="timer"></span><li>
</ul>



<nav style="margin-bottom: -15px; z-index: 4444;" class="navbar navbar-default navbar-fixed-bottom">
	<div class="container-fluid">
		<div class="navbar-header">
			<div style="margin-top: 5px;">
				<%-- Не чистить следующие 3 строчки! (в жопу code-convention)--%>
				<text class="navbarCopyright" title=
"Тестирующая система Gint.
  Гомель, 2015г.">Gint &copy;</text> 2015
			</div>
		</div>
		<ul class="nav navbar-nav navbar-right">
			<div style="margin-top: 5px; margin-right: 10px;">
				<a class="navbarLink" href="https://www.google.by/?gws_rd=ssl#q=%D1%81%D1%81%D1%8B%D0%BB%D0%BE%D1%87%D0%BA%D0%B0+%D0%BD%D0%B0+%D0%B3%D0%B8%D1%82"
						target="_blank">
					Open-source
				</a>
				<span>project</span>
			</div>
		</ul>
		<div class="nav navbar-nav navbar-right" style="width: 50%;">
			<div style="margin-top: 5px; text-align: left;">
				<a href="mailto:gint@tut.by" class="navbarLink">Contact us:</a>
				<span>gint@tut.by</span>
                </div>
            </div>
        </div>
    </nav>

    <head>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/tester.css" />

        <script type="text/javascript" src="js/tester/timerDown.js"></script>
        <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.session.js"></script>
        <style>
            .navbar-fixed-bottom {
                margin-bottom: -10%;
            }
            .navbar-default {
                max-height: 5%;
            }

        </style>
    </head>
    <body onload="time();">
        <%--<script>alert("${sessionScope.currentTime - sessionScope.startingTime}");</script>--%>
	<input type="hidden" id="timeLeft" value="${
			sessionScope.exam.testTime -
			sessionScope.currentTime / 1000
	 		+ sessionScope.exam.startingTime / 1000}"/>

</body>
</html>


<!-- end of navbar -->

