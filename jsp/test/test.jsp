<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>


<html>
<head>
	<script type="text/javascript"
			src="js/tester/edit-questions/edit-question.js"></script>
	<script type="text/javascript" src="js/tester/uploads.js"></script>
	<c:choose>
		<c:when test="${requestScope.testType eq 'OneOfSeveral'}">
			<%@ include file="/jsp/test/questions/showQuestion.jsp"%>
			<%@ include file="/jsp/test/questions/oneOfSeveralQuestion.jsp"%>
		</c:when>
		<c:when test="${requestScope.testType eq 'Multiple'}">
			<%@ include file="/jsp/test/questions/showQuestion.jsp"%>
			<%@ include file="/jsp/test/questions/multipleQuestion.jsp"%>
		</c:when>
		<c:when test="${requestScope.testType eq 'String'}">
			<%@ include file="/jsp/test/questions/showQuestion.jsp"%>
			<%@ include file="/jsp/test/questions/stringQuestion.jsp"%>
		</c:when>
		<c:when test="${requestScope.testType eq 'Correlate'}">
			<%@ include file="/jsp/test/questions/showQuestion.jsp"%>
			<%@ include file="/jsp/test/questions/correlateQuestion.jsp"%>
		</c:when>
		<c:otherwise>
			<%@ include file="/jsp/test/testStart.jsp"%>
		</c:otherwise>
	</c:choose>
	<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.session.js"></script>
    <title>Tester</title>
</head>

<body class="testerBody">
	<br/><br/>

	<input type="hidden" id="pageId" value="testPage"/>
</body>
</html>


