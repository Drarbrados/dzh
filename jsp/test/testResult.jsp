<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>
<html>
<head>
	<link rel="stylesheet" href="../../css/bootstrap.css" />
	<link rel="stylesheet" href="../css/glass_buttons_2.css" />
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/clr.js"></script>
    <title>Tester</title>
</head>

<body class="testerBody">
	<center>
		<br/>
		<div style="margin: 15px;"></div>
		<h1 class="font ${pageColor}">
			<c:out value="${sessionScope.user.fname} ${sessionScope.user.lname}"/></h1>

		<div style="margin: -15px;"></div>	
	
		<c:set var="t" value="${requestScope.titleTest}" />		
		<h2><a href="menu?id=${t.parentID}">${t.title}</a></h2>

		<c:choose>
			<c:when test="${exam.training == true}">
				<c:set var="mode" value="training"/>
			</c:when>
			<c:otherwise>
				<c:set var="mode" value="test"/>
			</c:otherwise>
		</c:choose>

		<h2>Тест завершён с оценкой ${exam.mark} в режиме [${mode}]</h2>
	
		<form action="testResult" method="post">
			<h3>
				<button style="width: 100; margin: 2px; padding: 3; padding-left: 7; padding-right: 7;"
					class="btn btn-default btn-lg" type="submit" name="back" value="">назад</button><br/>
			</h3>
		</form>

		<br/>
	</center>

	<input type="hidden" id="pageId" value="testresultPage"/>
</body>
</html>

