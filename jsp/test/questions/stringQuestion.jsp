<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
	<script type="text/javascript"
			src="js/tester/edit-questions/edit-string-question.js"></script>
</head>

<body>

	<script>
		updatePreview();
		var cells = document.getElementsByTagName("td");
		for(var i = 0; i < cells.length; i++) {
			cells[i].style.border = "none";
		}
		changeImageSize("imageWidth");
		function answer() {
			var answer = document.getElementById("textInput");
			document.getElementById("trueComb1").value = answer.value;

			if(answer.value != "") {
				document.forms['answerForm'].submit();
			} else {
				alert('Напишите ответ!');
			}
		}
		fixColumnHeightsInPreview();
	</script>



	<input type="hidden" id="pageId" value="questionPage"/>
</body>
</html>
