<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
</head>


<body onresize="fixColumnHeightsInPreview();">

  <input type="hidden" id="userFname" value="${sessionScope.user.fname}"/>
  <input type="hidden" id="userLname" value="${sessionScope.user.lname}"/>
  <input type="hidden" id="questionText" value="${question.text}"/>
  <input type="hidden" id="placeType" value="${question.customization.answersInRow}"/>
  <input type="hidden" id="pageWidth" value="${question.customization.answerAreaSize}"/>
  <input type="hidden" id="leftOffset" value="${question.customization.answerAreaLeftOffset}"/>
  <input type="hidden" id="imageWidth" value="${question.pictureSize}"/>
  <c:set var="i" value="1"/>
  <form action="checkAnswers" method="post" id="answerForm">
    <c:forEach items="${question.availableAnswers}" var="answer">
      <input type="hidden" id="answerText${i}" value="${answer.text}"/>
      <input type="hidden" id="imageSrc${i}" value="${answer.pictureId}"/>
      <input type="hidden" id="imageVisibility${i}" value="none"/>
      <input type="hidden" id="imageSize${i}" value="${answer.pictureSize}%"/>
      <input type="hidden" id="imageStartWidth${i}" value=""/>
      <input type="hidden" id="imageStartHeight${i}" value=""/>
      <input type="hidden" id="trueComb${i}" value="" name="trueComb${i}"/>
      <c:set var="i" value="${i + 1}"/>
    </c:forEach>


    <br/>
    <div align="center" style="opacity: 1; z-index: 1; margin-left: 1%;
            width: 98%; min-height: 130px;" id="preview">
      <h1 align="center" id="usernamePreview"></h1>
      <img style="border: none; display: none; border: 5px solid #50a0a0;" width="50%"
           src="${question.pictureId}" id="questionImg"/>
      <h2 style="margin-top: 5px;" align="center" id="questionTextPreview"></h2><br/>

      <div id="reviewAnswers"></div>

      <h3><button style="width: 150px; margin: 2px; padding: 3px; padding-left: 7px; padding-right: 7px;"
             class="btn btn-default btn-lg" id="answerComfirm"
             type="button" name="test" onclick="answer();">ответить</button></h3>
    </div>
  </form>
  <form action="skipQuestion" method="get" id="skipForm">
    <div align="center" style="opacity: 1; z-index: 1; margin-left: 1%;
        width: 98%;">
      <h3 style="margin-top: -10px;"><button style="width: 150px; margin: 2px; padding: 3px; padding-left: 7px; padding-right: 7px;"
           class="btn btn-default btn-lg"
           type="button" name="test" onclick="skip();">пропустить</button></h3>
    </div>
  </form>


</body>

<script>
  _setAddAnswerFormId("${i}");
  var i = 1;
  <c:forEach items="${question.availableAnswers}" var="answer">
    document.getElementById("trueComb" + i).value = "${answer.comb}";
    i++;
  </c:forEach>
  if(document.getElementById("questionImg").src.indexOf("showQuestion") < 0) {
    var src = document.getElementById("questionImg").src;
    var image = document.getElementById("questionImg").src.substring(src.lastIndexOf("/") + 1);
    src = src.substring(0, src.lastIndexOf("/")).concat("/images/perm/");
    document.getElementById("questionImg").src = src + "" + image;
    document.getElementById("questionImg").style.display = "";
  }
  function skip() {
    document.forms['skipForm'].submit();
  }
</script>


</html>
