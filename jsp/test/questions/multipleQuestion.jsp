<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
	<script type="text/javascript"
			src="js/tester/edit-questions/edit-multiple-question.js"></script>
</head>

<body>

	<script>
		updatePreview();
		var cells = document.getElementsByTagName("td");
		for(var i = 0; i < cells.length; i++) {
			if(document.getElementById("imageSrc" + (i + 1)) != null) {
				if(document.getElementById("imageSrc" + (i + 1)).value != "") {
					document.getElementById("imageVisibility" + (i + 1)).value = "";
					changeImageSize("imageSize" + (i + 1));
				}
			}
		}
		updatePreview();
		for(var i = 0; i < cells.length; i++) {
			cells[i].style.border = "none";
		}
		changeImageSize("imageWidth");
		function answer() {
			var checkboxes = document.getElementsByName("checkboxPreview");
			for(var i = 0; i < checkboxes.length; i++) {
				document.getElementById("trueComb" + (i + 1)).value = checkboxes[i].checked;
			}
			document.forms['answerForm'].submit();
		}
		fixColumnHeightsInPreview();
	</script>

	<%--<center>--%>

		<%--<br/>--%>
		<%--<div style="margin: 15px;"></div>--%>
		<%--<h1 class="font ${pageColor}">--%>
			<%--<c:out value="${sessionScope.user.fname} ${sessionScope.user.lname}"/></h1>--%>

		<%--<div style="margin: -15px;"></div>	--%>
	<%----%>
		<%--<c:set var="question" value="${requestScope.question}" />--%>
		<%--<h2>${question.text}</h2><br/>--%>

		<%--<form action="checkAnswers" method="post">--%>
			<%--<c:set var="count" value="0"/>--%>
			<%--<c:forEach items="${question.availableAnswers}" var="answer">--%>
				<%--<img style="border: 1px solid #cccccc;" width="500px"--%>
					 <%--src="images/test.jpg"/>--%>
				<%--<div style="margin-left: 40%;">--%>
					<%--<h4 align="left"> <input type="checkbox" name="option${count}" value="1"/> ${answer.text}</h4>--%>
				<%--</div>			--%>
				<%--<c:set var="count" value="${count + 1}"/>--%>
			<%--</c:forEach>--%>
			<%--<h3><button style="width: 150; margin: 2px; padding: 3; padding-left: 7; padding-right: 7;"--%>
				<%--class="btn btn-default btn-lg" id="answerComfirm"--%>
				<%--type="submit" name="test">ответить</button></h3>--%>
		<%--</form>--%>
		<%--<form action="skipQuestion" method="get">--%>
			<%--<h3 style="margin-top: -10px;"><button style="width: 150; margin: 2px; padding: 3; padding-left: 7; padding-right: 7;"--%>
							<%--class="btn btn-default btn-lg"--%>
							<%--type="submit" name="test">пропустить</button></h3>--%>
		<%--</form>--%>
		<%--<br/>--%>
	<%--</center>--%>

	<input type="hidden" id="pageId" value="questionPage"/>
</body>
</html>
