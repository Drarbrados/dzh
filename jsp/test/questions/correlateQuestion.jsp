<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
	<script type="text/javascript"
			src="js/tester/edit-questions/edit-correlate-question.js"></script>
</head>

<body>

	<script>
		updatePreview();
		var cells = document.getElementsByTagName("td");
		for(var i = 0; i < cells.length; i++) {
			if(document.getElementById("imageSrc" + (i + 1)) != null) {
				if(document.getElementById("imageSrc" + (i + 1)).value != "") {
					document.getElementById("imageVisibility" + (i + 1)).value = "";
					changeImageSize("imageSize" + (i + 1));
				}
			}
		}
		updatePreview();
		for(var i = 0; i < cells.length; i++) {
			cells[i].style.border = "none";
		}
		changeImageSize("imageWidth");
		function answer() {
			var table = document.getElementById("answersTable");
			var tds = [];
			tds = table.childNodes[0].childNodes;
			var td1 = tds[0];
			var td2 = tds[2];
			var length = td1.childNodes.length;
			for(var i = 0; i < length * 2; i += 2) {
				document.getElementById("trueComb" + (i + 1)).value =
						td1.childNodes[i / 2].comb;
				document.getElementById("trueComb" + (i + 2)).value =
						td2.childNodes[i / 2].comb;
//				alert(td1.childNodes[i / 2].comb + " " + td2.childNodes[i / 2].comb);
			}
			document.forms['answerForm'].submit();
		}
		$(function() {
			$("#sortable1").sortable();
			$("#sortable1").disableSelection();
			$("#sortable2").sortable();
			$("#sortable2").disableSelection();
		});
		fixColumnHeightsInPreview();
	</script>


	<input type="hidden" id="pageId" value="questionPage"/>
</body>
</html>
