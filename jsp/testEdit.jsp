<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/navbar.jsp"%>

<html>
<head>
<!-- Удалить запрет кеширования!!!!!!!!!!!!! -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Редактирование Теста</title>

	<script type="text/javascript" src="js/tester/filters.js"></script>
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/jquery-ui.js"></script>


	<style>
		#selectable li { background-color: #60b0b0; color: #404040; }
		#selectable .ui-selecting { background: #409090; }
		#selectable .ui-selected { background: #407070; color: #f0f0f0; }
		#selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }

		.ghq_area .ui-selecting { background: #609090; }
		.ghq_area .ui-selected { background: #609090; color: #f0f0f0; }
	</style>

</head>
<body class="testerBody" onmousemove="movement(event)">

<div style="width: 60%; height: 80%; background-color: #005050; z-index: 5555;
						position: fixed; margin-left: 20%;
						border-top-left-radius: 18px 15px;
						border-top-right-radius: 18px 15px;
						border-bottom-right-radius: 18px 15px;
						border-bottom-left-radius: 18px 15px;
						display: none;" id="frame">
	<iframe id="get_questions_iframe" style="width: 98%; height: 90%; margin-left: 1%; margin-top: 1%;
				border-top-left-radius: 18px 15px;
				border-top-right-radius: 18px 15px;
				border-bottom-right-radius: 18px 15px;
				border-bottom-left-radius: 18px 15px;"
			frameborder="0" scrolling="yes"
			marginheight="0" marginwidth="0"
			src="../testerbelsut/topictreeframe?tid=${test.id}" onload="refreshQuestions();">
	</iframe>
	<button id="iframe_btn" class="form-control testerBodyBtnInverse" style="width: 20%; height: 6%; margin-left: 40%;
				margin-top: 1%;"
			onclick="document.getElementById('frame').style.display = 'none';
						 document.getElementById('gshadow').style.display = 'none';">
		Готово</button>
	<br/><br/><br/>
</div>

<form action="edittest" method="post" id="edittest_post">

<br/>
<h1 align="center" style="margin-bottom: 2%;">Тест:
	<input name="testname" style="background-color: transparent; border: none" value="${test.title}"/>
</h1>


	<h4 class="areaTitle">Параметры теста:</h4>
	<div class="algorithmAreaWrapper">
		<table style="width: 100%;">
		<tr><td style="width: 45%;">
			<table style="width: 100%">
				<tr>
					<td style="width: 50%;"><h4>Время Теста: </h4></td>
					<td><input name="testtime" value="${test.maxTime}" class="form-control" style="width: 98%; margin-left: 1%;"/></td>
				</tr>
				<tr>
					<td><h4>Число Прохождений в День: </h4></td>
					<td><input name="testattemptsperday" value="${test.attemptsPerDay}" class="form-control" style="width: 98%; margin-left: 1%;"/></td>
				</tr>
				<tr>
					<td><h4>Баллов для Зачёта:</h4></td>
					<td><input name="testmincorrectnum" value="${test.minCorrectNum}" class="form-control" style="width: 98%; margin-left: 1%;"/></td>
				</tr>
				<tr>
					<td><h4>Количество Вопросов: </h4></td>
					<td><input name="testquestionsnumber" value="${test.questionsNumber}" class="form-control" style="width: 98%; margin-left: 1%;"/></td>
				</tr>
			</table>
		</td>
		<td style="width: 10%;"></td>
		<td style="width: 45%;">
			<table style="width: 100%">
				<tr>
					<td style="width: 50%;"><h4>Дата создания: </h4></td>
					<td><input name="testcreatedate" value="${0}" readonly style="width: 98%; margin-left: 1%; border: none; background-color: transparent !important;"/></td>
				</tr>
				<tr>
					<td><h4>Автор: </h4></td>
					<td><input name="testcreator" value="${0}" readonly style="width: 98%; margin-left: 1%; border: none; background-color: transparent !important;"/></td>
				</tr>
				<tr>
					<td><h4>Дата Изменения:</h4></td>
					<td><input name="testmodifydate" value="${0}" readonly style="width: 98%; margin-left: 1%; border: none; background-color: transparent !important;"/></td>
				</tr>
				<tr>
					<td><h4>Автор: </h4></td>
					<td><input name="testmodifyer" value="${0}" readonly style="width: 98%; margin-left: 1%; border: none; background-color: transparent !important;"/></td>
				</tr>
			</table>
		</td>
		</tr>
		</table>
	</div>


	<h4 class="areaTitle" style="margin-bottom: 5px;">Настройка выборки вопросов:</h4>
	<div><table width="100%" class="questionTable" id="table">
		<tr height="15px"><th style="width: 48%"><p class="colomnTitle">Группы вопросов:</p></th><th style="width: 4%"></th>
			<th style="width: 48%"><p class="colomnTitle">Вопросы из [<a id="atest" style="cursor: pointer;"
				onclick="document.getElementById('frame').style.display = ''; document.getElementById('gshadow').style.display = ''">${sessionScope.frameTest.title}</a>]:</p></th></tr>
		<tr>
			<td class="colomn">

			<td></td>
			<td>

			</td>
		</tr>
		<tr><td style="width: 48%" rowspan="2" id="from" class="colomn"></td>
		<td style="width: 4%; vertical-align: center; text-align: center;">
			<button type="button" onclick="moveLeft();" id="moveleft" style="background: transparent; border: none">
				<i class="glyphicon glyphicon-arrow-left"></i></button>
		</td>
		<td style="width: 48%;" rowspan="2" id="to" class="colomn">
			<ol id="selectable"></ol>
		</td></tr>
	</table></div>
	<button type="button" onclick="addGroup()" class="btn btn-default" style="margin-left: 1.2%; width: 45.55%;">Добавить группу</button>


	<p align="center"><button  class="btn btn-default" style="margin-bottom: 4.2%; margin-top: 1.1%; letter-spacing: 1pt">
		Сохранить изменения</button></p>

	</form>

	<c:forEach items="${algorithmList}" var="algorithm">
		<input type="hidden" value="${algorithm.name}" name="algorithm_name"/>
		<input type="hidden" value="${algorithm.allParameterNames}" name="algorithm_params"/>
	</c:forEach>

	<br/><br/>

</body>

<script>

	var left1 = 0;
	var left2 = 0;
	var inmove = false;
	function movementStart(event) {
		left1 = event.clientX;
		setTimeout('movementStop();', 150);
		inmove = true;
	}
	function movementStop() {
		left1 -= left2;
		inmove = false;
	}
	function movement(event) {
		if(inmove) {
			left2 = event.clientX;
		}
	}


	function refreshQuestions() {
		var doc = document.getElementById('get_questions_iframe').contentWindow.document;
		var questions = doc.getElementsByName("ifr_questions");
		var place = document.getElementById("selectable");
		var childs = place.childNodes;
		for(var i = 0; i < childs.length; i++) {
			place.removeChild(childs[i]);
			i--;
		}
		for(i = 0; i < questions.length; i++) {
			var li = document.createElement("li");
			li.className = "ui-widget-content btn question";
			li.id = "q" + questions[i].id;
			li.innerHTML = questions[i].value;
			li.onmousedown= function(event) {
				movementStart(event);
			};
			place.appendChild(li);
		}
		document.getElementById("atest").innerHTML = doc.getElementById("ifr_test").value;
	}

	function addGroup() {
		if(addGroup.id == null) {
			addGroup.id = 1;
		}
		var place = document.getElementById("from");
		var div = document.createElement("div");
		var hiddenQuestionsDiv = document.createElement("div");
		hiddenQuestionsDiv.style.marginTop = "-10px";
		hiddenQuestionsDiv.style.width = "95%";
		hiddenQuestionsDiv.style.marginLeft = "2.5%";
		hiddenQuestionsDiv.style.display = "none";
		var innerdiv = document.createElement("div");
		innerdiv.style.height = "5px";
		innerdiv.style.backgroundColor = "#eeeeee";
		hiddenQuestionsDiv.appendChild(innerdiv);
		var h4 = document.createElement("h4");
		h4.innerHTML = "Вопросы:";
		h4.className = "areaTitle";
		hiddenQuestionsDiv.appendChild(h4);
		var dragArea = document.createElement("div");
		dragArea.style.border = "1px dashed #409090";
		dragArea.style.width = "95%";
		dragArea.style.marginLeft = "2.5%";
		var ol = document.createElement("ol");
		ol.id = "ghq_area" + addGroup.id;

		var qinput = document.createElement("input");
		qinput.type = "hidden";
		qinput.name = "groupquestions";
		qinput.id = "groupquestions" + addGroup.id;
		qinput.value = "noq";
		ol.appendChild(qinput);

		ol.className = "ghq_area";
		ol.name = "qol";
		dragArea.appendChild(ol);


		$(ol).selectable();


		hiddenQuestionsDiv.appendChild(dragArea);
		hiddenQuestionsDiv.id = "ghq" + addGroup.id;
		hiddenQuestionsDiv.name = "ghq";
		hiddenQuestionsDiv.style.backgroundColor = "#eeeeee";
		hiddenQuestionsDiv.style.marginBottom = "5px";

		var hqd_sel_all = document.createElement("button");
		hqd_sel_all.innerHTML = "все";
		var hqd_sel_equ = document.createElement("button");
		hqd_sel_equ.innerHTML = "повторяющиеся";
		var hqd_del_sel = document.createElement("button");
		hqd_del_sel.innerHTML = "удалить";

		var btn_table = document.createElement("table");
		var bt_tr = document.createElement("tr");
		var bt_td1 = document.createElement("td");
		var bt_td2 = document.createElement("td");
		var bt_td3 = document.createElement("td");
		btn_table.appendChild(bt_tr);
		bt_tr.appendChild(bt_td1);
		bt_tr.appendChild(bt_td2);
		bt_tr.appendChild(bt_td3);
		hiddenQuestionsDiv.appendChild(btn_table);

		bt_td1.appendChild(hqd_sel_all);
		bt_td2.appendChild(hqd_sel_equ);
		bt_td3.appendChild(hqd_del_sel);

		btn_table.style.marginLeft = "2%";
		btn_table.style.width = "96%";

		bt_td1.style.width = "33%";
		bt_td2.style.width = "33%";
		bt_td3.style.width = "33%";

		hqd_del_sel.className = "qgroup-btn";
		hqd_sel_all.className = "qgroup-btn";
		hqd_sel_equ.className = "qgroup-btn";

		hqd_del_sel.type = "button";
		hqd_sel_all.type = "button";
		hqd_sel_equ.type = "button";

		id = addGroup.id;
		hqd_sel_all.onclick = function() {
			selectAllQuestionsInGroup(id);
		};
		hqd_sel_equ.onclick = function() {
			selectEquQuestionsInGroup(id);
		};
		hqd_del_sel.onclick = function() {
			deleteSelectedQuestionsInGroup(id);
		};



		var hiddenAlgorithmDiv = document.createElement("div");
		hiddenAlgorithmDiv.style.marginTop = "-10px";
		hiddenAlgorithmDiv.style.width = "95%";
		hiddenAlgorithmDiv.style.marginLeft = "2.5%";
		hiddenAlgorithmDiv.style.display = "none";
		innerdiv = document.createElement("div");
		innerdiv.style.height = "5px";
		innerdiv.style.backgroundColor = "#eeeeee";
		hiddenAlgorithmDiv.appendChild(innerdiv);
		h4 = document.createElement("h4");
		h4.innerHTML = "Алгоритм: ";
		var select = document.createElement("select");
		var option;
		var algorithms = document.getElementsByName("algorithm_name");
		for(var x = 0; x < algorithms.length; x++) {
			option = document.createElement("option");
			option.innerHTML = algorithms[x].value;
			select.appendChild(option);
		}
		h4.appendChild(select);
		select.style.width = "50%";
		select.id = "select" + addGroup.id;


		var id = addGroup.id;
		select.onchange = function() {
			changeAlgorithm(id);
		};




		h4.className = "areaTitle";
		hiddenAlgorithmDiv.appendChild(h4);
		dragArea = document.createElement("div");
		dragArea.style.border = "1px dashed #409090";
		dragArea.style.width = "95%";
		dragArea.style.marginLeft = "2.5%";
		var paramsdiv = document.createElement("div");
		dragArea.appendChild(paramsdiv);
		paramsdiv.id = "apars" + addGroup.id;
		hiddenAlgorithmDiv.appendChild(dragArea);
		hiddenAlgorithmDiv.id = "gha" + addGroup.id;
		hiddenAlgorithmDiv.name = "gha";
		hiddenAlgorithmDiv.style.backgroundColor = "#eeeeee";
		hiddenAlgorithmDiv.style.marginBottom = "5px";
		hiddenAlgorithmDiv.appendChild(document.createElement("br"));

		place.appendChild(div);
		place.appendChild(hiddenQuestionsDiv);
		place.appendChild(hiddenAlgorithmDiv);
		div.className = "btn question";
		div.style.marginBottom = "4px";
		div.id = "g" + addGroup.id;



		var table = document.createElement("table");
		table.style.width = "100%";
		var tr = document.createElement("tr");
		var td1 = document.createElement("td");
		var td2 = document.createElement("td");
		td1.style.width = "50%";
		td1.style.fontWeight = "bold";
		td2.style.textAlign = "right";
		div.appendChild(table);
		table.appendChild(tr);
		tr.appendChild(td1);
		tr.appendChild(td2);

		var text = document.createElement("input");
		text.style.backgroundColor = "transparent";
		text.style.border = "none";
		text.value = "Группа " + addGroup.id;
		text.id = "groupName" + addGroup.id;
		text.onchange = function() {
			changeGroupName(id, text.value);
		};
		td1.appendChild(text);

		var divinput = document.createElement("input");
		divinput.type = "hidden";
		divinput.value = text.value;
		divinput.name = "groupname";
		divinput.id = "gname" + addGroup.id;
		div.appendChild(divinput);
		divinput = document.createElement("input");
		divinput.type = "hidden";
		divinput.value = 0;
		divinput.name = "groupid";
		div.appendChild(divinput);





		var algorithmBtn = document.createElement("button");
		td2.appendChild(algorithmBtn);
		var i = document.createElement("i");
		i.className = "glyphicon glyphicon-pencil";
		i.style.fontSize = "18px";
		algorithmBtn.appendChild(i);
		algorithmBtn.style.backgroundColor = "transparent";
		algorithmBtn.style.border = "none";

		id = addGroup.id;
		algorithmBtn.onclick = function() {
			toggleGroupAlgorithm(id);
		};

		var questionsBtn = document.createElement("button");
		td2.appendChild(questionsBtn);
		i = document.createElement("i");
		i.className = "glyphicon glyphicon-list";
		i.style.fontSize = "18px";
		questionsBtn.appendChild(i);
		questionsBtn.style.backgroundColor = "transparent";
		questionsBtn.style.border = "none";

		id = addGroup.id;
		questionsBtn.onclick = function() {
			toggleGroupQuestions(id);
		};

		var closeBtn = document.createElement("button");
		td2.appendChild(closeBtn);
		i = document.createElement("i");
		i.className = "glyphicon glyphicon-remove";
		i.style.fontSize = "18px";
		closeBtn.appendChild(i);
		closeBtn.style.backgroundColor = "transparent";
		closeBtn.style.border = "none";


		closeBtn.type = "button";
		questionsBtn.type = "button";
		algorithmBtn.type = "button";


		id = addGroup.id;
		closeBtn.onclick = function() {
			deleteGroup(id);
		};

		id = addGroup.id;
		div.onclick = function() {
			groupClick(id);
		};

		changeAlgorithm(id);

		addGroup.id++;
	}

	function changeGroupName(id, newname) {
		var divinput = document.getElementById("gname" + id);
		divinput.value = newname;
	}

	function changeAlgorithmParams(id) {
		var pars = document.getElementsByName("algorithmpars" + id);
		var parsinp = document.getElementById("groupalgorithmparams" + id);
		parsinp.value = "";
		for(var i = 0; i < pars.length; i++) {
			if(parsinp.value == "") {
				parsinp.value = pars[i].value;
			} else {
				parsinp.value = parsinp.value + "|" + pars[i].value;
			}
		}
	}

	function changeAlgorithm(id) {
		var selected = document.getElementById("select" + id).value;
		var place = document.getElementById("apars" + id);
		var pars = place.childNodes;
		for(var i = 0; i < pars.length; i++) {
			place.removeChild(pars[i]);
			i--;
		}
		var algorithms = document.getElementsByName("algorithm_name");
		for(i = 0; i < algorithms.length; i++) {
			if(selected == algorithms[i].value) {
				var alginput = document.getElementById("groupalgorithmindex" + id);
				if(alginput != null) {
					place.parentNode.removeChild(alginput);
				}
				alginput = document.createElement("input");
				alginput.type = "hidden";
				alginput.value = i;
				alginput.name = "groupalgorithmindex";
				alginput.id = "groupalgorithmindex" + id;
				place.appendChild(alginput);
				break;
			}
		}
		pars = document.getElementsByName("algorithm_params");
		var str = pars[i].value;
		str = str.substring(1, str.length - 1);
		var res = str.split(",");
		var table = document.createElement("table");
		place.appendChild(table);
		table.style.width = "99%";
		table.style.marginLeft = "0.5%";

		alginput = document.getElementById("groupalgorithmparams" + id);
		if(alginput != null) {
			place.parentNode.removeChild(alginput);
		}
		alginput = document.createElement("input");
		alginput.type = "hidden";
		alginput.name = "groupalgorithmparams";
		alginput.id = "groupalgorithmparams" + id;
		alginput.value = "";

		for(i = 0; i < res.length; i++) {
			var tr = document.createElement("tr");
			table.appendChild(tr);
			var td1 = document.createElement("td");
			var td2 = document.createElement("td");
			tr.appendChild(td1);
			tr.appendChild(td2);
			var text = document.createElement('text');
			text.innerHTML= res[i] + ": ";
			text.style.color = "#307070";
			td1.appendChild(text);
			var input = document.createElement("input");
			input.value = "0";
			input.style.width = "100%";
			input.style.backgroundColor = "#fafafa";
			input.style.border = "none";
			input.style.color = "#307070";
			input.name = "algorithmpars" + id;
			input.onchange = function() {
				changeAlgorithmParams(id);
			};
			if(i != res.length - 1) {
				tr.style.borderBottom = "1px solid #b0b0b0";
			}
			td2.appendChild(input);
			if(alginput.value == "") {
				alginput.value = input.value;
			} else {
				alginput.value = alginput.value + "|" + input.value;
			}
			td1.style.width = "48%";
		}
		place.appendChild(alginput);
	}


	function deleteGroup(groupId) {
		groupClick(groupId);
		if(confirm("Удалить группу вопросов \"" +
				document.getElementById("groupName" + groupId).value + "\"?")) {
			var group = document.getElementById("g" + groupId);
			var hiddenQDiv = document.getElementById("ghq" + groupId);
			var hiddenADiv = document.getElementById("gha" + groupId);
			group.parentNode.removeChild(group);
			hiddenQDiv.parentNode.removeChild(hiddenQDiv);
			hiddenADiv.parentNode.removeChild(hiddenADiv);
			for (var i = 0; i < groupClick.arr.length; i++) {
				if (groupClick.arr[i] == groupId) {
					groupClick.arr.splice(i, 1);
					break;
				}
			}
		}
	}


	function selectAllQuestionsInGroup(id) {
		var list = document.getElementsByClassName("test" + id);
		for(var i = 0; i < list.length; i++) {
			$(list[i]).addClass("ui-selected");
		}
	}
	function selectEquQuestionsInGroup(id) {
		var list = document.getElementsByClassName("test" + id);
		for(var i = 0; i < list.length; i++) {
			$(list[i]).removeClass("ui-selected");
		}
		for(i = 0; i < list.length; i++) {
			for(var j = i + 1; j < list.length; j++) {
				if(list[i].innerHTML == list[j].innerHTML) {
					$(list[j]).addClass("ui-selected");
				}
			}
		}
	}
	function deleteSelectedQuestionsInGroup(id) {
		var list = document.getElementsByClassName("test" + id);
		for(var i = 0; i < list.length; i++) {
			if($(list[i]).hasClass("ui-selected")) {
				list[i].parentNode.removeChild(list[i]);
				i--;
			}
		}
	}


	groupClick.arr = [];
	function groupClick(id) {
		if(document.getElementById("g" + id).className == "btn question") {
			document.getElementById("g" + id).className = "btn question-active";
			var a = 0;
			for(var i = 0; i < groupClick.arr.length; i++) {
				if(groupClick.arr[i] == id) {
					a = 1;
					break;
				}
			}
			if(a == 0) {
				groupClick.arr.push(id);
			}
		} else {
			document.getElementById("g" + id).className = "btn question";
			for(i = 0; i < groupClick.arr.length; i++) {
				if(groupClick.arr[i] == id) {
					groupClick.arr.splice(i, 1);
					break;
				}
			}
		}
	}

	function toggleGroupAlgorithm(id) {
		groupClick(id);
		hidePrevQuestions();
		hidePrevQuestions.id = 0;
		document.getElementById("gha" + id).style.display = "";
		hidePrevAlgorithm();
		if(document.getElementById("gha" + id).style.display == "") {
			hidePrevAlgorithm.id = id;
		} else {
			hidePrevAlgorithm.id = 0;
		}
	}
	function toggleGroupQuestions(id) {
		groupClick(id);
		hidePrevAlgorithm();
		hidePrevAlgorithm.id = 0;
		document.getElementById("ghq" + id).style.display = "";
		hidePrevQuestions();
		if(document.getElementById("ghq" + id).style.display == "") {
			hidePrevQuestions.id = id;
		} else {
			hidePrevQuestions.id = 0;
		}
	}

	hidePrevAlgorithm.id = 0;
	function hidePrevAlgorithm() {
		if(document.getElementById("gha" + hidePrevAlgorithm.id) != null) {
			document.getElementById("gha" + hidePrevAlgorithm.id).style.display = "none";
		}
	}
	hidePrevQuestions.id = 0;
	function hidePrevQuestions() {
		if(document.getElementById("ghq" + hidePrevQuestions.id) != null) {
			document.getElementById("ghq" + hidePrevQuestions.id).style.display = "none";
		}
	}



	var selected = new Array();
	$(function() {
		$("#selectable").selectable({
			selected: function(event, ui){
				if(selected.indexOf(ui.selected.id) == -1) {
					selected.push(ui.selected.id);
				}
			},
			unselected: function(event, ui){
				if(selected.indexOf(ui.unselected.id) != -1) {
					selected.splice(selected.indexOf(ui.unselected.id), 1);
				}
			}
		});
	});

	function moveLeft() {
		for(var id = 0; id < groupClick.arr.length; id++) {
			var place = document.getElementById("ghq_area" + groupClick.arr[id]);
			var qinput = document.getElementById("groupquestions" + groupClick.arr[id]);
			if(qinput.value == "noq") {
				qinput.value = "";
			}
			for(var q = 0; q < selected.length; q++) {
				var input = document.createElement("input");
				input.type = "hidden";
				input.name = "qids";
				input.value = selected[q];
				if(qinput.value == "") {
					qinput.value = selected[q];
				} else {
					qinput.value = qinput.value + "|" + selected[q];
				}
				place.appendChild(input);
				var li = document.createElement("li");
				place.appendChild(li);
				li.className = "ui-widget-content test" + groupClick.arr[id];
				li.innerHTML = document.getElementById(selected[q]).innerHTML;
			}
			place.appendChild(qinput);
		}
		$('#selectable .ui-selected').removeClass('ui-selected');
		selected.splice(0, selected.length);
	}

</script>

</html>