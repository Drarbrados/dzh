<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/navbar.jsp"%>

<html>
<head>
  <script type="text/javascript" src="js/tester/filters.js"></script>
  <script type="text/javascript" src="js/tester/tableSort.js"></script>
  <script type="text/javascript" src="js/tester/showAverageByCol.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
</head>
<body class="testerBody">
  <br/>
  <div style="margin: 15px;"></div>
  <h1 align="center">Личная Статистика</h1>

  <h4 class="areaTitle">Настройка фильтров: </h4><br/>

  <table style="width: 90%; margin-left: 7%; margin-bottom: 10px;">
    <tr>
      <td style="width: 33%;">
          <input class="form-control" type="text" name="role"
                 maxlength="40" placeholder="Группа" disabled="disabled"
                 value="${sessionScope.user.group.name}" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
          <input class="form-control" type="text" name="role" disabled="disabled"
                 maxlength="40" placeholder="Студент"
                 value="${sessionScope.user.fname} ${sessionScope.user.lname}" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
        <table style="margin-top: -15px;">
          <tr>
            <td style="width: 10%;">
              <div>C:</div>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="day" id="dayFrom"
                      onchange="doFilters(); saveFilterConfs(this);">
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="month" id="monthFrom"
                      onchange="doFilters(); daysInMonth(1); saveFilterConfs(this);
                      saveFilterConfs(document.getElementById('dayFrom'));">
                <option value="0" selected="selected">Январь</option>
                <option value="1">Февраль</option>
                <option value="2">Апрель</option>
                <option value="3">Март</option>
                <option value="4">Май</option>
                <option value="5">Июнь</option>
                <option value="6">Июль</option>
                <option value="7">Август</option>
                <option value="8">Сентябрь</option>
                <option value="9">Октябрь</option>
                <option value="10">Ноябрь</option>
                <option value="11">Декабрь</option>
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="year" id="yearFrom"
                      onchange="doFilters(); daysInMonth(1); saveFilterConfs(this);
                      saveFilterConfs(document.getElementById('dayFrom'));">
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
                <option>2030</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 33%;">
        <input class="form-control" type="text" name="role" id="testFilter"
               maxlength="40" placeholder="Тест" style="width: 90%;"
               onkeyup="doFilters(); showAverageOnPage(); saveFilterConfs(this);"/><br/>
      </td>
      <td style="width: 33%;">
        <input class="form-control" type="text" name="role" disabled="disabled"
               maxlength="40" placeholder="Вопрос" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
        <table style="margin-top: -15px;">
          <tr>
            <td style="width: 10%;">
              <div>По:</div>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="day" id="dayTo"
                      onchange="doFilters(); saveFilterConfs(this);">
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="month" id="monthTo"
                      onchange="doFilters(); daysInMonth(2); saveFilterConfs(this);
                      saveFilterConfs(document.getElementById('dayTo'));">
                <option value="0" selected="selected">Январь</option>
                <option value="1">Февраль</option>
                <option value="2">Апрель</option>
                <option value="3">Март</option>
                <option value="4">Май</option>
                <option value="5">Июнь</option>
                <option value="6">Июль</option>
                <option value="7">Август</option>
                <option value="8">Сентябрь</option>
                <option value="9">Октябрь</option>
                <option value="10">Ноябрь</option>
                <option value="11">Декабрь</option>
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="year" id="yearTo"
                      onchange="doFilters(); daysInMonth(2); saveFilterConfs(this);
                      saveFilterConfs(document.getElementById('dayTo'));">
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
                <option selected="selected">2030</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 33%;">
        <table style="margin-top: -10px; width: 200px;">
          <tr>
            <td style="width: 50%">
              <div class="squaredThree-inverse-bright">
                <input style="display: none;" onclick="checkBoxSelect(this); saveFilterConfs(this);"
                       type="checkbox" name="modeTest" id="testChkBox" value="on" checked="checked"/>
                <label style="margin-left: 0px;" for="testChkBox"></label>
                    <span style="margin-left: 30px; margin-top: 5px; cursor: pointer;"
                          onclick="document.getElementById('testChkBox').click();"
                          id="testChkBoxSpan" class="statsTableCellTest" >
                      Зачёт
                    </span>
              </div>
            </td>
            <td>
              <div class="squaredThree-inverse-bright">
                <input style="display: none;" onclick="checkBoxSelect(this); saveFilterConfs(this);"
                       type="checkbox" name="modeTraining" id="trainingChkBox" value="on"
                       checked="checked"/>
                <label style="margin-left: 0px;" for="trainingChkBox"></label>
                    <span style="margin-left: 30px; margin-top: 5px; cursor: pointer;"
                          onclick="document.getElementById('trainingChkBox').click();"
                          class="statsTableCellTraining" id="trainingChkBoxSpan">
                      Тренировка
                    </span>
              </div>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 33%;"></td>
      <td style="width: 33%;"></td>
    </tr>
  </table>

  <form method="get" action="#" id="form">
    <table class="statsTable" id="table">
      <tr>
        <th id="studentTh" onclick="ghostSort(this); showAverageOnPage(); saveSortConfs(this);">Студент</th>
        <th id="testTh" onclick="sort(this); showAverageOnPage(); saveSortConfs(this);">Тест</th>
        <th id="resultTh" onclick="sort(this, 1); showAverageOnPage(); saveSortConfs(this);">Результат</th>
        <th id="dateTh" onclick="sort(this); showAverageOnPage(); saveSortConfs(this);">Дата</th>
      </tr>
      <c:forEach items="${requestScope.stats}" var="statRow">
        <tr class="statsTableRowS1">
          <td>${statRow.userStat.username}</td>
          <c:choose>
            <c:when test="${statRow.testStat.testMode eq statRow.testStat.testMode.testMode}">
              <td class="statsTableCellTest">${statRow.testStat.testName}</td>
            </c:when>
            <c:otherwise>
              <td class="statsTableCellTraining">${statRow.testStat.testName}</td>
            </c:otherwise>
          </c:choose>
          <td>${statRow.testStat.mark}</td>
          <td>${statRow.date}</td>
        </tr>
      </c:forEach>
    </table>
  </form>

  <br/><br/>

  <input type="hidden" id="pageId" value="statsPage"/>
</body>
</html>

<script>
  function daysInMonth(selectIndex) {
    if(selectIndex == 1) {
      var select = document.getElementById("dayFrom").innerHTML = '';
      for(var i = 0; i < 32 -
          new Date($("#yearFrom").val(), $("#monthFrom").val(), 32).getDate(); i++) {
        select = document.getElementById("dayFrom"),
                opt = document.createElement("option");
        opt.value = i + 1;
        if(opt.value <= 9) {
          opt.value = "0" + opt.value;
        }
        opt.textContent = opt.value;
        select.appendChild(opt);
      }
    } else {
      var select = document.getElementById("dayTo").innerHTML = '';
      for(var i = 0; i < 32 -
      new Date($("#yearTo").val(), $("#monthTo").val(), 32).getDate(); i++) {
        select = document.getElementById("dayTo"),
                opt = document.createElement("option");
        opt.value = i + 1;
        if(opt.value <= 9) {
          opt.value = "0" + opt.value;
        }
        opt.textContent = opt.value;
        select.appendChild(opt);
      }
    }
  }
  function saveSortConfs(td) {
    $.session.set("colId", td.id);
    if(td.prevsort == "yDown") {
      $.session.set("colDir", "yUp");
    } else {
      $.session.set("colDir", "yDown");
    }
  }
  function saveFilterConfs(filter) {
    $.session.set(filter.id, filter.value);
  }
  function onLoadPage() {
    daysInMonth(1);
    daysInMonth(2);
    if(!$.session.get("colId") || $.session.get("colId") == "NaN") {
      $("#monthFrom").val(0);
	  $("#monthTo").val(0);
	  $.session.set("colId", "studentTh");
      $.session.set("colDir", "yDown");
      $.session.set("testFilter", "");
      $.session.set("dayFrom", "01");
      $.session.set("dayTo", "01");
      $.session.set("monthFrom", "0");
      $.session.set("monthTo", "0");
      $.session.set("yearFrom", "2015");
      $.session.set("yearTo", "2030");
      $.session.set("testChkBox", "on");
      $.session.set("trainingChkBox", "on");
    } else {
      $("#testFilter").val($.session.get("testFilter"));
      $("#dayFrom").val($.session.get("dayFrom"));
      $("#dayTo").val($.session.get("dayTo"));
      $("#monthFrom").val($.session.get("monthFrom"));
      $("#monthTo").val($.session.get("monthTo"));
      $("#yearFrom").val($.session.get("yearFrom"));
      $("#yearTo").val($.session.get("yearTo"));
      $("#testChkBox").val($.session.get("testChkBox"));
      $("#trainingChkBox").val($.session.get("trainingChkBox"));
      if($("#testChkBox").val() == "on") {
        $("#testChkBox").attr("checked", true);
      } else {
        $("#testChkBox").attr("checked", false);
      }
      if($("#trainingChkBox").val() == "on") {
        $("#trainingChkBox").attr("checked", true);
      } else {
        $("#trainingChkBox").attr("checked", false);
      }
      if($.session.get("colId") == "studentTh") {
        document.getElementById("studentTh").prevsort = $.session.get("colDir");
        ghostSort(document.getElementById("studentTh"));
      } else if($.session.get("colId") == "testTh") {
        document.getElementById("testTh").prevsort = $.session.get("colDir");
        sort(document.getElementById("testTh"));
      } else if($.session.get("colId") == "resultTh") {
        document.getElementById("resultTh").prevsort = $.session.get("colDir");
        sort(document.getElementById("resultTh"), 1);
      } else if($.session.get("colId") == "dateTh") {
        document.getElementById("deteTh").prevsort = $.session.get("colDir");
        sort(document.getElementById("dateTh"), 1);
      }
    }
    doFilters();
    showAverageOnPage();
  }
  function testModeFilter() {
    var cellIndex = 1;
    var table = document.getElementById("table");
    for (var r = 1; r < table.rows.length; r++) {
      var a = 1;
      var row = table.rows[r].cells[cellIndex].className;
      if(document.getElementById("testChkBox").checked == false &&
              row == document.getElementById("testChkBoxSpan").className) {
        a = 0;
      } else if(document.getElementById("trainingChkBox").checked == false &&
              row == document.getElementById("trainingChkBoxSpan").className) {
        a = 0;
      } else if(table.rows[r].style.display == 'none') {
        a = 0;
      }
      if (a == 0) {
        table.rows[r].style.display = 'none';
      } else {
        table.rows[r].style.display = '';
      }
    }
  }
  function dateFilter(cellIndex) {
    var dayFrom = parseInt($("#dayFrom").val());
    var dayTo = parseInt($("#dayTo").val());
    var monthFrom = parseInt($("#monthFrom").val());
    var monthTo = parseInt($("#monthTo").val());
    var yearFrom = parseInt($("#yearFrom").val());
    var yearTo = parseInt($("#yearTo").val());
    var table = document.getElementById("table");
    for (var r = 1; r < table.rows.length; r++) {
      var a = 1;
      var date = table.rows[r].cells[cellIndex].innerHTML;
      if(date != "") {
        var year = parseInt(date.substring(0, 4));
        var month = parseInt(date.substring(5, 7)) - 1;
        var day = parseInt(date.substring(8, 10));
        if(table.rows[r].style.display == 'none') {
          a = 0;
        } else if(yearFrom > year || year > yearTo) {
          a = 0;
        } else if(year == yearTo && (monthFrom > month || month > monthTo)) {
          a = 0;
        } else if(year == yearTo && month == monthTo && (dayFrom > day || day > dayTo)) {
          a = 0;
        }
      }
      if (a == 0) {
        table.rows[r].style.display = 'none';
      } else {
        table.rows[r].style.display = '';
      }
    }
  }
  function checkBoxSelect(onclickedChkBox) {
    if(document.getElementById("testChkBox") == onclickedChkBox &&
            onclickedChkBox.checked == false) {
      if(document.getElementById("trainingChkBox").checked == false) {
        onclickedChkBox.checked = true;
      } else {
        onclickedChkBox.value = 'off';
        doFilters(); showAverageOnPage();
      }
    } else if(document.getElementById("trainingChkBox") == onclickedChkBox &&
            onclickedChkBox.checked == false) {
      if(document.getElementById("testChkBox").checked == false) {
        onclickedChkBox.checked = true;
      } else {
        onclickedChkBox.value = 'off';
        doFilters(); showAverageOnPage();
      }
    } else {
      onclickedChkBox.value = 'on';
      doFilters(); showAverageOnPage();
    }
  }
  function doFilters() {
    var testFilter = document.getElementById('testFilter');
    filter(testFilter.value, 1);
    testModeFilter();
    dateFilter(3);
  }
  function showAverageOnPage() {
    if(document.getElementById('testTh').prevsort == "yUp" ||
            document.getElementById('testTh').prevsort == "yDown") {
      showAverage(1, 2, 4);
      showAverage(0, 2, 4, true);
    } else {
      showAverage(0, 2, 4);
    }
  }
  onLoadPage();
</script>
