<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/navbar.jsp"%>

<html>
<head>
<!-- Удалить запрет кеширования!!!!!!!!!!!!! -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Группы вопросов</title>
    <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/tester/questionsGroup.js"></script>
	<script type="text/javascript" src="js/tester/filters.js"></script>
    
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/tester.css" />
	
    <style>
		 .algorithmValues{
		 	display: none;
		 }
		 
		 
		
		/* по фану, нужно удалить */
		#from .ui-selecting {
		    background: #9933ff;
		}
		#from .ui-selected {
		    background: #9933ff;
		    color: white;
		}
		#to .ui-selecting {
		    background: #9933ff;
		}
		#to .ui-selected {
		    background: #9933ff;
		    color: white;
		}
    </style>
    
</head>
<body class="testerBody">
<br/>
<h1 align="center" style="margin-bottom: 2%;">Создание группы вопросов</h1>

	<h4 class="areaTitle">Параметры выборки</h4>
	<div class="algorithmAreaWrapper">
		<div class="algorithmArea">
		<table style="width: 98%;"><tr>
			<th style="text-align: right;">Тип выборки:</th>
			<th>
				<select style="z-index: 500; opacity: 0.99" class="form-control" name="algorithm" size="1" onchange="updateParams();">
					<option selected value="Rand"> Рандомайз
					<option value="Rand1"> Рандомайз1
					<option value="Rand2"> Рандомайз2
				</select>
				<i class="glyphicon glyphicon-info-sign"></i>
			</th>
		</tr></table>
		<div id="paramsRand" class="algorithmValues">
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" Размер выборки"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" Параметр1"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" Параметр2"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" Параметр3"/></div>
			</div>
		</div>
		<div id="paramsRand1" class="algorithmValues">
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 1Размер выборки"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 1Параметр1"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 1Параметр2"/></div>
			</div>
		</div>
		<div id="paramsRand2" class="algorithmValues">
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 2Размер выборки"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 2Параметр1"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 2Параметр2"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 2Параметр3"/></div>
			</div>
			<div class="algorithmParam">
				<div><input class="form-control" align="center" type="text" name="param_name1" style="width: 100%" placeholder=" 2Параметр4"/></div>
			</div>
		</div>
		</div>
	</div>
	
	<h4 class="areaTitle" style="margin-bottom: 5px;">Выбор вопросов</h4>
	<div><table width="100%" class="questionTable" id="table">
		<tr height="15px"><th style="width: 48%"><p class="colomnTitle">Доступные вопросы</p></th><th style="width: 4%"></th><th style="width: 48%"><p class="colomnTitle">Добавленные вопросы</p></th></tr>
		<tr>
			<td class="colomn">
			<input type="text" placeholder="Поиск" class="form-control"
				   style="width: 95%; margin-left: 2.5%; margin-top: 3px;"
				   onkeyup=""/>
			</td>
			<td></td>
			<td>
				<input type="text" placeholder="Поиск" class="form-control"
					   style="width: 95%; margin-left: 2.5%; margin-top: 3px;"
					   onkeyup=""/>
			</td>
		</tr>
		<tr><td style="width: 48%" rowspan="2" id="from" class="colomn">
			<div id="q0" class="question" style="display: none;"></div>
			<div id="q1" class="btn btn-default bodyBtn question">Вопрос 1<input type="hidden" name="question_id" value="id"></div>
			<div id="q2" class="btn btn-default bodyBtn question">Вопрос 2<input type="hidden" name="question_id" value="id"></div>
			<div id="q3" class="btn btn-default bodyBtn question">Вопрос 33123afsdfgsdfggggggggas dasdasdвыаыфвафываф ывафывафыва фывафывафывафыв афвыафываgggg. В общем тут много всякого текста<input type="hidden" name="question_id" value="id"></div>
			<div id="q4" class="btn btn-default bodyBtn question">Вопрос 4<input type="hidden" name="question_id" value="id"></div>
			<div id="q5" class="btn btn-default bodyBtn question">Вопрос 5<input type="hidden" name="question_id" value="id"></div>
			<div id="q6" class="btn btn-default bodyBtn question">Вопрос 6<input type="hidden" name="question_id" value="id"></div>
			<div id="q7" class="btn btn-default bodyBtn question">Вопрос 7<input type="hidden" name="question_id" value="id"></div>
		</td>
		<td style="width: 4%; vertical-align: center; text-align: center">
			<i class="glyphicon glyphicon-transfer"></i>
		</td>
		<td style="width: 48%;" rowspan="2" id="to" class="colomn">
			<div id="q8" class="btn btn-default bodyBtn question">Вопрос 8<input type="hidden" name="question_id" value="id"></div>
			<div id="q9" class="btn btn-default bodyBtn question">Вопрос 9<input type="hidden" name="question_id" value="id"></div>
		</td></tr>
	</table></div>
	
	<p align="center"><button class="btn btn-default" style="margin-bottom: 4.2%; margin-top: 1.1%; letter-spacing: 1pt">
		Сохранить изменения</button></p>
	
</body>
</html>