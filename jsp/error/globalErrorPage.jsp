<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/navbar.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Ошибка</title>
    <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/tester/uploads.js"></script>
    
    <link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/tester.css" />
	
	<style>
		hr {
			border: none; /* Убираем границу */
			background-color: #DADADA; /* Цвет линии */
			color: #DADADA; /* Цвет линии для IE6-7 */
			height: 2px; /* Толщина линии */
			margin: auto 3%;
		}
	</style>
</head>
<body class="testerBody" style="cursor: default;">
	<% // move me to servlet!!
		// LOG.error()
	%>
	<h1 align="center" style="letter-spacing: 1pt">Ошибка №${pageContext.errorData.statusCode}</h1>
	<div style="margin: auto 10px" >
		<h3>Произошла ошибка во время выполнения запроса.</h3>
		<h3 style="margin: 0px">Попробуйте выполнить запрос позже</h3>
		<h3 style="margin: 8px auto">Возможная причина: &quot;${pageContext.errorData.throwable.localizedMessage}&quot;</h3>
		<h4 id="more" onclick="changeView();" class="viewButton">Более подробно: </h4>
	</div>
	<div id="more_view" class="hiddenText"><ul type="disc">
		<li type="square">Ошибка произошла по адресу: ${pageContext.errorData.requestURI}</li>
		<li type="square">Класс ошибки: ${pageContext.errorData.throwable}</li>
	</ul><hr/><ul type="disc">
		<% Throwable th = pageContext.getErrorData().getThrowable(); %>
		<% do{ %>
		<li><font style="color: #bb1111">Трассировка стека для: <%=th%></font></li>
			<%for(StackTraceElement elm : th.getStackTrace()){ %>
				<li> Класс:&ensp;<%=elm.getClassName() %><b>.</b><%=elm.getMethodName() %>()&emsp;:&thinsp;<%= elm.getLineNumber() %>стр.</li>
			<%}%>
			<%th =  th.getCause();
			if(th != null){ %>
				<li style="font-weight: bold;" type="circle">Причина:</li>
		<%}}while(th != null);%>
	</ul></div>

	<!--
	<div id="ifile">h</div>

	<iframe name="nothing" style="display: none;"></iframe>
	
	<form action="upload" enctype="multipart/form-data" method="POST" name="file" target="nothing">
		<input type="file" name="filePart" onchange="my();"/>
		<input type="text" name="fileName"/>
		<input type="submit"/>
	</form>-->
</body>

<script>
//	var str = 'P1315ABC123=4250452_-Пираты Карибского моря- - OST.mp3';
//	var uri = encodeURIComponent(str);
//	alert(str);
//	alert(uri);
	
//	$("h4").text(uri);

	function my( ){
		var form = $("form")[0];
		
	}


	function changeView(){
		var obj = document;
		if(obj.closed != true){
			$('#more_view').show(550);
			obj.closed = true;
		}
		else{
			$('#more_view').slideUp(550);
			obj.closed = false;
		}
//		$('#more_view').slideToggle(550);
//		$('#more_view').toggle(550);
	}
</script>
</html>