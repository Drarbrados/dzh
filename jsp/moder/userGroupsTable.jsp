<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
  <!--from navbar-->
  <link rel="stylesheet" href="css/bootstrap.css" />
  <link rel="stylesheet" href="css/tester.css" />

  <script type="text/javascript" src="js/tester/timerDown.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
  <!--from navbar-->

  <script type="text/javascript" src="js/tester/filters.js"></script>
  <script type="text/javascript" src="js/tester/tableSort.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/contextMenu.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css"/>
  <link rel="stylesheet" href="css/contextMenu.css" />
</head>
<body class="testerBody">
<br/>
<div style="margin: 15px;"></div>
<h1 align="center">Добавить группу: </h1><br/>


  <div style="margin: -15px;"></div>
  <h2 align="center">
    <c:if test="${rootGroup.visible}">
      <a style="color: orange;" href="showusergroups?id=${rootGroup.parentGroupId}">${rootGroup.name}</a>
    </c:if>
    <c:if test="${not rootGroup.visible}">
      <a href="showusergroups?id=${rootGroup.parentGroupId}">${rootGroup.name}</a>
    </c:if>
    <c:if test="${rootGroup.visible}">
      <i  style="font-size: 20px;" class="glyphicon glyphicon-minus wrenchIcon"
          id="${rootGroup.id}" onclick="iconclick(this, ${rootGroup.parentGroupId})"></i>
    </c:if>
    <c:if test="${not rootGroup.visible}">
      <i  style="font-size: 20px;" class="glyphicon glyphicon-plus wrenchIcon"
          id="${rootGroup.id}" onclick="iconclick(this, ${rootGroup.parentGroupId})"></i>
    </c:if>
  </h2>

  <c:set var="grouplist" value="${groups}" />
  <c:set var="count" value="0"/>
  <c:forEach items="${groups}" var="group">
    <c:set var="count" value="${count + 1}"/>
  <div style="margin-left: 33%; margin-top: -10px;">
    <h3 align="left">${count}.
      <c:if test="${group.visible eq true}">
        <a style="color: orange;" href="showusergroups?id=${group.id}">${group.name}</a>
      </c:if>
      <c:if test="${group.visible eq false}">
        <a href="showusergroups?id=${group.id}">${group.name}</a>
      </c:if>
      <c:if test="${group.visible eq true}">
        <i  style="font-size: 20px;" class="glyphicon glyphicon-minus wrenchIcon"
            id="${group.id}" onclick="iconclick(this, ${group.parentGroupId})"></i>
      </c:if>
      <c:if test="${group.visible eq false}">
        <i style="font-size: 20px;" class="glyphicon glyphicon-plus wrenchIcon"
           id="${group.id}" onclick="iconclick(this, ${group.parentGroupId})"></i>
      </c:if>
    </h3>
  </div>
  </c:forEach>

  <form action="showusergroups" method="post">
    <button id="plus" name="plus" style="display: none"></button>
    <button id="minus" name="minus" style="display: none"></button>
    <input type="hidden" name="id" id="id" value=""/>
  </form>

  <br/>
</body>

<script>
    function iconclick(icon, pgid) {
      if(icon.className == "glyphicon glyphicon-minus wrenchIcon") {
        document.getElementById("minus").value = icon.id;
        document.getElementById("id").value = pgid;
        document.getElementById("minus").click();
      } else {
        document.getElementById("plus").value = icon.id;
        document.getElementById("id").value = pgid;
        document.getElementById("plus").click();
      }
    }
</script>

</html>