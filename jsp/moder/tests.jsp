<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>

<html>
<head>
    <title>Tester</title>
</head>

<body class="testerBody" onclick="finishAll();">

		<br/>
		<div style="margin: 15px;"></div>
		<h1 align="center">
			<c:out value="${sessionScope.user.fname} ${sessionScope.user.lname}"/></h1>


		<div style="margin-top: -15px;" align="center">
			<c:set var="t" value="${requestScope.titleTest}" />
			<h2 id="link${t.nodeValue.id}">
				<a href="showtests?id=${titleTest.parentNode.nodeValue.id}">
					<text>${t.nodeValue.title}</text></a>
				<c:if test="${not t.nodeAccessible}">
					<i  class="glyphicon glyphicon-wrench
					wrenchIcon wrenchIcon-disabled"
						onclick=""></i>
				</c:if>
				<c:if test="${t.nodeAccessible}">
					<i  class="glyphicon glyphicon-wrench
					wrenchIcon wrenchIcon"
						onclick="editTitle('link${t.nodeValue.id}');"></i>
				</c:if>
			</h2>
			<input id="link${t.nodeValue.id}_x" type="text"
				   style="display: none; margin-top: 25px; width: 50%;
				   font-size: 25px; height: 40px;"
				   onkeypress="editFinish('link${t.nodeValue.id}', event);"
				   class="form-control"/>
		</div>

		<form action="showtests" method="post">
			<c:set var="tests" value="${requestScope.testList}" />
			<c:set var="i" value="1"/>
			<c:forEach items="${requestScope.testList}" var="node">
				<table style="margin-left: 25%; width: 50%; margin-top: -10px;">
					<tr><td>
					<c:choose>
						<c:when test="${node.nodeValue.test == false}">
							<c:set var="prefix" value=""/>
						</c:when>
						<c:otherwise>
							<c:set var="prefix" value="[Тест]"/>
						</c:otherwise>
					</c:choose>
					<h3 id="link${node.nodeValue.id}" align="left">${i}. ${prefix}
						<a href="showtests?id=${node.nodeValue.id}">
						<text>${node.nodeValue.title}</text></a>
						<i class="glyphicon glyphicon-wrench wrenchIcon"
							onclick="editTitle('link${node.nodeValue.id}');"></i>
					</h3>
					<input id="link${node.nodeValue.id}_x" type="text"
						   style="display: none; width: 100%; margin-top: 20px;
						   height: 35px; font-size: 22px;"
						   onkeypress="editFinish('link${node.nodeValue.id}', event);"
						   class="form-control"/>
					</td></tr>
				</table>
				<c:set var="i" value="${i + 1}"/>
			</c:forEach>
			<br/>
			<button style="display: none" id="changeName" name="changeName" value="test"></button>
			<c:if test="${t.nodeAccessible}">
				<div align="center">
					<button class="btn btn-default" name="addTopic" style="min-width: 200px; margin: 2px;">
						Добавить Раздел</button><br>
					<button class="btn btn-default" name="addTest" style="min-width: 200px; margin: 2px;">
						Добавить Тест</button>
				</div>
			</c:if>
			<input type="hidden" name="modId" id="modId"/>
		</form>

	<br/><br/><br/>

	<input type="hidden" id="pageId" value="mainPage"/>
</body>

	<script>
		function editTitle(id) {
			var h_element = document.getElementById(id);
			var input_element = document.getElementById(id + "_x");
			h_element.style.display = "none";
			input_element.style.display = "";
			input_element.value = h_element.childNodes[1].childNodes[1].innerHTML;
		}
		function editFinish(id, event) {
			var h_element = document.getElementById(id);
			var input_element = document.getElementById(id + "_x");
			if(event.keyCode == 13) {
				h_element.style.display = "";
				input_element.style.display = "none";
				document.getElementById("modId").value = id.replace(new RegExp("link"), "");
				document.getElementById("changeName").value = input_element.value;
			}
		}
	</script>


</html>
