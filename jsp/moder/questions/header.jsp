<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
  <script type="text/javascript"
          src="js/tester/edit-questions/edit-question.js"></script>
  <script type="text/javascript" src="js/tester/uploads.js"></script>
</head>
<body>


  <form method="get" action="editquestion" id="selectQuestionTypeForm">
    <input type="hidden" id="questionId_" name="questionId"
           value="<c:out value="${question.id}" default="0"/>"/>
  </form>

  <input type="hidden" id="userFname" value="${sessionScope.user.fname}"/>
  <input type="hidden" id="userLname" value="${sessionScope.user.lname}"/>

  <iframe name="nothing" style="display: none;"></iframe>
  <form action="upload" enctype="multipart/form-data" method="POST" id="fileUpload" name="file" target="nothing">
    <input id="file" type="file" name="filePart" style="display: none;" onchange="fileUpload();"/>
    <input type="hidden" name="fileName" id="fileName"/>
    <input type="hidden" id="fileTo" value=""/>
  </form>

  <input type="hidden" id="lastImageName" value=""/>

  <br/>
  <div style="margin-top: -15px;"></div>
  <c:choose>
    <c:when test="${question == null}">
      <h1 align="center">Создание Вопроса</h1>
    </c:when>
    <c:otherwise>
      <h1 align="center">Редактирование Вопроса</h1>
    </c:otherwise>
  </c:choose>

  <br/>

  <table style="width: 90%; margin-left: 5%;">
    <tr>
      <td style="width: 25%; background-color: #f0f0f0;
          border-bottom-left-radius: 15px;
          border-top-left-radius: 15px;">
        <h4 class="areaTitleInTable">Вернуться в раздел (тест):</h4>
      </td>
      <td style="width: 33%; background-color: #f0f0f0;">
        <form style="padding: 0; margin: 0;" action="questionlist" method="post"
              id="to_question_list">
          <a style="cursor: pointer;" onclick="document.forms['to_question_list'].submit();">
            <h4 style="font-weight: bold;" class="areaTitleInTable">${test.title}</h4>
          </a>
          <input type="hidden" name="testId" value="${test.id}"/>
        </form>
      </td>
      <td style="width: 3%; background-color: #f0f0f0;"></td>
      <td style="width: 14%; background-color: #f0f0f0;">
        <h4 class="areaTitleInTable">Тип вопроса:</h4>
      </td>
      <td style="width: 25%; background-color: #f0f0f0;
          border-bottom-right-radius: 15px;
          border-top-right-radius: 15px;">
        <c:choose>
          <c:when test="${question == null}">
            <select style="border-bottom-right-radius: 15px;
                           border-top-right-radius: 15px;
                           -moz-appearance: none;"
                    form="selectQuestionTypeForm" class="form-control" name="questionType"
                    id="questionType">
              <option value="1">С одним ответом</option>
              <option value="2">Множественный</option>
              <option value="3">Строковый</option>
              <option value="4">Соотношение</option>
            </select>
          </c:when>
          <c:otherwise>
            <h4 id="qTypeField" class="areaTitleInTable">${questionType}</h4>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
  <br/>
  <h4 style="margin-left: 5%;" class="areaTitle">Параметры вопроса:</h4>
  <table style="width: 45%; margin-left: 6%;">
    <tr>
      <td style="width: 35%;">
        <h5 class="areaTitle">Создан:</h5>
      </td>
      <td style="width: 40%;">
        <h5 class="areaTitle">${question.creator.fname} ${question.creator.lname}</h5>
      </td>
      <td style="width: 25%;">
        <h5 class="areaTitle">[${question.createDate}]</h5>
      </td>
    </tr>
    <tr>
      <td>
        <h5 class="areaTitle">Последнее изменение:</h5>
      </td>
      <td>
        <h5 class="areaTitle">${question.modifier.fname} ${question.modifier.lname}</h5>
      </td>
      <td>
        <h5 class="areaTitle">[${question.modifyDate}]</h5>
      </td>
    </tr>
  </table>



  <form method="post" action="editquestion" id="editQuestionForm">

  <input type="hidden" id="maxAnswersNum" name="maxAnswersNum" value="0"/>
  <input type="hidden" id="questionId" name="questionId"
         value="<c:out value="${question.id}" default="0"/>"/>


  <div align="center">

    <br/>
    <input class="form-control" type="text" name="questionText"
           maxlength="200" placeholder="Текст вопроса" id="questionText" autofocus="autofocus"
           value="${question.text}" style="width: 90%;" required="required"
           onchange="updatePreview();"/>
    <table style="width: 90%; margin-top: 5px;">
      <tr>
        <td style="width: 55%;">
          <button class="form-control" id="addQuestionImage" type="button"
                  style="width: auto; " onclick="$('#fileTo').val(this.id); $('#file').click();">
            Прикрепить Изображение</button>
          <input type="hidden" id="_QuestionImage" name="_QuestionImage" value=""/>
          <h4 style="margin-top: 0; display: none;" id="imageConfigs">
            <span id="imageName"></span>
            <button class="btn btn-default btn-lg transparentCloseBtn"
                    style="padding: 2px; padding-right: 15px;" type="button"
                    onclick="deleteImage(this);" id="deleteQuestionImage">x</button>
            <span>размер:</span>
            <input type="text" style="width: 60px;"
                   id="imageWidth" onchange="changeImageSize(this.id);"
                   name="questionImageSize"/>
          </h4>
        </td>
        <td style="width: 30%;"></td>
        <td style="width: 15%;">
          <table style="width: 100%;">
            <tr>
              <td style="width:70%; text-align: right; padding-right: 7px;">Оценка: </td>
              <td style="width:30%;">
                <input type="text" id="mark" name="mark"
                       value="1" class="form-control" required="required"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </div>

    <br/>
    <h4 style="margin-left: 5%;" class="areaTitle">Параметры ответов:</h4>

  <div align="center" id="answers"></div>

  <br/><br/>
  <button class="form-control testerBodyBtnInverse" type="button" name="newQuestion"
          style="margin-left: 5%; width: auto" onclick="addAnswerForm();">Добавить Ответ</button>
  <br/><br/><br/><br/>

  <div style="margin-left: 1.5%; opacity: 0.99;">
    <table style="width: 98%;" id="placeTypeTable">
      <tr>
        <td style="width: 65%;"></td>
        <td style="width: 35%;">
          <table style="width: 100%;">
            <tr>
              <td style="width:45%;"></td>
              <td style="width:4%;">
                              <span class="form-control iconSpan">
                                  <i class="glyphicon glyphicon-th-large"></i>
                              </span>
              </td>
              <td style="width:1%;"></td>
              <td style="width:50%; opacity: 0.99">
                <div align="left">
                  <select class="form-control" name="answersInRow" id="placeType"
                          onchange="updatePreview();">
                    <option value = "1">по одному</option>
                    <option value = "2">по два</option>
                    <option value = "3">по три</option>
                    <option value = "4">по четыре</option>
                    <option value = "5">по пять</option>
                  </select>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table style="width: 98%; margin-top: 5px;">
      <tr>
        <td style="width: 65%;"></td>
        <td style="width: 35%;">
          <table style="width: 100%;">
            <tr>
              <td style="width:45%;"></td>
              <td style="width:4%;">
                              <span class="form-control iconSpan">
                                  <i class="glyphicon glyphicon-resize-horizontal" id="ipw"></i>
                              </span>
              </td>
              <td style="width:1%;"></td>
              <td style="width:50%;">
                <div align="left">
                  <input type="range" id="pageWidth" min="5" max="99" step="1"
                         class="form-control" value="99" name="answerAreaSize"
                         onmousemove="resizeHorizontal();"
                         onmouseout="setIcons();"/>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table style="width: 98%; margin-top: 5px; margin-bottom: -308px;"
           id="leftOffsetTable">
      <tr>
        <td style="width: 65%;"></td>
        <td style="width: 35%;">
          <table style="width: 100%;">
            <tr>
              <td style="width:45%;"></td>
              <td style="width:4%;">
                              <span class="form-control iconSpan">
                                  <i class="glyphicon glyphicon-transfer" id="ilo"></i>
                              </span>
              </td>
              <td style="width:1%;"></td>
              <td style="width:50%;">
                <div align="left">
                  <input type="range" id="leftOffset" min="0" max="95"
                         class="form-control" value="0" name="answerAreaLeftOffset"
                         onmousemove="moveQuestionsArea();"
                         onmouseout="setIcons();"/>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  </form>


  <br/><br/><br/><br/><br/><br/>
  <h3 align="center">Предпросмотр</h3>
  <div align="center" style="opacity: 1; z-index: 1; margin-left: 1%;
    width: 98%; border: 2px dashed #50a0a0; min-height: 130px;" id="preview">
    <h1 align="center" id="usernamePreview"></h1>
    <img style="border: none; display: none; border: 5px solid #50a0a0;" width="50%"
         src="#" id="questionImg"/>
    <h2 style="margin-top: 5px;" align="center" id="questionTextPreview"></h2><br/>
    <div id="reviewAnswers"></div>
    <div align="center">
      <h3><button style="width: 150px; margin: 2px; padding: 3px; padding-left: 7px; padding-right: 7px;"
                  class="btn btn-default btn-lg" id="answerComfirm"
                  type="button" name="test">ответить</button></h3>
      <h3 style="margin-top: -10px;"><button style="width: 150px; margin: 2px; padding: 3px; padding-left: 7px; padding-right: 7px;"
                                             class="btn btn-default btn-lg"
                                             type="button" name="test">пропустить</button></h3>
    </div>
  </div>

  <div align="center">
    <br/><br/>
    <button class="btn btn-default" type="submit" name="newQuestion"
             form="editQuestionForm" style="width: auto;">Сохранить Изменения</button>
  </div>

  <br/><br/>

</body>

<script>

  if(document.getElementById("qTypeField") != null) {
    var val = document.getElementById("qTypeField").innerHTML;
    switch (val) {
      case '1': val = "С одним ответом"; break;
      case '2': val = "Множественный"; break;
      case '3': val = "Строковый"; break;
      case '4': val = "Соотношение"; break;
      default: val = "some error"; break;
    }
    document.getElementById("qTypeField").innerHTML = val;
  }

  $("#questionType").val("${questionType}");
  $("#questionType").change(function() {
    if($("#questionType").val() != "${questionType}") {
      document.forms['selectQuestionTypeForm'].submit();
    }
  });
  function fileUpload() {
    var id = document.getElementById("fileTo").value;
    var button = document.getElementById(id);
    _addImage(button, $("#file").val(), "${questionType}");
    $("#fileName").val($("#file").val());
    document.forms['fileUpload'].submit();
  }
  function receiveAttributes() {
    if("${question.customization.answersInRow}" != null &&
            "${question.customization.answersInRow}" != "") {
      $("#placeType").val("${question.customization.answersInRow}");
      $("#pageWidth").val("${question.customization.answerAreaSize}");
      $("#leftOffset").val("${question.customization.answerAreaLeftOffset}");
      _resizeHorizontalInit();
      $("#mark").val("${question.prize}");
      if("${question.pictureId}" != "") {
        var id = "perm/".concat("${question.pictureId}");
        document.getElementById("_QuestionImage").value = id;
//        alert(id);
        _addImage(document.getElementById("addQuestionImage"), id, "${questionType}");
      }
      $("#imageWidth").val("${question.pictureSize}%");
      _changeImageSize("imageWidth", "${questionType}");
    }
  }

  receiveAttributes();
</script>


</html>

