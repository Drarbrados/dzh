<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>

<%@ include file="header.jsp"%>

<c:choose>
  <c:when test="${requestScope.questionType == 1 or requestScope.questionType == null}">
    <%@ include file="editOneOfSeveralQuestion.jsp"%>
  </c:when>
  <c:when test="${requestScope.questionType == 2}">
    <%@ include file="editMultipleQuestion.jsp"%>
  </c:when>
  <c:when test="${requestScope.questionType == 3}">
    <%@ include file="editStringQuestion.jsp"%>
  </c:when>
  <c:when test="${requestScope.questionType == 4}">
    <%@ include file="editCorrelateQuestion.jsp"%>
  </c:when>
</c:choose>

<html>
<head>
</head>
<body class="testerBody">


</body>
</html>