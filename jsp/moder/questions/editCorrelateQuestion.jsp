<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
  <script type="text/javascript"
          src="js/tester/edit-questions/edit-correlate-question.js"></script>
</head>

<body onresize="fixColumnHeightsInPreview();">
</body>
</html>

<script>
  $("#placeType").val(2);
  $("#placeTypeTable").hide();
  $("#pageWidth").val("50%");
  $("#leftOffsetTable").css("marginBottom", "-268");
  function receiveAttributes() {
    var answers = [];
    var answerPictures = [];
    var answerPictureSizes = [];
    <c:forEach items="${question.availableAnswers}" var="answer">
      answers.push("${answer.text}");
      answerPictures.push("${answer.pictureId}");
      answerPictureSizes.push("${answer.pictureSize}");
    </c:forEach>
    for(var i = 0; i < answers.length / 2; i++) {
      addAnswerForm();
    }
    for(var i = 0; i < answers.length; i++) {
      $("#answerText" + (i + 1)).val(answers[i]);
      if(answerPictures[i] != "") {
        var id = "perm/".concat(answerPictures[i]);
        document.getElementById("_AnswerImage" + (i + 1)).value = id;
        _addImage(document.getElementById("addAnswerImage" + (i + 1)), id, 3);
        document.getElementById("imageSize" + (i + 1)).value = answerPictureSizes[i];
        changeImageSize("imageSize" + (i + 1));
      }
    }
  }
  receiveAttributes();
  updatePreview();
</script>