<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/navbar.jsp"%>

<html>

<head>
  <script type="text/javascript" src="js/tester/filters.js"></script>
  <script type="text/javascript" src="js/tester/tableSort.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/contextMenu.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css"/>
  <link rel="stylesheet" href="css/contextMenu.css" />
</head>
<body class="testerBody">

<div style="width: 60%; height: 80%; background-color: #005050;
						position: fixed; margin-left: 20%; z-index: 5555;
						border-top-left-radius: 18px 15px;
						border-top-right-radius: 18px 15px;
						border-bottom-right-radius: 18px 15px;
						border-bottom-left-radius: 18px 15px;
						display: none;" id="frame">
  <iframe id="iframe" style="width: 98%; height: 90%; margin-left: 1%; margin-top: 1%;
				border-top-left-radius: 18px 15px;
				border-top-right-radius: 18px 15px;
				border-bottom-right-radius: 18px 15px;
				border-bottom-left-radius: 18px 15px;"
          frameborder="0" scrolling="yes"
          marginheight="0" marginwidth="0"
          src="http://localhost:8080/testerbelsut/showusergroups?tid=">
  </iframe>
  <button class="form-control testerBodyBtnInverse" style="width: 20%; height: 6%; margin-left: 40%;
				margin-top: 1%;"
          onclick="document.getElementById('frame').style.display = 'none';
                   document.getElementById('gshadow').style.display = 'none';">
    Готово</button>
  <br/><br/><br/>
</div>


<br/>
<div style="margin: 15px;"></div>
<h1 align="center">Управление разделами и тестами</h1><br/>

<h4 class="areaTitle">Дерево доступных резделов / тестов:</h4><br/>




<form method="post" action="questionlist" id="go_to_questions">

  <table class="statsTable" id="table">
    <tr>
      <th class="statsTableInsetNeighborTh"></th>
      <th style="cursor: default;" class="statsTableInsetTh">
        <input class="form-control" type="text" name="testFilter"
               maxlength="40" placeholder="Поиск" id="testFilter"
               value="" style="width: 100%;" onkeyup="colorFilter(this.value, 1, 1)"/>
      </th>
      <th class="statsTableInsetNeighborTh" style="position: relative">
        <h4 class="areaTitle"
            style="position: absolute;
            left: 15px; bottom: 5px;
            word-wrap: normal;
            white-space: nowrap">
            <table style="width: 250px;">
              <tr>
                <td style="background-color: #fafafa; border: none; padding: 0;">
                  <div class="squaredThree-inverse-bright">
                    <input style="display: none;" onclick="checkBoxSelect(this);"
                           type="checkbox" id="chkBoxTest"/>
                    <label style="margin-left: 0px;" for="chkBoxTest"></label>
                    <span style="margin-left: 30px; margin-top: 5px; color: #207070;"
                            onclick="document.getElementById('chkBoxTest').click();">
                      Тесты
                    </span>
                  </div>
                </td>
                <td style="background-color: #fafafa; border: none; padding: 0;">
                  <div class="squaredThree-inverse-bright">
                    <input style="display: none;" onclick="checkBoxSelect(this);"
                           type="checkbox" id="chkBoxTopic"/>
                    <label for="chkBoxTopic"></label>
                    <span style="margin-left: 30px; margin-top: 5px!important;"
                          onclick="document.getElementById('chkBoxTopic').click();">
                      Разделы
                    </span>
                  </div>
                </td>
              </tr>
            </table>
        </h4>
      </th>
      <th class="statsTableInsetNeighborTh"></th>
      <th class="statsTableInsetNeighborTh"></th>
    </tr>
    <tr>
      <th style="cursor: default;">Id</th>
      <th style="cursor: default;">Название</th>
      <th style="cursor: default;">Вопросы</th>
      <th style="cursor: default;">Дата Создания</th>
      <th style="cursor: default;">Создатель</th>
      <th style="cursor: default;">Открыт</th>
    </tr>
    <c:set var="i" value="${0}"/>
    <c:forEach items="${testList}" var="test">

      <c:if test="${test.accessible}">
        <tr>
          <c:if test="${test.type eq 'раздел'}">
            <td style="cursor: pointer; color: #404040" onmousemove="menuid=${test.id}" name="tds">${test.id}</td>
            <td title="Настроить" class="testTableRow" style="cursor: pointer; color: #404040" onmousemove="menuid=${test.id}" onmouseup="submitForm2(${test.parentId}, event);" id="testTitle${i}"name="tds">${test.title}</td>
            <td title="Управлять Вопросами" class="testTableRow" style="cursor: pointer; color: #404040" onmousemove="menuid=${test.id}" onmouseup="submitForm(${test.id}, event);" name="tds">${test.questionCount}</td>
            <td title="Удалить" class="testTableDeleteRow" style="cursor: pointer; color: #404040" onmousemove="menuid=${test.id}" onmouseup="alert('Ups');" name="tds">test.createDate</td>
            <td style="cursor: pointer; color: #404040" onmousemove="menuid=${test.id}"name="tds">test.creator</td>
            <td title="Управлять Доступом" class="testTableRow" style="cursor: pointer; color: #404040" onmousemove="menuid=${test.id}" onclick="menuid=${test.id}; manageGroups();" name="tds">0</td>
          </c:if>
          <c:if test="${test.type eq 'тест'}">
            <td style="cursor: pointer; color: #207070" onmousemove="menuid=${test.id}"name="tds">${test.id}</td>
            <td title="Настроить" class="testTableRow" style="cursor: pointer; color: #207070" onmousemove="menuid=${test.id}" onmouseup="submitForm2(${test.id}, event);" id="testTitle${i}"name="tds">${test.title}</td>
            <td title="Управлять Вопросами" class="testTableRow" style="cursor: pointer; color: #207070" onmousemove="menuid=${test.id}" onmouseup="submitForm(${test.id}, event);" name="tds">${test.questionCount}</td>
            <td title="Удалить" class="testTableDeleteRow" style="cursor: pointer; color: #207070" onmousemove="menuid=${test.id}" onmouseup="alert('Ups');" name="tds">test.createDate</td>
            <td style="cursor: pointer; color: #207070" onmousemove="menuid=${test.id}" name="tds">test.creator</td>
            <td title="Управлять Доступом" class="testTableRow" style="cursor: pointer; color: #207070" onmousemove="menuid=${test.id}" onclick="menuid=${test.id}; manageGroups();" name="tds">0</td>
          </c:if>
        </tr>
      </c:if>

      <c:if test="${not test.accessible}">
        <tr>
          <td style="cursor: pointer; color: #404040" class="notAccessible">${test.id}</td>
          <td style="cursor: pointer; color: #404040" id="testTitle${i}" class="notAccessible" name="tds">${test.title}</td>
          <td style="cursor: pointer; color: #404040;" class="notAccessible">${test.questionCount}</td>
          <td style="cursor: pointer; color: #404040" class="notAccessible">test.createDate</td>
          <td style="cursor: pointer; color: #404040;" class="notAccessible">test.creator</td>
          <td style="cursor: pointer; color: #404040" class="notAccessible">0</td>
        </tr>
      </c:if>
      <c:set var="i" value="${i + 1}"/>
    </c:forEach>
  </table>
  <input type="hidden" id="questionId" name="questionId" value=""/>
  <input type="hidden" id="testId" name="testId" value=""/>
</form>

<br/><br/>

<input type="hidden" id="pageId" value="questionListPage"/>
</body>


<script>

  function returnFalse() {
    return false;
  }

  var menuid = -1;

  function manageGroups() {
    if(menuid != -1) {
      document.getElementById("iframe").src =
              "../testerbelsut/showusergroups?tid=" + menuid;
      document.getElementById("frame").style.display = "";
      document.getElementById("gshadow").style.display = "";
    }
  }


  var menu = [{
    name: 'Управление Доступом',
    img: 'imgs/logo32.png',
    title: 'open to group',
    fun: function () {
      manageGroups();
    }
  }, {
    name: 'Настроить',
    img: 'imgs/logo32.png',
    title: 'update button',
    fun: function () {
      submitForm2(menuid, 1);
    }
  }, {
    name: 'Удалить',
    img: 'imgs/logo32.png',
    title: 'create button',
    fun: function () {
      alert('Ups');
    }
  },{
      name: 'Управление Вопросами',
      img: 'imgs/logo32.png',
      title: 'create button',
      fun: function () {
        submitForm(menuid, 1);
     }
  }];

  $("td[name=tds]").contextMenu(menu,{triggerOn:'contextmenu'});


  document.getElementById("chkBoxTopic").checked = true;
  document.getElementById("chkBoxTest").checked = true;
  doFilters();

  function checkBoxSelect(onclickedChkBox) {
    if(document.getElementById("chkBoxTest") == onclickedChkBox &&
            onclickedChkBox.checked == false) {
      if(document.getElementById("chkBoxTopic").checked == false) {
        onclickedChkBox.checked = true;
      } else {
        onclickedChkBox.value = 'off';
        doFilters();
      }
    } else if(document.getElementById("chkBoxTopic") == onclickedChkBox &&
            onclickedChkBox.checked == false) {
      if(document.getElementById("chkBoxTest").checked == false) {
        onclickedChkBox.checked = true;
      } else {
        onclickedChkBox.value = 'off';
        doFilters();
      }
    } else {
      onclickedChkBox.value = 'on';
      doFilters();
    }
  }

  function testModeFilter() {
    var cellIndex = 5;
    var table = document.getElementById("table");
    for (var r = 2; r < table.rows.length; r++) {
      var a = 1;
      var row = table.rows[r].cells[cellIndex].style.color;
      //alert(row);
      if(document.getElementById("chkBoxTest").checked == false &&
          row == "rgb(32, 112, 112)") {
        a = 0;
      } else if(document.getElementById("chkBoxTopic").checked == false &&
          row == "rgb(64, 64, 64)") {
        a = 0;
      }
      if (a == 0) {
        table.rows[r].style.display = 'none';
      } else {
        table.rows[r].style.display = '';
      }
    }
  }

  function doFilters() {
    colorFilter($("#testFilter").val(), 1, 1);
    testModeFilter();
  }

  function submitForm(tid, event) {
    if(event == 1 || event.which == 1) {
      $("#testId").val(tid);
      document.forms['go_to_questions'].submit();
    }
  }
  function submitForm2(tid, event) {
    if(event == 1 || event.which == 1) {
      $("#testId").val(tid);
      location.replace("../testerbelsut/showtests?id=" + tid);
    }
  }

  function setHierarchyOffsets() {
    var i = 0;
    <c:forEach items="${testList}" var="test">
      document.getElementById("testTitle" + i).style.
          paddingLeft = "" + 7 * "${test.offset}" + "px";
      i++;
    </c:forEach>
    document.getElementById("testTitle0").style.paddingLeft = "2px";
  }
  setHierarchyOffsets();

  $("#questionTextFilter").keyup(function() {
    var availableTags = [];
    <c:forEach items="${requestScope.testList}" var="test">
    availableTags.push("${test.title}");
    </c:forEach>
    $("#questionTextFilter").autocomplete({
      source: function(request, callback){
        var searchParam  = request.term;
        init(searchParam, callback);
      },
      minLength: 1,
      select: function (event, ui) {
        $("#testId").val(ui.item.value);
        $(this).val(ui.item.label);
        return false;
      }
    });
  });

  function init(query, callback) {
    if(init.idArray == null) {
      init.idArray = [];
      init.nameArray = [];
      <c:forEach items="${requestScope.testList}" var="test">
      init.idArray.push("${test.id}");
      init.nameArray.push("${test.title}");
      </c:forEach>
    }
    var response = [];
    for(var i = 0; i < init.idArray.length; i++)  {
      var a = 1;
      for (var j = 0; j < query.length; j++) {
        if (init.idArray[i].indexOf(query) < 0) {
          a = 0;
          break;
        }
      }
      if(a == 1) {
        var obj = {label: init.nameArray[i], value: init.idArray[i]};
        response.push(obj);
      }
    }
    callback(response);
  }

  function submitFormAdd() {
    if($("#questionTextFilter").val() == "") {
      alert("Выберите тест привязки");
    } else {
      $("#questionId").val(0);
      document.forms['edit_q_form'].submit();
    }
  }

  function submitFormEdit(qid, tid) {
    $("#questionId").val(qid);
    $("#testId").val(tid);
    document.forms['edit_q_form'].submit();
  }

</script>


</html>