<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <script type="text/javascript"
            src="js/tester/edit-questions/edit-string-question.js"></script>
</head>

<body onresize="fixColumnHeightsInPreview();">
</body>
</html>

<script>
    $("#placeTypeTable").hide();
    $("#pageWidth").val("50%");
    $("#leftOffsetTable").css("marginBottom", "-268");
    function receiveAttributes() {
        var answers = [];
        <c:forEach items="${question.availableAnswers}" var="answer">
            answers.push("${answer.text}");
        </c:forEach>
        for(var i = 0; i < answers.length; i++) {
            addAnswerForm();
            $("#answerText" + (i + 1)).val(answers[i]);
        }
    }
    receiveAttributes();
    updatePreview();
</script>