<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<html>
<head>
  <!--from navbar-->
  <link rel="stylesheet" href="css/bootstrap.css" />
  <link rel="stylesheet" href="css/tester.css" />

  <script type="text/javascript" src="js/tester/timerDown.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
  <!--from navbar-->


  <script type="text/javascript" src="js/tester/filters.js"></script>
  <script type="text/javascript" src="js/tester/tableSort.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.session.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/contextMenu.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css" />
  <link rel="stylesheet" href="css/contextMenu.css" />
</head>
<body class="testerBody">
<br/>
<div style="margin: 15px;"></div>
<h1 align="center">Выбор Раздела</h1>

<form method="get" action="questionlist" id="back_to_tests">
  <table style="margin-left: 5%; width: 90%; margin-bottom: 5px;">
    <tr>
      <td style="width: 35%;"></td>
      <td style="width: 30%;"></td>
      <td style="width: 5%;"></td>
      <td style="width: 25%;"></td>
      <td style="width: 5%;"></td>
    </tr>
  </table>
</form>

<form method="get" action="topictreeframe" id="change_question">
  <input name="tid" value="" type="hidden"/>
  <table style="margin-left: 5%; width: 90%; margin-bottom: 5px;">
    <%-- Parent tests output --%>
    <c:forEach items="${testParents}" var="currentTest">
      <tr>
        <td style="width: 35%;"></td>
        <c:if test="${currentTest.accessible}">
          <td style="width: 35%;">
            <a style="cursor: pointer;" onclick="changeQuestion(${currentTest.id});">
              <h4 class="areaTitleInTable">${currentTest.title}</h4>
            </a>
          </td>
        </c:if>
        <c:if test="${not currentTest.accessible}">
          <td style="width: 35%;" class="notAccessible_questions">
            <a style="cursor: pointer;" onclick="changeQuestion(${currentTest.id});">
              <h4 class="areaTitleInTable">${currentTest.title}</h4>
            </a>
          </td>
        </c:if>
        <td style="width: 5%;"></td>
        <td style="width: 25%;"></td>
      </tr>
    </c:forEach>
    <%-- Current test output --%>
    <c:if test="${test.accessible}">
      <tr>
        <td style="width: 35%; background-color: #f0f0f0;
            border-bottom-left-radius: 15px;
            border-top-left-radius: 15px;">
          <h4 class="areaTitleInTable">Выбранный ${test.test ? "тест" : "раздел"}</h4>
        </td>
        <td style="width: 35%; background-color: #f0f0f0">
          <a style="cursor: pointer;" onclick="changeQuestion(${test.id});">
            <h4 style="margin-left: 10px; font-weight: bold;"
                class="areaTitleInTable">${test.title}</h4>
          </a>
        </td>
        <td style="width: 5%; background-color: #f0f0f0"></td>
        <td style="width: 25%; padding-bottom: 3px; background-color: #f0f0f0;
              border-bottom-right-radius: 15px;
              border-top-right-radius: 15px;">
          <button class="btn btn-default" name="newQuestion" onclick="submitFormAdd();"
                  style="width: 100%; height: auto;
                    border-bottom-right-radius: 15px;
                    border-top-right-radius: 15px;">
            Вопросов: ${fn:length(questionList)}
          </button>
        </td>
      </tr>
    </c:if>
    <c:if test="${not test.accessible}">
      <tr>
        <td style="width: 35%; background-color: #f0f0f0;
          border-bottom-left-radius: 15px;
          border-top-left-radius: 15px;">
          <h4 class="areaTitleInTable">Выбранный ${test.test ? "тест" : "раздел"}:</h4>
        </td>
        <td style="width: 35%; background-color: #f0f0f0" class="notAccessible_questions">
          <a style="cursor: pointer;">
            <h4 style="margin-left: 10px; font-weight: bold;"
                class="areaTitleInTable">${test.title}</h4>
          </a>
        </td>
        <td style="width: 5%; background-color: #f0f0f0"></td>
        <td style="width: 25%; padding-bottom: 3px; background-color: #f0f0f0;
            border-bottom-right-radius: 15px;
            border-top-right-radius: 15px;">
         <%-- <button class="btn btn-primary" name="newQuestion"
                  style="width: 100%; height: auto;
                  border-bottom-right-radius: 15px;
                  border-top-right-radius: 15px;">
            вавава
          </button> --%>
        </td>
      </tr>
    </c:if>
      <%-- Child tests output --%>
      <c:forEach items="${testChildrens}" var="currentTest">
      <tr>
        <td></td>
        <td>
          <a style="cursor: pointer;" onclick="changeQuestion(${currentTest.id});">
            <h4 style="margin-left: 20px;" class="areaTitleInTable">${currentTest.title}</h4>
          </a>
        </td>
        <td></td>
        <td></td>
      </tr>
    </c:forEach>
  </table><br/>
</form>

<c:forEach items="${questionList}" var="question">
  <input type="hidden" name="ifr_questions" id="${question.id}" value="${question.text}"/>
</c:forEach>

<input type="hidden" id="ifr_test" value="${sessionScope.frameTest.title}"/>

</body>


<script>

  var menu = [{
    name: 'create',
    img: 'imgs/logo32.png',
    title: 'create button',
    fun: function () {
      alert('i am add button')
    }
  }, {
    name: 'update',
    img: 'imgs/logo32.png',
    title: 'update button',
    fun: function () {
      alert('i am update button')
    }
  }, {
    name: 'delete',
    img: 'imgs/logo32.png',
    title: 'create button',
    fun: function () {
      alert('i am delete button')
    }
  }];

  $("td[name=tds]").contextMenu(menu,{triggerOn:'contextmenu'});


  $("#questionTextFilter").keyup(function() {
    var availableTags = [];
    <c:forEach items="${requestScope.testList}" var="test">
    availableTags.push("${test.title}");
    </c:forEach>
    $("#questionTextFilter").autocomplete({
      source: function(request, callback){
        var searchParam  = request.term;
        init(searchParam, callback);
      },
      minLength: 1,
      select: function (event, ui) {
        $("#tid").val(ui.item.value);
        $(this).val(ui.item.label);
        return false;
      }
    });
  });

  function init(query, callback) {
    if(init.idArray == null) {
      init.idArray = [];
      init.nameArray = [];
      <c:forEach items="${requestScope.testList}" var="test">
      init.idArray.push("${test.id}");
      init.nameArray.push("${test.title}");
      </c:forEach>
    }
    var response = [];
    for(var i = 0; i < init.idArray.length; i++)  {
      var a = 1;
      for (var j = 0; j < query.length; j++) {
        if (init.idArray[i].indexOf(query) < 0) {
          a = 0;
          break;
        }
      }
      if(a == 1) {
        var obj = {label: init.nameArray[i], value: init.idArray[i]};
        response.push(obj);
      }
    }
    callback(response);
  }

  function submitFormAdd() {
    $("#questionId").val(0);
    document.forms['edit_q_form'].submit();
  }

  function submitFormEdit(qid, qtype) {
    $("#questionId").val(qid);
    $("#questionType").val(qtype);
    $("#tid").val(${test.id});
    document.forms['edit_q_form'].submit();
  }

  function changeQuestion(tid) {
    document.getElementsByName("tid")[0].value = tid;
    document.forms['change_question'].submit();
  }
  function backToTests() {
    document.forms['back_to_tests'].submit();
  }

</script>


</html>