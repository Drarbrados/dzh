<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
  <script type="text/javascript"
          src="js/tester/edit-questions/edit-oos-question.js"></script>
</head>

<body onresize="fixColumnHeightsInPreview();">

</body>
</html>

<script>
  function receiveAttributes() {
      var answers = [];
      var answersValidate = [];
      var answerPictures = [];
      var answerPictureSizes = [];
      <c:forEach items="${question.availableAnswers}" var="answer">
        answers.push("${answer.text}");
        answersValidate.push("${answer.comb}");
        answerPictures.push("${answer.pictureId}");
        answerPictureSizes.push("${answer.pictureSize}");
      </c:forEach>
      for(var i = 0; i < answers.length; i++) {
          addAnswerForm();
          $("#answerText" + (i + 1)).val(answers[i]);
          if(answersValidate[i] == "true") {
              $("#answerChkBox" + (i + 1)).val(i + 1);
              $("#answerChkBox" + (i + 1)).click();
          }
          if(answerPictures[i] != "") {
              var id = "perm/".concat(answerPictures[i]);
              document.getElementById("_AnswerImage" + (i + 1)).value = id;
              _addImage(document.getElementById("addAnswerImage" + (i + 1)), id, 1);
              document.getElementById("imageSize" + (i + 1)).value = answerPictureSizes[i];
              changeImageSize("imageSize" + (i + 1));
          }
      }
  }
  receiveAttributes();
  updatePreview();
</script>