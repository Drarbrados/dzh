<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>

<html>
<head>
    <title>Tester</title>
</head>

<body class="testerBody">
	<center>
		<br/>
		<div style="margin: 15px;"></div>
		<h1>
			<c:out value="${sessionScope.user.fname} ${sessionScope.user.lname}"/></h1>

		<div style="margin: -15px;"></div>
		<c:set var="t" value="${requestScope.titleTest}" />
		<h2><a href="menu?id=${t.parentId}">${t.title}</a></h2>

		<c:set var="tests" value="${requestScope.testList}" />
		<c:set var="count" value="0"/>
		<c:forEach items="${requestScope.testList}" var="testStat">
			<c:set var="count" value="${count + 1}"/>
			<div style="margin-left: 33%; margin-top: -10px;">
				<h3 align="left">${count}.
					<a href="menu?id=${testStat.id}">${testStat.title}</a></h3>
			</div>
		</c:forEach>
		<br/>
	</center>

	<br/><br/>

	<input type="hidden" id="pageId" value="mainPage"/>
</body>
</html>
