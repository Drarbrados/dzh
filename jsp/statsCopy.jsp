<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>

<html>
<head>
</head>
<body class="testerBody">
  <br/>
  <div style="margin: 15px;"></div>
  <h1 align="center">Статистика</h1>



  <table style="width: 90%; margin-left: 7%;">
    <tr>
      <td style="width: 33%;">
          <input class="form-control" type="text" name="role"
                 maxlength="40" placeholder="Группа" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
          <input class="form-control" type="text" name="role"
                 maxlength="40" placeholder="Студент" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
        <table style="margin-top: -15px;">
          <tr>
            <td style="width: 10%;">
              <div>C:</div>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="day">
                <option>01</option>
                <option>02</option>
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="month">
                <option>Январь</option>
                <option>Февраль</option>
                <option>Апрель</option>
                <option>Март</option>
                <option>Май</option>
                <option>Июнь</option>
                <option>Июль</option>
                <option>Август</option>
                <option>Сентябрь</option>
                <option>Октябрь</option>
                <option>Ноябрь</option>
                <option>Декабрь</option>
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="year">
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
                <option>2030</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 33%;">
        <input class="form-control" type="text" name="role"
               maxlength="40" placeholder="Тест" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
        <input class="form-control" type="text" name="role"
               maxlength="40" placeholder="Вопрос" style="width: 90%;"/><br/>
      </td>
      <td style="width: 33%;">
        <table style="margin-top: -15px;">
          <tr>
            <td style="width: 10%;">
              <div>По:</div>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="day">
                <option>01</option>
                <option>02</option>
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="month">
                <option>Январь</option>
                <option>Февраль</option>
                <option>Апрель</option>
                <option>Март</option>
                <option>Май</option>
                <option>Июнь</option>
                <option>Июль</option>
                <option>Август</option>
                <option>Сентябрь</option>
                <option>Октябрь</option>
                <option>Ноябрь</option>
                <option>Декабрь</option>
              </select>
            </td>
            <td style="width: 30%;">
              <select class="form-control" style="width: 95%;" name="year">
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
                <option>2030</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>





  <table class="table table-bordered statsTable" id="table">
    <tr>
      <th style="">Группа</th>
      <th style="">Студент</th>
      <th style="">Тест [Вопросы]</th>
      <th style="">Результат</th>
      <th style="">Дата</th>
    </tr>

    <form method="get" action="#" id="form">
      <tr class="statsTableRowS1">
        <td>ЭМ-41</td>
        <td>Анасов Алексей</td>
        <td>ПМО МПС 1</td>
        <td>0/3</td>
        <td>29-01-2015</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 1</td>
        <td>-</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 2</td>
        <td>-</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 3</td>
        <td>-</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS1">
        <td>ЭМ-41</td>
        <td>Анасов Алексей</td>
        <td>ПМО МПС 1</td>
        <td>2/3</td>
        <td>29-01-2015</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 1</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 2</td>
        <td>-</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 3</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS2">
        <td>ЭМ-41</td>
        <td>[Анасов Алексей]</td>
        <td></td>
        <td>[2/6]</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS1">
        <td>ЭМ-41</td>
        <td>Железниченко Андрей</td>
        <td>ПМО МПС 1</td>
        <td>3/3</td>
        <td>29-01-2015</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 1</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 2</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 3</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS2">
        <td>ЭМ-41</td>
        <td>[Железниченко Андрей]</td>
        <td></td>
        <td>[3/3]</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS3">
        <td>[ЭМ-41]</td>
        <td></td>
        <td></td>
        <td>[5/9]</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS1">
        <td>ЭC-41</td>
        <td>Жосткий Артур</td>
        <td>Связист 1.4</td>
        <td>3/3</td>
        <td>30-01-2015</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 1</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 2</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Вопрос 3</td>
        <td>+</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS2">
        <td>ЭC-41</td>
        <td>[Жосткий Артур]</td>
        <td></td>
        <td>[3/3]</td>
        <td></td>
      </tr>
      <tr class="statsTableRowS3">
        <td>[ЭС-41]</td>
        <td></td>
        <td></td>
        <td>[3/3]</td>
        <td></td>
      </tr>
    </form>
  </table>

  <br/><br/>

  <input type="hidden" id="pageId" value="statsPage"/>
</body>
</html>
