<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>
<html>
<head>
	<link rel="stylesheet" href="css/bootstrap.css" />
	<title> Tester </title>
</head>


<body class="testerBody">
	<center>
		<br/>
		<h1 style="font-weight: bold;"><x style="color: #308080;">Gin</x><x style="color: orange;">T</x></h1>
		<div style="margin-top: -5px"/>

		<form action="login" method="post">
			<div class="input-group" style="margin-left: 40%;">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				<input style="width: 30%;" class="form-control" type="text"
					   placeholder="login" name="login"
					   maxlength="15" value="" required/><br/>
			</div>
			<div style="margin-top: 5px;"></div>
			<div class="input-group" style="margin-left: 40%;">
			<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				<input style="width: 30%;" class="form-control"
						type="password" placeholder="password" name="password"
						maxlength="15" value="" required/><br/>
			</div>
			<button style="margin: 8px; padding: 4; padding-left: 7; padding-right: 7;"
				class="btn btn-default btn-lg" type="submit" name="logIn">Войти</button>
		</form>
	</center>

	<input type="hidden" id="pageId" value="loginPage"/>
</body>
</html>
