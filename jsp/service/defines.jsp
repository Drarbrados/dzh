<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="css/tester.css"/>
  <link rel="icon" href="imgs/logo32.png" type="image/x-icon"/>
</head>
<body>
  <%--DEFINE GRADIENT--%>
  <input type="hidden" value="50a0a0" id="upperLimit"/>
  <input type="hidden" value="50" id="deltaOne"/>
  <input type="hidden" value="50" id="deltaTwo"/>
  <input type="hidden" value="1" id="offsetOne"/>
  <input type="hidden" value="100" id="offsetTwo"/>
</body>
</html>