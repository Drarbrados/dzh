<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/navbar.jsp"%>

<html>

<head>
    <script type="text/javascript" src="js/tester/spanColors.js"></script>
</head>

<body class="testerBody">

<br/><br/>
<h1 align="center" style="margin-left: -25px">Настройки Аккаунта</h1>

<form method="post" action="editUser">
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s1" style="background-color: #6bb; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-star"></i></span>
        <input class="form-control" type="text" name="role"
               maxlength="40" value="Пользователь (студент)" required readonly style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s2" style="background-color: #6aa; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
        <input class="form-control" type="text" placeholder="group" name="group"
               maxlength="30" value="${sessionScope.user.group.name}" required readonly style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s3" style="background-color: #6aa; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input class="form-control" type="text" placeholder="login" name="login"
               maxlength="30" value="${sessionScope.user.login}" required readonly style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s4" style="background-color: #6aa; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input class="form-control" type="text" placeholder="first name" name="fName"
               maxlength="30" value="${sessionScope.user.fname}" required readonly style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s5" style="background-color: #699; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input class="form-control" type="text" placeholder="last name" name="lName"
               maxlength="30" value="${sessionScope.user.lname}" required readonly style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s6" style="background-color: #688; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input class="form-control" type="password" placeholder="старый пароль" name="oldpassw"
               pattern="[0-9a-zA-Z-_@$%]{4,20}"
               maxlength="20" value="" style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s7" style="background-color: #677; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input class="form-control" type="password" placeholder="новый пароль" name="passw"
               pattern="[0-9a-zA-Z-_@$%]{4,20}"
               maxlength="20" value="" style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 30%; margin-bottom: 1%;">
        <span id="s8" style="background-color: #666; color: #eee" class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input class="form-control" type="password" placeholder="подтверждение пароля" name="cpassw"
               pattern="[0-9a-zA-Z-_@$%]{4,20}"
               maxlength="20" value="" style="width: 50%;"/><br/>
    </div>
    <div class="input-group" style="margin-left: 45%; width: 50%;">
        <button type="submit" class="btn btn-default">Подтвердить</button>
    </div>
    <input type="hidden" name="groupId" value="${sessionScope.user.group.id}"/>
</form>

    <input type="hidden" id="pageId" value="editPersonalDataPage"/>
</body>
</html>

<script>
    setSpanColors(8);
</script>
