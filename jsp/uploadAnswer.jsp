<!--
 Created by denis on 23.1.15.
 -->
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>inside iframe</title>
</head>
<body onload="complete('${OK}', '${ID}', 'Всё пропало, шеф.');">
</body>

<script>
	function complete(ok, id, msg){
		var doc = window.parent.document;
		if(doc.uploadListener !== undefined && doc.uploadListener !== null){
			doc.uploadListener(ok, id, msg);
		}
		else{
			if(msg === null || msg === undefined)
				msg = '\n\tNo message.';
			else
				msg = '\n' + msg;
			alert('Загрузка: ' + (ok === true ? 'прошла успешка\n  id:' + id : 'ОШИБКА') + '\n\n' + msg);
		}
	}
</script>

</html>
