
time.sec = 0;

function time() {
    if(document.getElementById("timeLeft").value < 999999 &&
        document.getElementById("timeLeft").value > 0) {
        //if(document.getElementById("pageId").value != "testresultPage" &&
        //    document.getElementById("pageId").value != "questionPage") {
        //    window.location.replace('skipQuestion');
        //}
        if(document.getElementById("timeLeft").value < 60) {
            document.getElementById("timerNav").className = "nav navbar-nav timerNav timerNav-alert";
        }
        var second = parseInt(document.getElementById("timeLeft").value);
        if(time.sec == 0 && second != null) {
            time.sec = second;
        }
        var sec = time.sec;
        var min = (sec - sec % 60) / 60;
        sec -= min * 60;
        if(min <= 9){ min = "0" + min;}
        if(sec <= 9){ sec = "0" + sec;}
        if(document.getElementById){ timer.innerHTML = min + ":" + sec;}
        if(min == 00 && sec == 00){
            window.location.replace('testResult');
        }
        time.sec--;
        setTimeout("time()", 1000);
        document.getElementById("timeLeft").value--;
        document.getElementById("timeLeft").click();
    }
}

function getTime() {
    return time.sec;
}