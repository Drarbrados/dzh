function sort(el, sortType, div, offset) {
    var col_sort = el.innerHTML;
    var tr = el.parentNode;
    var table = tr.parentNode;
    var td, col_sort_num;
    var arrow;
    for (var i=0; (td = tr.getElementsByTagName("th").item(i)); i++) {
        if (td.innerHTML == col_sort) {
            col_sort_num = i;
            if (td.prevsort == "yUp" || td.prevsort == "yDown"){
                if(td.prevsort == "yDown") {
                    td.prevsort = "yUp";
                    el.up = 1;
                } else {
                    td.prevsort = "yDown";
                    el.up = 0;
                }
                if(td.firstChild instanceof Text) {
                    arrow = td.insertBefore(document.createElement("span"),td.firstChild);
                } else {
                    arrow = td.firstChild;
                }
            } else{
                td.prevsort = "yDown";
                arrow = td.insertBefore(document.createElement("span"),td.firstChild);
                el.up = 0;
            }
            arrow.innerHTML = el.up?"↑ ":"↓ ";
        } else{
            if (td.prevsort == "yUp" || td.prevsort == "yDown"){
                td.prevsort = "n";
                if (td.firstChild) td.removeChild(td.firstChild);
            }
        }
    }

    var a = new Array();


    var length = table.rows.length;
    if(offset == null) {
        offset = 1;
    } else {
        offset += 1;
    }
    if(sortType == 1){
        for (i = offset; i < length; i++) {
            a[i - 1] = new Array();
            if(div != true) {
                a[i - 1][0] = parseInt(table.rows[i].getElementsByTagName("td").item(col_sort_num).innerHTML);
            } else {
                a[i - 1][0] = parseInt(parseTags((table.rows[i].getElementsByTagName("td").item(col_sort_num).innerHTML)));
            }
            a[i - 1][1] = table.rows[i];
        }
        if(el.up) {
            a.sort(function(a, b){return a[0] - b[0]});
        } else {
            a.sort(function(a, b){return b[0] - a[0]});
        }
    } else if(sortType == 2) {
        for (i = offset; i < length; i++) {
            a[i - 1] = new Array();
            a[i - 1][0] = parseInt(parseColor(table.rows[i].getElementsByTagName("td").item(col_sort_num).style.backgroundColor));
            a[i - 1][1] = table.rows[i];
        }
        if(el.up) {
            a.sort(function(a, b){return a[0] - b[0]});
        } else {
            a.sort(function(a, b){return b[0] - a[0]});
        }
    } else {
        for (i = offset; i < length; i++) {
            a[i - 1] = new Array();
            if(div == null || div == false) {
                a[i - 1][0] = table.rows[i].getElementsByTagName("td").item(col_sort_num).innerHTML;
            } else {
                a[i - 1][0] = parseTags(table.rows[i].getElementsByTagName("td").item(col_sort_num).innerHTML);
            }
            a[i - 1][1] = table.rows[i];
        }
        a.sort();
        if(el.up) {
            a.reverse();
        }
    }

    for(i = offset - 1; i < a.length; i++) {
        table.appendChild(a[i][1]);
    }
}

function ghostSort(el) {
    var col_sort = el.innerHTML;
    var tr = el.parentNode;
    var td, col_sort_num;
    var arrow;
    for (var i=0; (td = tr.getElementsByTagName("th").item(i)); i++) {
        if (td.innerHTML == col_sort) {
            col_sort_num = i;
            if (td.prevsort == "yUp" || td.prevsort == "yDown"){
                if(td.prevsort == "yDown") {
                    td.prevsort = "yUp";
                    el.up = 1;
                } else {
                    td.prevsort = "yDown";
                    el.up = 0;
                }
                if(td.firstChild instanceof Text) {
                    arrow = td.insertBefore(document.createElement("span"),td.firstChild);
                } else {
                    arrow = td.firstChild;
                }
            } else{
                td.prevsort = "yDown";
                arrow = td.insertBefore(document.createElement("span"),td.firstChild);
                el.up = 0;
            }
            arrow.innerHTML = el.up?"↑ ":"↓ ";
        } else{
            if (td.prevsort == "yUp" || td.prevsort == "yDown"){
                td.prevsort = "n";
                if (td.firstChild) td.removeChild(td.firstChild);
            }
        }
    }
}

function parseTags(tags) {
    return tags.replace(new RegExp('<(.|\n)*?>', 'g'), "");
}
function parseColor(color) {
    color = color.replace(new RegExp(', \\d*', 'g'), "");
    return color.replace(new RegExp('\\D', 'g'), "");
}