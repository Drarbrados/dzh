function setSpanColors(length) {
    var color = parseInt($("#upperLimit").val(), 16);
    var delta1 = Math.round(parseInt($("#deltaOne").val(), 16) / length);
    var delta2 = Math.round(parseInt($("#deltaTwo").val(), 16) / length);
    var offset1 = parseInt($("#offsetOne").val(), 16);
    var offset2 = parseInt($("#offsetTwo").val(), 16);
    var clr;
    for (var i = 0; i < length; i++) {
        clr = "#" + (color - delta1 * i * offset1 - delta2 * i * offset2).toString(16);
        document.getElementById("s" + [i + 1]).style.backgroundColor = clr;
    }
}