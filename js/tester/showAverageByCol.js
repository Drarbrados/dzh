function showAverage(averageColIndex, resultColIndex, cellCount, leaveLast) {

    var table = document.getElementById("table");
    if(table.rows.length > 1) {
        var length = table.rows.length;
        if(leaveLast == false || leaveLast == null) {
            for (var i = 0; i < length; i++) {
                if (table.rows[i].cells[resultColIndex].innerHTML.substring(0, 1) == '[') {
                    table.deleteRow(i);
                    length--;
                    i--;
                }
            }
        }

        for(i = 1; i < table.rows.length; i++) {
            if(table.rows[i].style.display != 'none') {
                var averageColValue = table.rows[i].cells[averageColIndex].innerHTML;
                break;
            }
        }
        var j = 0;
        length = table.rows.length;
        var count = 0;
        for(; i <= length; i++) {
            count++;
            if(i != length && table.rows[i].style.display == 'none') {
                continue;
            }
            if(i == length || (table.rows[i].cells[averageColIndex].innerHTML != averageColValue)) {

                if(i != length && table.rows[i].cells[averageColIndex].innerHTML == "") {
                    continue;
                }
                if(i != length) {
                    averageColValue = table.rows[i].cells[averageColIndex].innerHTML;
                }
                var row = table.insertRow(i);
                row.className = "statsTableRowS2";
                row.id = "average" + j;
                j++;
                if(i != length) {
                    length++;
                }
                var cells = [];
                for (var k = 0; k < cellCount; k++) {
                    cells[k] = row.insertCell(k);
                }
                for(k = i - 1; k >= 1; k--) {
                    if(table.rows[k].style.display == '' &&
                        table.rows[k].cells[averageColIndex].innerHTML != "") {
                        cells[averageColIndex].innerHTML =
                            '[' + table.rows[k].cells[averageColIndex].innerHTML + ']';
                        break;
                    }
                }
                var average = 0;
                var count2 = 0;
                for (k = i - 1; k > i - count; k--) {
                    if (table.rows[k].style.display != 'none') {
                        if(table.rows[k].cells[resultColIndex].innerHTML.substring(0, 1) != '[') {
                            count2++;
                            average += parseInt(table.rows[k].cells[resultColIndex].innerHTML);
                        }
                    }
                }
                if (count2 > 0) {
                    average /= count2;
                }
                count = 0;
                cells[resultColIndex].innerHTML = '[' + average.toFixed(2) + ']';
            }
        }
    }
}