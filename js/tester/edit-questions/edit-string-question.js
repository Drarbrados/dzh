
function changeImageSize(id) {
    _changeImageSize(id, 3);
}

function addImage(button, imageName) {
    _addImage(button, imageName, 3);
}

function deleteImage(button) {
    _deleteImage(button, 3);
}

function resizeHorizontal() {
    _resizeHorizontal(3);
}

function moveQuestionsArea() {
    _moveQuestionsArea(3);
}

function updatePreview() {
    _updatePreview(3);
}

function addAnswerForm() {
    _addAnswerForm(3);
}

function fixColumnHeightsInPreview() {
    _fixColumnHeightsInPreview();
}

