
function changeImageSize(id) {
    _changeImageSize(id, 1);
}

function addImage(button, imageName) {
    _addImage(button, imageName, 1);
}

function deleteImage(button) {
    _deleteImage(button, 1);
}

function resizeHorizontal() {
    _resizeHorizontal(1);
}

function moveQuestionsArea() {
    _moveQuestionsArea(1);
}

function updatePreview() {
    _updatePreview(1);
}

function addAnswerForm() {
    _addAnswerForm(1);
}

function fixColumnHeightsInPreview() {
    _fixColumnHeightsInPreview();
}
