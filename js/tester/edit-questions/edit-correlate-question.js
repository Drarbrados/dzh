
function changeImageSize(id) {
    _changeImageSize(id, 4);
}

function addImage(button, imageName) {
    _addImage(button, imageName, 4);
}

function deleteImage(button) {
    _deleteImage(button, 4);
}

function resizeHorizontal() {
    _resizeHorizontal(4);
}

function moveQuestionsArea() {
    _moveQuestionsArea(4);
}

function updatePreview() {
    _updatePreview(4);
}

function addAnswerForm() {
    _addAnswerForm(4, 1);
}

function fixColumnHeightsInPreview() {
    _fixColumnHeightsInPreview();
}
