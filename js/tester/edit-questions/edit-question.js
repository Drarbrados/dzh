
function _changeImageSize(id, type) {
    if(id == "imageWidth") {
        //question case
        var imageWidth = document.getElementById("imageWidth");
        var pictureImg = document.getElementById("questionImg");
        var width = parseInt(imageWidth.value.replace(new RegExp("%"), ""));
        if(width > 100) {
            width = 100;
        }
        imageWidth.value = "" + width + "%";
        pictureImg.style.width = imageWidth.value;
    } else if(_addAnswerForm != null){
        //answer case
        _updatePreview(type);
    }
}

function _addImage(button, imageName, type) {
    var x = imageName.lastIndexOf("_");
    if(x <= 0) {
        //
    }
    x++;
    if(x < 0) {
        x = 0;
    }
    if(button.id == "addQuestionImage") {
        document.getElementById("questionImg").src = "images/" + imageName;
        //alert("question_image_test " + document.getElementById("questionImg").src);
        //alert(document.getElementById("questionImg").src);
        imageName = imageName.substring(x);
        //show question (image + configs) and hide add image button
        button.style.display = "none";
        document.getElementById("imageConfigs").style.display = "";
        document.getElementById("imageName").innerHTML = imageName;
        var pictureImg = document.getElementById("questionImg");
        var imageWidth = document.getElementById("imageWidth");
        if(pictureImg != null) {
            //set image src
            //pictureImg.src = "#";
            pictureImg.style.display = "";
            //set default image size
            imageWidth.value = "50%";
        }
    } else if(_addAnswerForm != null){
        for (var i = 1; i < _addAnswerForm.id; i++) {
            if(button.id == "addAnswerImage" + i) {
                if(imageName.indexOf("perm") >= 0) {
                    document.getElementById("imageSrc" + i).value = "images/" + imageName;
                }
                //alert("answer_image_test " + document.getElementById("imageSrc" + i).value);
                imageName = imageName.substring(x);
                //show answer image configs and hide add image button
                document.getElementById("addAnswerImage" + i).style.display = "none";
                document.getElementById("imageConfigs" + i).style.display = "";
                document.getElementById("answerImageName" + i).innerHTML = imageName;
                //change image params in answer form
                //set image src
                //document.getElementById("imageSrc" + i).value = "#";
                document.getElementById("imageVisibility" + i).value = "";
                if(document.getElementById("imageSize" + i).value == "") {
                    document.getElementById("imageSize" + i).value = "50%";
                }
                //apply changes by repaint preview
                _updatePreview(type);
                break;
            }
        }
    }
}

function _deleteImage(button, type) {
    if(button.id == "deleteQuestionImage") {
        //hide question (image + configs) and show add image button
        document.getElementById("addQuestionImage").style.display = "";
        document.getElementById("imageConfigs").style.display = "none";
        var pictureImg = document.getElementById("questionImg");
        if(pictureImg != null) {
            pictureImg.src = "#";
            pictureImg.style.display = "none";
        }
        document.getElementById("_QuestionImage").value = "";
    } else if(_addAnswerForm != null) {
        //find answer image by id
        for (var i = 1; i < _addAnswerForm.id; i++) {
            if(button.id == "deleteAnswerImage" + i) {
                //hide answer image configs and show add image button
                document.getElementById("addAnswerImage" + i).style.display = "";
                document.getElementById("imageConfigs" + i).style.display = "none";
                //change image params in answer form
                document.getElementById("imageSrc" + i).value = "#";
                document.getElementById("imageVisibility" + i).value = "none";
                document.getElementById("_AnswerImage" + i).value = "";
                //apply changes by repaint preview
                _updatePreview(type);
                break;
            }
        }
    }
}

function _resizeHorizontal(type) {
    var icon = document.getElementById("ipw");
    //get width from pageWidth slider element
    var width = parseInt(document.getElementById("pageWidth").value);
    if(icon != null) {
        icon.className = "";
        icon.innerHTML = "" + width;
    }
    //get answer table from review
    if(type != 3) {
        var workArea = document.getElementById("answersTable");
    } else {

        workArea = document.getElementById("textInput");
    }
    if(workArea != null) {
        //get max width from pageWidth slider element
        var widthMax = parseInt(document.getElementById("pageWidth").max);
        //set answer table width
        workArea.style.width = width + "%";
        //set min and max values of leftOffset slider element
        document.getElementById("leftOffset").min = (width - widthMax);
        document.getElementById("leftOffset").max = (widthMax - width);
        //need if current offset lower then min or higher then max
        _moveQuestionsArea(type);
        //need to change cell sizes by image sizes changes
        _fixColumnHeightsInPreview(type);
    }
}

function _resizeHorizontalInit() {
    var width = parseInt(document.getElementById("pageWidth").value);
    var widthMax = parseInt(document.getElementById("pageWidth").max);
    document.getElementById("leftOffset").min = (width - widthMax);
    document.getElementById("leftOffset").max = (widthMax - width);
}

function _moveQuestionsArea(type) {
    var icon = document.getElementById("ilo");
    //get offset from leftOffset slider element
    var offset = document.getElementById("leftOffset").value;
    if(icon != null) {
        icon.className = "";
        icon.innerHTML = "" + offset;
    }
    //get answer table from review
    if(type != 3) {
        var workArea = document.getElementById("answersTable");
    } else {
        workArea = document.getElementById("textInput");
    }
    if(workArea != null) {
        //set left offset
        workArea.style.marginLeft = offset + "%";
    }
}

function setIcons() {
    document.getElementById("ilo").innerHTML = "";
    document.getElementById("ipw").innerHTML = "";
    document.getElementById("ilo").className = "glyphicon glyphicon-transfer";
    document.getElementById("ipw").className = "glyphicon glyphicon-resize-horizontal";
}


/**
 * @type: multitype
 * @uses: add_new_answer / delete_answer / add_image / change_image_size / load_page / ...
 * @description: totally rebuilds preview
 * @param type - question type
 */
function _updatePreview(type) {
    //update username label
    document.getElementById("usernamePreview").innerHTML =
        $('#userFname').val() + " " + $('#userLname').val();
    //update question text label
    document.getElementById("questionTextPreview").innerHTML =
        document.getElementById("questionText").value;

    //get review area
    var place = document.getElementById("reviewAnswers");
    //clear review area
    place.innerHTML = "";

    if(type == 3) {
        _updatePreviewString(place, type);
        return;
    }
    //below there is code to oos, multiple and correlate questions

    //number of columns in row
    var colCountDef = parseInt(document.getElementById("placeType").value);
    var rowIndex = 0; //current row
    var colIndex = 0; //current column
    var colCount = 0; //total column count
    var trs = []; //rows
    var tds = []; //cells
    //create answer table
    var table = document.createElement("table");
    table.id = "answersTable";
    //set table width by pageWidth slider value
    table.style.width = parseInt(document.getElementById("pageWidth").value) + "%";
    //set left offset by leftOffset slider value
    table.style.marginLeft = document.getElementById("leftOffset").value + "%";
    //add answer table to review area
    place.appendChild(table);

    //loop by answer forms
    for(var i = 1; i < _addAnswerForm.id; i++) {
        //if answer form with this id exist
        if(document.getElementById("answerText" + i) != null) {
            colCount++;
            //if there are new row
            if(colIndex == 0) {
                rowIndex++;
                colIndex++;
                //create a new row
                trs[rowIndex - 1] = document.createElement("tr");
                //create cells in new row
                if(type != 4) {
                    for (var j = 0; j < colCountDef; j++) {
                        //create a cell
                        tds[(rowIndex - 1) * colCountDef + j] = document.createElement("td");
                        //set cell width
                        tds[(rowIndex - 1) * colCountDef + j].style.width = "" + (100 / colCountDef) + "%";
                        //set cell border
                        if (type != 4) {
                            tds[(rowIndex - 1) * colCountDef + j].style.border = "1px dashed #70c0c0";
                        }
                        //add a cell to the row
                        trs[rowIndex - 1].appendChild(tds[(rowIndex - 1) * colCountDef + j]);
                        if (type == 4) {
                            tds[(rowIndex - 1) * colCountDef + j].style.backgroundColor = "#50a0a0";
                            tds[(rowIndex - 1) * colCountDef + j].style.borderRadius = "25px";
                            if (j == 0) {
                                tds[(rowIndex - 1) * colCountDef + j].style.width =
                                    "" + (100 / colCountDef - 5) + "%";
                                var separator = document.createElement("td");
                                separator.style.width = "10%";
                                trs[rowIndex - 1].appendChild(separator);
                            } else {
                                var separateRow = document.createElement("tr");
                                separator = document.createElement("td");
                                separator.style.height = "10px";
                                separateRow.appendChild(separator);
                                table.appendChild(separateRow);
                            }
                        }
                    }
                    //add row to the table
                    table.appendChild(trs[rowIndex - 1]);
                } else {
                    if(colCount == 1) {
                        var tr = document.createElement("tr");
                        var td = [];
                        td[0] = document.createElement("td");
                        td[0].style.width = "45%";
                        td[0].id = "sortable1";
                        var separator = document.createElement("td");
                        separator.style.width = "10%";
                        td[1] = document.createElement("td");
                        td[1].style.width = "45%";
                        td[1].id = "sortable2";
                        tr.appendChild(td[0]);
                        tr.appendChild(separator);
                        tr.appendChild(td[1]);
                        table.appendChild(tr);
                    }
                    for (j = 0; j < 2; j++) {
                        tds[(rowIndex - 1) * colCountDef + j] = document.createElement("div");
                        tds[(rowIndex - 1) * colCountDef + j].style.backgroundColor = "#50a0a0";
                        tds[(rowIndex - 1) * colCountDef + j].style.borderRadius = "25px";
                        tds[(rowIndex - 1) * colCountDef + j].style.marginTop = "10px";
                        var comb = document.getElementById("trueComb" + (i + j));
                        if(comb != null) {
                            tds[(rowIndex - 1) * colCountDef + j].comb = comb.value;
                        }
                        td[j].appendChild(tds[(rowIndex - 1) * colCountDef + j]);
                    }
                }
            }

            //create table into current cell
            var innerTable = document.createElement("table");
            innerTable.style.width = "100%";
            //create image row
            var innerTableImageRow = document.createElement("tr");
            //create empty cell
            var innerTableImageRowTd1 = document.createElement("td");
            innerTableImageRowTd1.style.width = "10px";
            //innerTableImageRowTd1.style.backgroundColor = "#ff0000";
            //create a cell to an image
            var innerTableImageRowTd2 = document.createElement("td");
            innerTableImageRowTd2.style.position = "relative";
            innerTableImageRowTd2.id = "imageCell" + colCount;
            //create image element
            var answerImage = document.createElement("img");
            answerImage.style.border = "0px solid #50a0a0";
            if(type == 4) {
                answerImage.style.borderRadius = "25px";
            }
            //set image src by imageSrc element from answer form
            answerImage.src = document.getElementById("imageSrc" + i).value;
            //set image display settings by imageVisibility element from answer form
            answerImage.style.display = document.getElementById("imageVisibility" + i).value;

            if(answerImage.src.indexOf("showQuestion") > 0) {
                answerImage.style.display = "none";
            } else {
                if(answerImage.src.indexOf("perm") < 0 && answerImage.src.indexOf("temp") < 0) {
                    answerImage.src = "images/perm/" + document.getElementById("imageSrc" + i).value;
                }
            }
            //attach image to innerTableImageRowTd2 bottom
            answerImage.style.position = "absolute";
            answerImage.style.bottom = "0";
            //define start image size to such elements in answer form
            document.getElementById("imageStartWidth" + i).value = answerImage.width;
            document.getElementById("imageStartHeight" + i).value = answerImage.height;
            //set width by imageSize element from answer form
            var width = parseInt(document.getElementById("imageSize" + i).
                value.replace(new RegExp("%"), ""));
            if(width > 100) {
                width = 100;
            }
            answerImage.style.width = "" + width + "%";
            answerImage.usersize = width / 100;
            answerImage.id = "answerImage" + i;
            //add image to image cell
            innerTableImageRowTd2.appendChild(answerImage);
            //create text row
            var innerTableTextRow = document.createElement("tr");
            //create checkbox cell
            var innerTableTextRowTd1 = document.createElement("td");
            innerTableTextRowTd1.style.width = "10px";
            innerTableTextRowTd1.style.position = "relative";
            //innerTableTextRowTd1.style.backgroundColor = "#ffff00";
            //create answer correctness checkbox

            //oos or multiple

            //create textarea cell
            var innerTableTextRowTd2 = document.createElement("td");
            innerTableTextRowTd2.style.position = "relative";
            innerTableTextRowTd2.id = "textCell" + colCount;
            innerTableTextRowTd2.paddingRight = "5px";
            //create textarea
            var answerText = document.createElement("text");
            //get text from answerText element from answer form
            answerText.innerHTML = document.getElementById("answerText" + i).value;
            //hardcode textarea style
            answerText.style.border = "none";
            answerText.style.backgroundColor = "transparent";
            answerText.style.width = "95%";
            //answerText.style.resize = "none";
            answerText.style.whiteSpace = "normal";
            answerText.style.wordWrap = "break-word";
            answerText.style.paddingLeft = "5px";
            answerText.disabled = "disabled";
            //answerText.className = "form-control";
            answerText.style.marginTop = "5px";
            answerText.style.fontSize = "18";
            answerText.style.height = "auto";
            answerText.style.position = "absolute";
            answerText.style.top = "0";
            answerText.style.left = "15";
            answerText.style.overflow = "hidden";
            answerText.style.marginBottom = "2px";
            answerText.name = "answerTextReview";
            answerText.index = colCount;
            answerText.style.cursor = "pointer";

            if(type == 1) {
                createRadioButton(colCount, innerTableTextRowTd1, answerText);
            } else if(type == 2){
                createCheckbox(colCount, innerTableTextRowTd1, answerText);
            }


            //answerText.style.marginRight = "4px";
            //set textarea id
            answerText.id = "answerTextPreview" + i;
            //add textarea to textarea cell
            innerTableTextRowTd2.appendChild(answerText);
            //add cells to image row
            innerTableImageRow.appendChild(innerTableImageRowTd1);
            innerTableImageRow.appendChild(innerTableImageRowTd2);
            //add cells to text row
            innerTableTextRow.appendChild(innerTableTextRowTd1);
            innerTableTextRow.appendChild(innerTableTextRowTd2);
            //add rows to inner table
            innerTable.appendChild(innerTableImageRow);
            innerTable.appendChild(innerTableTextRow);
            //add inner table to outer table cell
            tds[(rowIndex - 1) * colCountDef + colIndex - 1].appendChild(innerTable);

            //increase answer number
            colIndex++;
            //new row
            if(colIndex - 1 == colCountDef) {
                colIndex = 0;
            }
        }
    }
    //create answer number element
    var colCountElement = document.createElement("input");
    colCountElement.type = "hidden";
    colCountElement.id = "colCount";
    colCountElement.value = colCount;
    //add answer number element to review area
    place.appendChild(colCountElement);

    _fixColumnHeightsInPreview(type);



    function createCheckbox(colCount, place, text) {

        var answerCorrectnessDiv = document.createElement("div");
        var answerCorrectnessCheckbox = document.createElement("input");
        var answerCorrectnessLabel = document.createElement("label");

        answerCorrectnessDiv.className = "squaredThree-inverse-bright";
        answerCorrectnessDiv.style.marginBottom = "30px";
        answerCorrectnessDiv.style.marginTop = "5px";
        answerCorrectnessDiv.style.zIndex = "555";

        answerCorrectnessCheckbox.type = "checkbox";
        answerCorrectnessCheckbox.name = "checkboxPreview";
        answerCorrectnessCheckbox.style.display = "none";
        answerCorrectnessCheckbox.id = "answChk" + colCount;

        answerCorrectnessLabel.style.marginLeft = "2px";
        answerCorrectnessLabel.for = "answChk" + colCount;
        answerCorrectnessDiv.onclick = function() {
            __check(answerCorrectnessCheckbox);
        };
        text.onclick = function() {
            __check(answerCorrectnessCheckbox);
        };

        answerCorrectnessDiv.appendChild(answerCorrectnessCheckbox);
        answerCorrectnessDiv.appendChild(answerCorrectnessLabel);
        place.appendChild(answerCorrectnessDiv);
    }

    function createRadioButton(colCount, place, text) {

        var answerCorrectnessDiv = document.createElement("div");
        var answerCorrectnessRadio = document.createElement("input");
        var answerCorrectnessLabel = document.createElement("label");

        answerCorrectnessDiv.className = "roundedTwo-inverse-bright";
        answerCorrectnessDiv.style.marginTop = "2px";
        answerCorrectnessDiv.style.marginBottom = "5px";
        answerCorrectnessDiv.style.zIndex = "555";

        answerCorrectnessRadio.type = "radio";
        answerCorrectnessRadio.name = "radioPreview";
        answerCorrectnessRadio.style.display = "none";
        answerCorrectnessRadio.id = "answRadio" + colCount;

        answerCorrectnessLabel.style.marginLeft = "2px";
        answerCorrectnessLabel.for = "answRadio" + colCount;
        answerCorrectnessDiv.onclick = function() {
            __select(answerCorrectnessRadio);
        };
        text.onclick = function() {
            __select(answerCorrectnessRadio);
        };

        answerCorrectnessDiv.appendChild(answerCorrectnessRadio);
        answerCorrectnessDiv.appendChild(answerCorrectnessLabel);
        place.appendChild(answerCorrectnessDiv);
    }
}



/**
 * @type: service
 * @description: continues updates of string question's answer
 * @see: _updatePreview
 */
function _updatePreviewString(place, type) {

    if(document.getElementById("textInput") == null) {
        var textInput = document.createElement("input");
        textInput.type = "text";
        textInput.className = "form-control";
        textInput.id = "textInput";
        place.appendChild(textInput);
        _resizeHorizontal(type);
    }
}

/**
 * @type: multitype
 * @uses: page_scale / change_text_in_preview / change_image_size / resize_horiz / ...
 * @description: fix elements relative sizes
 */
function _fixColumnHeightsInPreview() {
    //number of columns in row
    var colCountDef = parseInt(document.getElementById("placeType").value);
    //total number of columns
    var colCount = parseInt(document.getElementById("colCount").value);

    var maxImageRowHeights = [];
    var maxTextRowHeights = [];
    var rowIndex = 0;
    var colIndex = 0;
    var imageRowHeight;
    var textRowHeight;

    //find max row heights
    for(var i = 1; i <= colCount; i++) {
        if(colIndex == 0) {
            maxImageRowHeights[rowIndex] = 0;
            maxTextRowHeights[rowIndex] = 0;
        }
        colIndex++;
        //create phantom text
        var text = document.createElement("text");
        //set in phantom text value from answer text
        text.innerHTML = document.getElementById("textCell" + i).
            childNodes[0].innerHTML;
        //add phantom text to textCell
        document.getElementById("textCell" + i).appendChild(text);
        //set styles like in updatePreview()
        text.style.width = "95%";
        text.style.whiteSpace = "normal";
        text.style.wordWrap = "break-word";
        text.style.marginTop = "1px";
        text.style.fontSize = "18";
        text.style.height = "auto";
        text.style.position = "absolute";
        text.style.top = "0";
        text.style.left = "15";
        text.style.overflow = "hidden";
        text.style.marginBottom = "2px";
        //get height of phantom text
        textRowHeight = $(text).height();
        if(textRowHeight == null || textRowHeight == 0) {
            textRowHeight = 24;
        }
        textRowHeight += 6;
        //delete phantom text
        text.parentNode.removeChild(text);

        //get height of answer image
        var image = document.getElementById("imageCell" + i).childNodes[0];
        imageRowHeight = parseInt(image.height);
        //add border width
        if(imageRowHeight != 0) {
            imageRowHeight += parseInt(image.style.borderTopWidth) +
            parseInt(image.style.borderBottomWidth) - 1;
        }

        //check and set max rows heights
        if(imageRowHeight > maxImageRowHeights[rowIndex]) {
            maxImageRowHeights[rowIndex] = imageRowHeight;
        }
        if(textRowHeight > maxTextRowHeights[rowIndex]) {
            maxTextRowHeights[rowIndex] = textRowHeight;
        }
        //new row
        if(colIndex == colCountDef) {
            colIndex = 0;
            rowIndex++;
        }
    }
    //reset indexes
    rowIndex = 0;
    colIndex = 0;
    //apply max row heights to all elements in rows
    for(i = 1; i <= colCount; i++) {
        colIndex++;
        //set height of answer image cell
        document.getElementById("imageCell" + i).
            style.height = maxImageRowHeights[rowIndex];
        //set center position to image
        image = document.getElementById("imageCell" + i).childNodes[0];
        var imageWidth = document.getElementById("textCell" + i).childNodes[0].scrollWidth;
        image.style.left = imageWidth * (1 - image.usersize) / 2;


        //set height of answer text sell
        document.getElementById("textCell" + i).
            style.height =  maxTextRowHeights[rowIndex];

        //new row
        if(colIndex == colCountDef) {
            colIndex = 0;
            rowIndex++;
        }
    }
}

/**
 * @type: on action
 * @action: answer_from-input.onchange()
 * @description: changes text in preview if it changes in answer form
 */
function _changeAnswerTextInPreview(answerTextInput, type) {
    if(type == 3) {
        _updatePreview(type);
        return;
    }
    //get answer form id suffix
    var id = answerTextInput.id.replace(new RegExp("answerText"), "");
    //get answer text preview by its own id + suffix
    var answerTextPreview = document.getElementById("answerTextPreview" + id);
    //update answer text preview
    if(answerTextPreview != null) {
        answerTextPreview.innerHTML = answerTextInput.value;
    }
    _fixColumnHeightsInPreview();
}


/**
 * @type: config
 * @description: defines answer form start id
 */
function _setAddAnswerFormId(value) {
    _addAnswerForm.id = value;
}





/**
 * @type: on action
 * @action: newQuestion-btn.click()
 * @description: creates new answer form and refreshes preview area
 * @param type - question type
 * @param parity - in-pair number of correlate question's answer
 * @static id - id of next answer form
 */
function _addAnswerForm(type, parity) {
    //init static answer id
    if (_addAnswerForm.id == null) {
        _addAnswerForm.id = 1;
    }
    //create answer form base element
    var place = document.createElement("div");
    place.id = "answer" + _addAnswerForm.id;
    place.parity = parity;
    if(parity == 2) {
        place.style.marginBottom = "65px";
    }
    place.style.marginTop = "25px";
    //add answer form base element to answers container
    document.getElementById("answers").appendChild(place);

    //create image params which use in preview and in image-scale methods
    var imageSrc = document.createElement("input");
    imageSrc.type = "hidden";
    imageSrc.id = "imageSrc" + _addAnswerForm.id;
    imageSrc.value = "#";
    var imageVisibility = document.createElement("input");
    imageVisibility.type = "hidden";
    imageVisibility.id = "imageVisibility" + _addAnswerForm.id;
    imageVisibility.value = "none";
    var imageStartWidth = document.createElement("input");
    imageStartWidth.type = "hidden";
    imageStartWidth.id = "imageStartWidth" + _addAnswerForm.id;
    var imageStartHeight = document.createElement("input");
    imageStartHeight.type = "hidden";
    imageStartHeight.id = "imageStartHeight" + _addAnswerForm.id;
    //add image-params to answer form
    place.appendChild(imageSrc);
    place.appendChild(imageVisibility);
    place.appendChild(imageStartWidth);
    place.appendChild(imageStartHeight);

    //create input element to answer text
    var inputText = document.createElement("input");
    inputText.type = "text";
    inputText.className = "form-control";
    inputText.maxLength = 255;
    if(type != 4) {
        inputText.placeholder = "Текст ответа";
    } else if(parity == 1){
        inputText.placeholder = "Левый столбец";
    } else if(parity == 2) {
        inputText.placeholder = "Правый столбец";
    }
    inputText.style.width = "90%";
    inputText.id = "answerText" + _addAnswerForm.id;
    inputText.name = "answerText" + _addAnswerForm.id;
    inputText.required = "required";

    //replace answer text in preview-form if it was changed
    inputText.onchange = function() {
        _changeAnswerTextInPreview(inputText, type);
    };
    //add answer text-input element to answer form
    place.appendChild(inputText);

    //create delete button to answer form
    var deleteButton = document.createElement("button");
    deleteButton.type = "button";
    deleteButton.innerHTML = "x";
    deleteButton.className = "btn btn-default btn-lg transparentCloseBtn";
    //hardcode delete button position
    deleteButton.style.marginLeft = "87.5%";
    deleteButton.style.marginTop = "-39px";
    deleteButton.id = "deleteForm" + _addAnswerForm.id;
    //delete answer by click on the delete button
    deleteButton.onclick = function() {
        _deleteAnswerForm(deleteButton.id, type);
    };
    //add delete button to answer form
    place.appendChild(deleteButton);

    //create answer tools panel
    var answerToolsTable = document.createElement("table");
    answerToolsTable.style.width = "90%";
    answerToolsTable.style.marginTop = "5px";
    var answerToolsTableTr = document.createElement("tr");
    //create cell with image tools
    var answerToolsTableTrTd1 = document.createElement("td");
    answerToolsTableTrTd1.style.width = "55%";
    //create image-add button
    var imageButton = document.createElement("button");
    imageButton.type = "button";
    imageButton.className = "form-control";
    imageButton.innerHTML = "Прикрепить Изображение";
    imageButton.style.width = "auto";
    imageButton.id = "addAnswerImage" + _addAnswerForm.id;
    //add an image by click
    imageButton.onclick = function() {
        $('#fileTo').val(imageButton.id);
        $('#file').click();
    };

    var questionImageHiddenInput = document.createElement("input");
    questionImageHiddenInput.id = "_AnswerImage" + _addAnswerForm.id;
    questionImageHiddenInput.name = "_AnswerImage" + _addAnswerForm.id;
    questionImageHiddenInput.type= "hidden";
    //create panel with configs of existing image
    var imageConfigs = document.createElement("h4");
    imageConfigs.style.marginTop = "0px";
    imageConfigs.style.display = "none";
    imageConfigs.id = "imageConfigs" + _addAnswerForm.id;
    //create image name element
    var pictureName = document.createElement("span");
    pictureName.id = "answerImageName" + _addAnswerForm.id;
    //create image delete button
    var imageDeleteButton = document.createElement("button");
    imageDeleteButton.type = "button";
    imageDeleteButton.innerHTML = "x";
    imageDeleteButton.className = "btn btn-default btn-lg transparentCloseBtn";
    imageDeleteButton.style.padding= "2px";
    imageDeleteButton.style.paddingRight = "15px";
    imageDeleteButton.style.marginLeft = "7px";
    imageDeleteButton.id = "deleteAnswerImage" + _addAnswerForm.id;
    //delete an image by click
    imageDeleteButton.onclick = function() {
        _deleteImage(imageDeleteButton, type);
    };
    //create image size info-label element
    var imageSizeSpan = document.createElement("span");
    imageSizeSpan.innerHTML = "размер: ";
    //create image size input element
    var imageSizeInput = document.createElement("input");
    imageSizeInput.type = "text";
    imageSizeInput.style.marginRight = "20px";
    imageSizeInput.style.width = "60px";
    imageSizeInput.id = "imageSize" + _addAnswerForm.id;
    imageSizeInput.name = "answerImageSize" + _addAnswerForm.id;
    //change image size by the size input value
    imageSizeInput.onchange = function() {
        _changeImageSize(imageSizeInput.id, type);
    };
    //add image configs to config panel
    imageConfigs.appendChild(pictureName);
    imageConfigs.appendChild(imageDeleteButton);
    imageConfigs.appendChild(imageSizeSpan);
    imageConfigs.appendChild(imageSizeInput);
    //add configs panel to first cell of tools panel
    answerToolsTableTrTd1.appendChild(imageButton);
    answerToolsTableTrTd1.appendChild(questionImageHiddenInput);
    answerToolsTableTrTd1.appendChild(imageConfigs);
    //create empty cell
    var answerToolsTableTrTd2 = document.createElement("td");
    answerToolsTableTrTd2.style.width = "10%";
    //create cell with answer correctness
    var answerToolsTableTrTd3 = document.createElement("td");
    answerToolsTableTrTd3.style.width = "25%";
    //create container with right alignment
    var divRight = document.createElement("div");
    divRight.align = "right";
    //create answer correctness checkbox
    var answerCorrectnessDiv = document.createElement("div");
    var answerCorrectnessLabel = document.createElement("label");
    var answerCorrectnessCheckbox = document.createElement("input");
    //create answer correctness info-label element
    var answerCorrectnessSpan = document.createElement("span");
    answerCorrectnessSpan.innerHTML = " Верный";
    //oos or multiple
    if(type == 1) {
        answerCorrectnessDiv.className = "roundedTwo-inverse";
        answerCorrectnessDiv.style.marginRight = "80px";
        answerCorrectnessDiv.style.marginTop = "-5px";


        answerCorrectnessCheckbox.type = "radio";
        answerCorrectnessCheckbox.name = "radioForm";
        answerCorrectnessCheckbox.value = _addAnswerForm.id;
        answerCorrectnessCheckbox.style.display = "none";
        answerCorrectnessCheckbox.id = "answerChkBox" + _addAnswerForm.id;
        answerCorrectnessDiv.onclick = function() {
            __select(answerCorrectnessCheckbox);
        };

        answerCorrectnessLabel.style.marginLeft = "0px";
        answerCorrectnessLabel.for = "answerChkBox" + _addAnswerForm.id;
    } else if(type == 2) {
        answerCorrectnessDiv.className = "squaredThree-inverse";
        answerCorrectnessDiv.style.marginRight = "80px";
        answerCorrectnessDiv.style.marginTop = "-20px";

        answerCorrectnessCheckbox.type = "checkbox";
        answerCorrectnessCheckbox.name = "answerChkBox" + _addAnswerForm.id;
        answerCorrectnessCheckbox.style.display = "none";
        answerCorrectnessCheckbox.id = "answerChkBox" + _addAnswerForm.id;

        answerCorrectnessLabel.style.marginLeft = "0px";
        answerCorrectnessLabel.for = "answerChkBox" + _addAnswerForm.id;
        answerCorrectnessDiv.onclick = function() {
            __check(answerCorrectnessCheckbox);
        };
    }
    answerCorrectnessSpan.style.marginLeft = "30px";


    //add answer correctness info and checkbox to right alignment container
    if(type != 4) {
        answerCorrectnessDiv.appendChild(answerCorrectnessCheckbox);
        answerCorrectnessDiv.appendChild(answerCorrectnessLabel);
        answerCorrectnessDiv.appendChild(answerCorrectnessSpan);
        divRight.appendChild(answerCorrectnessDiv);
    }
    //add right alignment container to third cell of answer tools table
    answerToolsTableTrTd3.appendChild(divRight);
    //add cells to row
    answerToolsTableTr.appendChild(answerToolsTableTrTd1);
    answerToolsTableTr.appendChild(answerToolsTableTrTd2);
    answerToolsTableTr.appendChild(answerToolsTableTrTd3);
    //add row to answer tools table
    answerToolsTable.appendChild(answerToolsTableTr);
    //add answer tools table to answer form (if it is not string question)
    if(type != 3) {
        place.appendChild(answerToolsTable);
    }

    $("#maxAnswersNum").val(_addAnswerForm.id);

    //increment static answer id
    _addAnswerForm.id++;

    //update preview to show new answer
    _updatePreview(type);

    //add parity question form
    if(parity != null && parity == 1) {
        _addAnswerForm(type, 2);
    }
}






/**
 * @type: on action
 * @action: any_checkbox.click()
 * @description: imitate click() on customized checkboxes
 * @param checkbox - checkbox element to click
 */
function __check(checkbox) {
    checkbox.click();
}
/**
 * @type: on action
 * @action: any_radio_btn.click()
 * @description: imitate click() on customized radio buttons
 * @param radio - radio element to click
 */
function __select(radio) {
    radio.click();
}





/**
 * @type: on action
 * @action: deleteForm[id].click()
 * @description: deletes answer form and refreshes preview area
 * @param id - delete button id
 * @param type - question type
 */
function _deleteAnswerForm(id, type) {
    if(type == 4) {
        deleteAnswerFormCorrelate(id, type);
        return;
    }
    //get answer from id suffix
    id = parseInt(id.replace(new RegExp("deleteForm"), ""));
    //get answer form by its own id + suffix
    var answer = document.getElementById("answer" + id);
    //remove answer form
    answer.parentNode.removeChild(answer);
    //decrement static answer id if the removed answer form was the last one
    if(id == _addAnswerForm.id - 1) {
        _addAnswerForm.id--;
    }
    //update preview to show answers without deleted one
    _updatePreview(type);


    /**
     * @type: service
     * @description: deletes correlate question's answer form
     * @see: _deleteAnswerForm
     */
    function deleteAnswerFormCorrelate(id, type) {
        //get answer from id suffix
        id = parseInt(id.replace(new RegExp("deleteForm"), ""));
        //get answer forms by theirs own id + suffix
        var answer = document.getElementById("answer" + id);
        if(answer.parity == 1) {
            var answer2 = document.getElementById("answer" + (id + 1));
            id = id + 1;
        } else {
            answer2 = document.getElementById("answer" + (id - 1));
        }
        //remove answer forms
        answer.parentNode.removeChild(answer);
        answer2.parentNode.removeChild(answer2);
        //decrement static answer id if the removed answer form was the last one
        if(id == _addAnswerForm.id - 1) {
            _addAnswerForm.id--;
            $("#maxAnswersNum").val(_addAnswerForm.id);
        }
        //update preview to show answers without deleted one
        _updatePreview(type);
    }
}

