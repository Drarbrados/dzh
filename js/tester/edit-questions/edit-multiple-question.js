
function changeImageSize(id) {
    _changeImageSize(id, 2);
}

function addImage(button, imageName) {
    _addImage(button, imageName, 2);
}

function deleteImage(button) {
    _deleteImage(button, 2);
}

function resizeHorizontal() {
    _resizeHorizontal(2);
}

function moveQuestionsArea() {
    _moveQuestionsArea(2);
}

function updatePreview() {
    _updatePreview(2);
}

function addAnswerForm() {
    _addAnswerForm(2);
}

function fixColumnHeightsInPreview() {
    _fixColumnHeightsInPreview();
}
