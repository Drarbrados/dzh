function filter(phrase, cellIndex, offset) {
    var table = document.getElementById("table");
    if(offset == null) {
        offset = 1;
    } else {
        offset += 1;
    }
    for (var r = offset; r < table.rows.length; r++) {
        var a = 1;
        var row = table.rows[r].cells[cellIndex].innerHTML;
        for (var i = 0; i < phrase.length; i++) {
            if (row.toLowerCase().indexOf(phrase.toLowerCase()) < 0) {
                a = 0;
                break;
            }
        }
        if (a == 0) {
            table.rows[r].style.display = 'none';
        } else {
            table.rows[r].style.display = '';
        }
    }
}

function colorFilter(phrase, cellIndex, offset) {
    var table = document.getElementById("table");
    if(offset == null) {
        offset = 1;
    } else {
        offset += 1;
    }
    for (var r = offset; r < table.rows.length; r++) {
        var a = 1;
        var row = table.rows[r].cells[cellIndex].innerHTML;
        for (var i = 0; i < phrase.length; i++) {
            if (row.toLowerCase().indexOf(phrase.toLowerCase()) < 0) {
                a = 0;
                break;
            }
        }
        if (a == 0) {
            table.rows[r].style.opacity = "0.5";
        } else {
            table.rows[r].style.opacity = "1";
        }
    }
}



