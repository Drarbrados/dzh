package tester.servlets.qgroup;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tester.dao.exceptions.DAOException;

public class QuestionsGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 2230609860693387591L;
	private static final String page ="jsp/questionsGroup.jsp";

	// forward to jsp
	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
		if(request.getParameter("IWantSomeException") != null){
	    	try{
	    		throw new DAOException("Oh my god, EXCEPTION!");
	    	}
	    	catch(DAOException ee){
	    		throw new IOException("Нешта здарылася :(", ee);
	    	}
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(page);
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}
    }
	
	// Edit or add new qgr
	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
		
    }
}
