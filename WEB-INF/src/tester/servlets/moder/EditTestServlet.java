package tester.servlets.moder;


import tester.dao.QuestionDAO;
import tester.dao.TesterDAOFactory;
import tester.transfers.algorithms.AbstractAlgorithmStrategy;
import tester.transfers.algorithms.Algorithm;
import tester.transfers.algorithms.GaussByDifficultyAlgorithm;
import tester.transfers.algorithms.SimpleRandomizeAlgorithm;
import tester.transfers.tests.Test;
import tester.transfers.tests.TestTreeNode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class EditTestServlet extends HttpServlet {

    private QuestionDAO questionDAO = TesterDAOFactory.getFactory().getQuestionDAO();

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        TestTreeNode testTreeNode = (TestTreeNode)request.getSession().getAttribute("titleTest");
        Test test = testTreeNode.getNodeValue();

        request.setAttribute("test", test);



        if(request.getSession().getAttribute("frameTest") == null) {
            request.getSession().setAttribute("frameTest", test);
        } else {
            test = (Test)request.getSession().getAttribute("frameTest");
        }

        request.setAttribute("questionList", questionDAO.getQuestionsFor(test));

        AbstractAlgorithmStrategy.setLocale("RUS");
        ArrayList<Algorithm> algorithms = new ArrayList<>();
        algorithms.add(new SimpleRandomizeAlgorithm());
        algorithms.add(new GaussByDifficultyAlgorithm());


        request.setAttribute("algorithmList", algorithms);

        RequestDispatcher dispatcher =
                request.getRequestDispatcher("jsp/testEdit.jsp");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("--------------------");
        System.out.println("testname " + request.getParameter("testname"));
        System.out.println("testtime " + request.getParameter("testtime"));
        System.out.println("testattemptsperday " + request.getParameter("testattemptsperday"));
        System.out.println("testmincorrectnum " + request.getParameter("testmincorrectnum"));
        System.out.println("testquestionsnumber " + request.getParameter("testquestionsnumber"));
        System.out.println("testcreatedate " + request.getParameter("testcreatedate"));
        System.out.println("testcreator " + request.getParameter("testcreator"));
        System.out.println("testmodifydate " + request.getParameter("testmodifydate"));
        System.out.println("testmodifyer " + request.getParameter("testmodifyer"));
        System.out.println("groupname " + parseArray(request.getParameterValues("groupname")));
        System.out.println("groupid " + parseArray(request.getParameterValues("groupid")));
        System.out.println("groupalgorithmindex " + parseArray(request.getParameterValues("groupalgorithmindex")));
        System.out.println("groupalgorithmparams " + parseArray(request.getParameterValues("groupalgorithmparams")));
        System.out.println("groupquestions " + parseArray(request.getParameterValues("groupquestions")));
        System.out.println("--------------------");



        doGet(request, response);
    }


    private String parseArray(String array[]) {
        String res = "";
        for(String s : array) {
            if(res.equals("")) {
                res = s;
            } else {
                res = res + " ||| " + s;
            }
        }
        return res;
    }

}
