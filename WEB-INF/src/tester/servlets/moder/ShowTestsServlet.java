package tester.servlets.moder;

import tester.dao.ModerableTestDAO;
import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.transfers.questions.StringQuestion;
import tester.transfers.tests.ModerableTest;
import tester.transfers.tests.SalutableTest;
import tester.transfers.tests.Test;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowTestsServlet extends HttpServlet {

	public static final long serialVersionUID = 1L;

	private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
	//private static SalutableTestDAO  testsDAO = factory.getSalutableTestDAO();
	private static ModerableTestDAO moderTestsDAO = factory.getModetableTestDAO();
	//GET ROOT TEST, NOT 1

	private int id;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
		TestTreeNode testNode = null;
		id = -1;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch (NumberFormatException e) {

		}
		User user = (User)request.getSession().getAttribute("user");
		try {
			testNode = moderTestsDAO.getTestTreeFor(user);
			TestTreeNode tmp = null;
			if (testNode != null)
				tmp = testNode.subTreeSearch(id);
			if (tmp != null) {
				testNode = tmp;
			}
			if(testNode == null){
				response.sendError(403);
				return;
			}
			request.setAttribute("titleTest", testNode);
			request.getSession().setAttribute("titleTest", testNode);
			request.setAttribute("testList", testNode.getChildrenNodes());

			RequestDispatcher dispatcher;
			if(!testNode.getNodeValue().isTest()) {
				dispatcher = request.getRequestDispatcher("jsp/moder/tests.jsp");
			} else {
				dispatcher = request.getRequestDispatcher("edittest");
			}
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			}
		} catch (DAOException e) {
			e.printStackTrace();
			//TODO: Log me fully
		}
    }

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TestTreeNode testNode = null;
		try {
			User user = (User)request.getSession().getAttribute("user");
			testNode = moderTestsDAO.getTestTreeFor(user);
			TestTreeNode tmp = null;
			if (testNode != null)
				tmp = testNode.subTreeSearch(id);
			if (tmp != null) {
				testNode = tmp;
			}
			if(testNode == null){
				response.sendError(403);
				return;
			}
			List<TestTreeNode> nodes = testNode.getChildrenNodes();
			if(request.getParameter("addTopic") != null) {
				String search = "Новый Раздел";
				String postfix = " 0";
				int temp = 0;
				Test topic = new SalutableTest("Новый Раздел" + postfix, 0, 0, 0,
						testNode.getNodeValue().getId(), false);
				for(int i = 0; i < nodes.size(); i++) {
					boolean flag = false;
					for(TestTreeNode node : nodes) {
						if(node.getNodeValue().equals(topic)) {
							flag = true;
							break;
						}
					}
					if(!flag) {
						break;
					} else {
						temp++;
						postfix = " " + String.valueOf(temp);
						topic.setTitle(search + postfix);
					}
				}
				moderTestsDAO.addTest(topic);
				TestTreeNode topicNode = new TestTreeNode(user, topic);
				testNode.addChildNode(topicNode);
			} else if(request.getParameter("addTest") != null) {
				String search = "Новый Тест";
				String postfix = " 0";
				int temp = 0;
				Test test = new SalutableTest("Новый Тест" + postfix, 5 * 60, 2, 1,
						testNode.getNodeValue().getId(), true);
				for(int i = 0; i < nodes.size(); i++) {
					boolean flag = false;
					for(TestTreeNode node : nodes) {
						if(node.getNodeValue().equals(test)) {
							flag = true;
							break;
						}
					}
					if(!flag) {
						break;
					} else {
						temp++;
						postfix = " " + String.valueOf(temp);
						test.setTitle(search + postfix);
					}
				}
				moderTestsDAO.addTest(test);
				TestTreeNode tNode = new TestTreeNode(user, test);
				testNode.addChildNode(tNode);
			} else if(request.getParameter("changeName") != null) {
				String newName = request.getParameter("changeName");
				int id_ = Integer.parseInt(request.getParameter("modId"));
				for(TestTreeNode node : nodes) {
					if(node.getNodeValue().getId() == id_) {
						Test test = node.getNodeValue();
						test.setTitle(newName);
						moderTestsDAO.update(test);
						break;
					}
				}
			}
		} catch (DAOException e) {
			e.printStackTrace();
		}

		request.setAttribute("titleTest", testNode);
		request.setAttribute("testList", testNode.getChildrenNodes());

		RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/moder/tests.jsp");
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}
	}
}
