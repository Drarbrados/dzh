package tester.servlets.moder;

import tester.dao.*;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.ElementIsMissingException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.transfers.Answer;
import tester.transfers.tests.SalutableTest;
import tester.transfers.questions.*;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class EditQuestionServlet extends HttpServlet {



    private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
    private static SalutableTestDAO testsDAO = factory.getSalutableTestDAO();
    private static QuestionDAO questionDAO = factory.getQuestionDAO();
    private static AnswerDAO answerDAO = factory.getAnswerDAO();
    private static PictureDAO pictureDAO = factory.getPictureDAO();

    // What the fuck !?!?!?!?!
    private SalutableTest test;
    private int questionType = 1;
    private ArrayList<Answer> answers = new ArrayList<>();
    private Question question;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if(request.getParameter("questionType") != null &&
                !request.getParameter("questionType").equals("")) {
            questionType = Integer.parseInt(request.getParameter("questionType"));
        }

        if(request.getParameter("questionId") != null &&
                "0".equals(request.getParameter("questionId"))){
            question = null;
        } else {
            try {
                question = questionDAO.getQuestionById(
                        Integer.parseInt(request.getParameter("questionId")));
            }catch(NumberFormatException e){
                e.printStackTrace();
                System.out.println("Hello Exception!");}
        }

        if(request.getParameter("testId") != null) {
            try {
                test = testsDAO.getTestById(Integer.parseInt(request.getParameter("testId")));
            } catch (IncorrectInputDataException e) {
                e.printStackTrace();
            }
        }
        request.setAttribute("test", test);

        if(request.getParameter("questionId") == null) {
            request.setAttribute("question", null);
        } else {

        }

        sendAttributes(request);

        RequestDispatcher dispatcher =
                request.getRequestDispatcher("jsp/moder/questions/editQuestion.jsp");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        if("0".equals(request.getParameter("questionId")) ||
//                request.getParameter("questionId") == null ){
//            question = null;
//        }

        int maxAnswersNum = Integer.parseInt(request.getParameter("maxAnswersNum"));
        answers.clear();
        int parity = 1;
        for(int i = 1; i <= maxAnswersNum; i++) {
            if(request.getParameter("answerText" + i) != null) {
                answers.add(new Answer(request.getParameter("answerText" + i)));
                if(request.getParameter("_AnswerImage" + i) != null &&
                        !"".equals(request.getParameter("_AnswerImage" + i))) {
                    int aImageSize = Integer.parseInt(request.getParameter("answerImageSize" + i).
                            replaceAll("\\D", ""));
                    answers.get(answers.size() - 1).setPictureSize(aImageSize);
                    String aImage = request.getParameter("_AnswerImage" + i);
                    aImage = aImage.substring(aImage.indexOf("/") + 1);
                    System.out.println("ANSWX");
                    answers.get(answers.size() - 1).setPictureId(aImage);
                    try {
                        File src = pictureDAO.getPictureFile(
                                answers.get(answers.size() - 1).getPictureId());
                        File dest = src.getParentFile().getParentFile();
                        String destPath = dest.getAbsolutePath().concat("/perm/");
                        destPath = destPath.concat("P".concat(aImage.substring(1)));
                        dest = new File(destPath);
                        System.out.println("src  " + src + "\n" + "dest " + dest);
                        src.renameTo(dest);
                        answers.get(answers.size() - 1).setPictureId("P".concat(aImage.substring(1)));
                    } catch (ElementIsMissingException e) {
                        aImage = "P".concat(aImage.substring(1));
                        answers.get(answers.size() - 1).setPictureId(aImage);
                    }

                } else {
                    //delete current on filesystem
                }
                switch (questionType) {
                    case 1:
                        if (request.getParameter("radioForm") != null) {
                            int val = Integer.parseInt(request.getParameter("radioForm"));
                            if (i == val) {
                                answers.get(answers.size() - 1).setComb(Boolean.toString(true));
                            }
                        }
                        break;
                    case 2:
                        if (request.getParameter("answerChkBox" + i) != null) {
                            if(request.getParameter("answerChkBox" + i).equals("on")) {
                                answers.get(answers.size() - 1).setComb(Boolean.toString(true));
                            }
                        }
                        break;
                    case 3:
                        break;
                    case 4:
                        String par = Integer.toString(parity);
                        if(parity < 9) {
                            par = "0" + par;
                        }
                        answers.get(answers.size() - 1).setComb(par);
                        break;
                }
                parity++;
            }
        }

        //System.out.println(answers);

        String questionText = request.getParameter("questionText");


        int prize = Integer.parseInt(request.getParameter("mark"));
        int answersInRow = Integer.parseInt(request.getParameter("answersInRow"));
        int answerAreaSize = Integer.parseInt(request.getParameter("answerAreaSize"));
        int answerAreaLeftOffset = Integer.parseInt(request.getParameter("answerAreaLeftOffset"));
        Customization customization = new Customization();
        customization.setAnswersInRow(answersInRow);
        customization.setAnswerAreaSize(answerAreaSize);
        customization.setAnswerAreaLeftOffset(answerAreaLeftOffset);

        if(question == null) {
            switch (questionType) {
                case 1:
                    question = new OneOfSeveralQuestion(questionText, test);
                    break;
                case 2:
                    question = new SeveralMultipleQuestion(questionText, test);
                    break;
                case 3:
                    question = new StringQuestion(questionText, test);
                    break;
                case 4:
                    question = new CorrelateQuestion(questionText, test);
                    break;
            }
        } else {
            question.setText(questionText);
        }
        question.setTest(test);
        if(!"".equals(request.getParameter("_QuestionImage"))) {
            int qImageSize = Integer.parseInt(request.getParameter("questionImageSize").
                    replaceAll("\\D", ""));
            question.setPictureSize(qImageSize);
            String qImage = request.getParameter("_QuestionImage");
            System.out.println("QIMAGE " + qImage);
            qImage = qImage.substring(qImage.indexOf("/") + 1);
            if(!pictureDAO.compareTempPerm(qImage, question.getPictureId())) {
                question.setPictureId(qImage);
                try {
                    File src = pictureDAO.getPictureFile(question.getPictureId());
                    File dest = src.getParentFile().getParentFile();
                    String destPath = dest.getAbsolutePath().concat("/perm/");
                    destPath = destPath.concat("P".concat(qImage.substring(1)));
                    dest = new File(destPath);
                    System.out.println("src  " + src + "\n" + "dest " + dest);
                    src.renameTo(dest);
                    question.setPictureId("P".concat(qImage.substring(1)));
                    System.out.println("PID " + question.getPictureId());
                } catch (ElementIsMissingException e) {
                    e.printStackTrace();
                }
            }
        } else {
            question.setPictureId(null);
            //delete current on filesystem
        }


        question.setPrize(prize);
        question.setCustomization(customization);
        try {
            if(question.getId() == 0) {
                question.setCreator((User)request.getSession().getAttribute("user"));
                questionDAO.addQuestion(question);
            } else {
                question.setModifier((User) request.getSession().getAttribute("user"));
                questionDAO.updateQuestionData(question);
            }
            for(Answer answer : answers) {
                answer.setQuestionId(question.getId());
                answerDAO.addAnswer(answer);
                question.addAvailableAnswer(answer);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }




        sendAttributes(request);

        RequestDispatcher dispatcher =
                request.getRequestDispatcher("jsp/moder/questions/editQuestion.jsp");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }

    public void sendAttributes(HttpServletRequest request)
            throws ServletException, IOException {
        request.setAttribute("question", question);
        request.setAttribute("questionType", questionType);
        request.setAttribute("test", test);
    }
}
