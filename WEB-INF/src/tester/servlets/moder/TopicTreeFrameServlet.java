package tester.servlets.moder;

import tester.dao.ModerableTestDAO;
import tester.dao.QuestionDAO;
import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;
import tester.service.Logger;
import tester.transfers.questions.Question;
import tester.transfers.tests.HierarchyTest;
import tester.transfers.tests.ModerableTest;
import tester.transfers.tests.PermissionTestWrapper;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class TopicTreeFrameServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(TopicTreeFrameServlet.class);

    private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
    @Deprecated
    private static SalutableTestDAO salutableTestsDAO = factory.getSalutableTestDAO();
    private static ModerableTestDAO moderableTestDAO = factory.getModetableTestDAO();
    private static QuestionDAO questionDAO = factory.getQuestionDAO();

    private int tid = -1;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("user");
        TestTreeNode<ModerableTest> root = null;
        try {
            root = moderableTestDAO.getTestTreeFor(user);
        } catch (DAOException e) {
            String msg = LOG.error(e, "Can't get TestTreeNode for User: '%s'",
                    user == null ? "null" : user.toString());
            throw new IOException(msg, e);
        }
        try{

            tid = Integer.parseInt(request.getParameter("tid"));
        } catch (NumberFormatException e) {
            System.out.println("ПИЧАЛЬКА!!!!!!!!!!!!!!!!!!!");
        }

        doPost(request, response);
    }

    private List<HierarchyTest> createHierarchyTestsList(TestTreeNode<ModerableTest> node){
        LinkedList<HierarchyTest> list = new LinkedList<>();
        appendNodeToList(list, node, 0);
        return list;
    }

    private void appendNodeToList(List<HierarchyTest> list, TestTreeNode<ModerableTest> node, int depth){
        HierarchyTest hierarchy = null;

        hierarchy = new HierarchyTest(
                node.getNodeValue(),
                depth,
                questionDAO.getQuestionsNumberFor(node.getNodeValue()),
                node.isNodeAccessible()
        );
        list.add(hierarchy);

        ++depth;
        for(TestTreeNode<ModerableTest> subNode : node.getChildrenNodes()){
            appendNodeToList(list, subNode, depth);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("test", null);// вот же ж жуй. в сессие есть такая же пропертя!

        TestTreeNode<ModerableTest> root = null, node = null;
        User user = (User) request.getSession().getAttribute("user");
        try {
            root = moderableTestDAO.getTestTreeFor(user);
        } catch (DAOException e) {
            String msg = LOG.error(e, "Unexpected error.");
            throw new IOException(msg, e);
        }

        //if root == null ..
        if(root != null) {
            int id = tid;
            node = root.subTreeSearch(id);
        }
        // if node == null (equals prev.)
        if(node == null){
            LOG.error("Can't get node to edit. %s.", root == null ? "Permission denude" : "Can't find test by id");
            response.sendError(403);
            return;
        }

        LinkedList<PermissionTestWrapper> list = new LinkedList<>();
        insertParents(list, node);
        request.setAttribute("testParents", list);

        request.setAttribute("test",
            new PermissionTestWrapper(node.getNodeValue(), node.isNodeAccessible())
         );

        list = new LinkedList<>();
        appendChildren(list, node);
        request.setAttribute("testChildrens", list);

        if(node.isNodeAccessible()) {
            ArrayList<Question> questions = questionDAO.getQuestionsFor(node.getNodeValue());
            request.setAttribute("questionList", questions);
        }
        else{
            request.setAttribute("questionList", new LinkedList<Question>());
        }

        request.getSession().setAttribute("frameTest", node.getNodeValue());


        RequestDispatcher dispatcher =
                request.getRequestDispatcher("jsp/moder/questions/testTreeFrame.jsp");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }

    }

    private void insertParents(LinkedList<PermissionTestWrapper> list, TestTreeNode<ModerableTest> node){
        if(node.isRoot())
            return;

        PermissionTestWrapper test = null;
        do{
            node = node.getParentNode();
            test = new PermissionTestWrapper(node.getNodeValue());
            test.setAccessible(node.isNodeAccessible());
            list.addFirst(test);
        }while(!node.isRoot());
    }

    private void appendChildren(List<PermissionTestWrapper> list, TestTreeNode<ModerableTest> node) {
        for(TestTreeNode<ModerableTest> subNode : node.getChildrenNodes()){
            list.add(
                    new PermissionTestWrapper(subNode.getNodeValue(), subNode.isNodeAccessible())
            );
        }
    }
}
