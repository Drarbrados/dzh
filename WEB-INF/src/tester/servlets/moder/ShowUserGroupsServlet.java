package tester.servlets.moder;

import tester.dao.GroupDAO;
import tester.dao.ModerableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;
import tester.transfers.Group;
import tester.transfers.tests.ModerableTest;
import tester.transfers.tests.SalutableTest;
import tester.transfers.tests.Test;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ShowUserGroupsServlet extends HttpServlet {

	public static final long serialVersionUID = 1L;

	private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
	//private static SalutableTestDAO  testsDAO = factory.getSalutableTestDAO();
	private static ModerableTestDAO moderTestsDAO = factory.getModetableTestDAO();
	//GET ROOT TEST, NOT 1

	private static GroupDAO groupDAO = factory.getGroupDAO();
	private static ModerableTestDAO testDAO = factory.getModetableTestDAO();

	private int id;
	private Test test;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

		User user = (User)request.getSession().getAttribute("user");

		if(request.getParameter("tid") != null) {
			int tid = Integer.parseInt(request.getParameter("tid"));
			test = new ModerableTest(tid);
		}

		id = 1;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch (NumberFormatException e) {

		}
		try {
			testDAO.getTestVisibilityById(test);
			Group root = groupDAO.getGroupById(id);
			if(test.checkAvailableGroupSet(root)) {
				root.setVisible();
			}
			List<Group> groups = groupDAO.getChildrensFromGroup(root);
			for(Group g : groups) {
				if(test.checkAvailableGroupSet(g)) {
					g.setVisible();
				}
			}
			request.setAttribute("rootGroup", root);
			request.setAttribute("groups", groups);

		} catch (DAOException e) {
			e.printStackTrace();
			//TODO: Log me fully
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/moder/userGroupsTable.jsp");
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}
    }

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			if (request.getParameter("plus") != null) {
				int gid = Integer.parseInt(request.getParameter("plus"));
				testDAO.openToGroup(test, new Group(gid));
			} else if (request.getParameter("minus") != null) {
				int gid = Integer.parseInt(request.getParameter("minus"));
				testDAO.closeToGroup(test, new Group(gid));
			}
		} catch (DAOException e) {
			e.printStackTrace();
		}

		doGet(request, response);
	}
}
