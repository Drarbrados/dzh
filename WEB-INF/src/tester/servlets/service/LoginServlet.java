package tester.servlets.service;

import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.AuthenticationException;
import tester.service.Logger;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(LoginServlet.class);
    private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
    private static UserDAO userDAO = factory.getUserDAO();

	public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/login.jsp");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = null;
        try {
            user = userDAO.getUser(login, password);
        } catch (AuthenticationException e) {
            LOG.trace("Authentication exception: login=\"%s\", password=\"%s\"", login, password);
        }
        if(user != null) {
            request.getSession().setAttribute("user", user);
            response.sendRedirect("menu");
        } else {
            doGet(request, response);
        }
    }
}
