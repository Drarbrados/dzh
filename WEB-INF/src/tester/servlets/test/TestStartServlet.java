package tester.servlets.test;

import java.io.*;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;


import tester.transfers.Exam;
import tester.transfers.users.User;
import tester.dao.exceptions.DAOException;
import tester.service.Logger;
import tester.transfers.tests.SalutableTest;

public class TestStartServlet extends HttpServlet {
	private static final long serialVersionUID = 11L;
	private static final Logger LOG = Logger.getLogger(TestStartServlet.class);
	private String page;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {


		SalutableTest test = (SalutableTest)request.getSession().getAttribute("test");
		request.setAttribute("titleTest", test);


		RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/test/test.jsp");
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}
    }   
    public void doPost(HttpServletRequest request, final HttpServletResponse response)
        	throws ServletException, IOException {

		SalutableTest test = (SalutableTest)request.getSession().getAttribute("test");
		User user = (User) request.getSession().getAttribute("user");
		Long sTime = (Long)request.getSession().getAttribute("startingTime");

		try {
			Exam exam = test.createExam(user);
			if(request.getParameter("test") != null && sTime == null) {
				exam.setTraining(false);
			}
			request.getSession().setAttribute("exam", exam);
			exam.startExam();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		response.sendRedirect("showQuestion");
    }
}
