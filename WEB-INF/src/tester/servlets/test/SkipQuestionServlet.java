package tester.servlets.test;

import tester.transfers.Exam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SkipQuestionServlet extends HttpServlet {
 
	public static final long serialVersionUID = 1L;

    private String page = "showQuestion";

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

        System.out.println("SKIP_QUESTION_SERVLET");

        Exam exam = (Exam) request.getSession().getAttribute("exam");

        exam.moveCurrentQuestionToEnd();
		response.sendRedirect(page);
    }
}
