package tester.servlets.test;

import tester.transfers.Answer;
import tester.transfers.Exam;
import tester.transfers.questions.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckAnswersServlet extends HttpServlet {
 
	public static final long serialVersionUID = 1L;
    

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        	throws ServletException, IOException {

		System.out.println("CHECK_ANSWERS_SERVLET");

		Exam exam = (Exam)request.getSession().getAttribute("exam");

		Question question = exam.getQuestion();

		//refactoring


		if (question instanceof OneOfSeveralQuestion) {
			for (int i = 1; request.getParameter("trueComb" + i) != null; i++) {
				if (request.getParameter("trueComb" + i).equals("true")) {
					question.addUserAnswer(question.getAvailableAnswers().get(i - 1));
				}
			}
		} else if (question instanceof SeveralMultipleQuestion) {
			for (int i = 1; request.getParameter("trueComb" + i) != null; i++) {
				if (request.getParameter("trueComb" + i).equals("true")) {
					question.addUserAnswer(question.getAvailableAnswers().get(i - 1));
				}
			}
		} else if (question instanceof StringQuestion) {
			question.addUserAnswer(new Answer(request.getParameter("trueComb1")));
		} else if (question instanceof CorrelateQuestion) {
			for (int i = 1; request.getParameter("trueComb" + i) != null; i++) {
				question.addUserAnswer(new Answer("", request.getParameter("trueComb" + i)));
			}
		}
		//refactoring
		exam.passQuestion();


		//refactoring
    	response.sendRedirect("showQuestion");
    }    
}
