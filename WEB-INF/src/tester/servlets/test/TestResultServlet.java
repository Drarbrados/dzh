package tester.servlets.test;

import tester.transfers.Exam;
import tester.transfers.tests.Test;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


public class TestResultServlet extends HttpServlet {
 
	public static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

		Exam exam = (Exam)request.getSession().getAttribute("exam");
		exam.resetTimer();

		RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/test/testResult.jsp");
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}

    }   
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        	throws ServletException, IOException {

		request.getSession().setAttribute("exam", null);
		Test test = (Test)request.getSession().getAttribute("test");

		response.sendRedirect("menu?id=" + test.getId());
    }    
}
