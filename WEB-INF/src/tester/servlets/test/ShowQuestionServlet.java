package tester.servlets.test;

import tester.transfers.questions.Question;
import tester.transfers.users.User;
import tester.transfers.Exam;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ShowQuestionServlet extends HttpServlet {
 
	public static final long serialVersionUID = 1L;

    private String page = "";
    private int id = 0;
    private Question question;
	private Exam exam;


    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

		System.out.println("SHOW_QUESTION_SERVLET");

		if(request.getParameter("id") != null) {
			id = new Integer(request.getParameter("id"));
		}

		// TODO: refactoring
		exam = (Exam)request.getSession().getAttribute("exam");

		question = exam.getQuestion();
		if(question != null) {
			request.setAttribute("question", question);
			request.setAttribute("testType", question.getStringQType());
			page = "jsp/test/test.jsp";
		} else {
			page = "testResult";
		}

		request.setAttribute("idTest", id);

		RequestDispatcher dispatcher = request.getRequestDispatcher(page);
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}
    }
}
