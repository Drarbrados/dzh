package tester.servlets.navigation;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.service.Logger;
import tester.transfers.tests.SalutableTest;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

public class ShowMenuServlet extends HttpServlet {
	public static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ShowMenuServlet.class);

	private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
	private static SalutableTestDAO  testsDAO = factory.getSalutableTestDAO();
    
    public void aldDoGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
		int id = 1;//GET ROOT TEST, NOT 1
		if(request.getParameter("id") != null) {
			id = new Integer(request.getParameter("id"));
		}
		ArrayList<SalutableTest> temp;
		temp = testsDAO.getChildrenOf(id);
		request.setAttribute("testList", temp);
		request.setAttribute("idTest", id);
		try {
			SalutableTest test = testsDAO.getTestById(id);
			request.getSession().setAttribute("test", test);
			request.setAttribute("titleTest", test);
			if(test != null && test.isTest()) {
				response.sendRedirect("test");
			} else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/main.jsp");
				if (dispatcher != null) {
					dispatcher.forward(request, response);
				}
			}
		} catch (IncorrectInputDataException e) {
			e.printStackTrace();
		}
    }


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int id = 1;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		}
		catch (NumberFormatException | NullPointerException e){}

		User user = (User)request.getSession().getAttribute("user");
		TestTreeNode<SalutableTest> root = null, node = null;
		try {
			root = testsDAO.getTestTreeFor(user);
		} catch (DAOException e) {
			String msg = LOG.error(e, "Can't get TestTreeNode for User: '%s'",
					user == null ? "null" : user.toString());
			throw new IOException(msg, e);
		}

		if(root != null) {
			node = root.subTreeSearch(id);
			if (node == null)
				node = root;
		}

		forward(node, request, response);
	}

	private void forward(TestTreeNode<SalutableTest> node,
				HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		SalutableTest test = node != null ? node.getNodeValue() : null;

		request.getSession().setAttribute("test", test);
		request.setAttribute("titleTest", test);
		request.setAttribute("testList", createChildrenTestsList(node));
		request.setAttribute("idTest", test != null ? test.getId() : null);

		if(test != null && test.isTest()) {
			response.sendRedirect("test");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/main.jsp");
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			}
		}
	}

	private List<SalutableTest> createChildrenTestsList(TestTreeNode<SalutableTest> node){
		if(node == null)
			return new ArrayList(1);

		int size = node.getChildrenNodesNumber();
		ArrayList<SalutableTest> list = new ArrayList<>(size);

		for(int i = 0; i < size; ++i){
			list.add(node.getChildNode(i).getNodeValue());
		}

		return list;
	}
}
