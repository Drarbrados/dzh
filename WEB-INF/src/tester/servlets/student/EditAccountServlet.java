package tester.servlets.student;

import tester.dao.GroupDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.DAOException;
import tester.transfers.users.Admin;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditAccountServlet extends HttpServlet {

    private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
    private static UserDAO userDAO = factory.getUserDAO();
    private static GroupDAO groupDAO = factory.getGroupDAO();
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/editPersonalData.jsp");
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }  
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

        User user = (User)request.getSession().getAttribute("user");
        user.setLogin(request.getParameter("login"));
        user.setFname(request.getParameter("fName"));
        user.setLname(request.getParameter("lName"));
        if("ADMIN".equals(request.getParameter("role"))) {
            user = new Admin(user);
        }
        if(user.getPassword().equals(request.getParameter("oldpassw"))) {
            if(request.getParameter("passw").equals(request.getParameter("cpassw"))) {
                user.setPassword(request.getParameter("passw"));
            }
        }
        //user.setGroup(groupDAO.getGroupById(request.getParameter("groupId")));

        try {
            userDAO.update(user);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        response.sendRedirect("editUser");
    }
}