package tester.servlets;

import java.text.Normalizer;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tester.dao.PictureDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.ElementIsMissingException;
import tester.dao.filesystem.FSPictureDAO;
import tester.service.Logger;

/**
 * 09.08.2015
 * 						Ужос! 
 * 						Быдло!
 * 						Гопник!
 * 						Рукожоп!
 *
 * 				уже можно удалить
 * @author Dzianis
 *
 */
@Deprecated
public class DownloadFileServlet extends HttpServlet {
	private static final Logger LOG = Logger.getLogger(DownloadFileServlet.class);
	private static final long serialVersionUID = -7582767752859934669L;
	private static final String dir = "images/perm/"; // I won't be property
	
	private final PictureDAO pictureDAO = TesterDAOFactory.getFactory().getPictureDAO();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		
        String fileId = getUtf8Id(request);
        if(fileId == null){
        	writeFines(request, response, fileId);
        	return;
        }
        
        String fileName = null;
        try {
			fileName = pictureDAO.getPicture(fileId).getName();
		} catch (ElementIsMissingException e) {
		}
        if(fileName == null){
        	writeFines(request, response, fileId);
        	return;
        }
        
        fileName = request.getParameter("id");
        fileName = fileName.substring(fileName.indexOf('_') + 1);// ещё одно быдло
        
        response.setContentType("multipart/form-data");
        response.addHeader("Content-Disposition", "form-data; charset=utf-8; filename=\"" + fileName + "\"");
        
		RequestDispatcher dispatcher = request.getRequestDispatcher(dir + fileId);// codemonkey
        if (dispatcher != null)
            dispatcher.forward(request, response);
	}
	
	private String getUtf8Id(HttpServletRequest request){
		String query = request.getQueryString();
		LOG.fatal("GET '%s' query", query);
		int eq = query.indexOf('=') + 1;
		if(eq == 0)
			return null;

		query = query.substring(eq);
		LOG.fatal("CONVERT TO '%s' query", query);
		query = URI.create(query).getPath();// convert URI syntaxes to simple string
		LOG.fatal("RESULT '%s' query", query);
		return query;
	}

	
	private void writeFines(HttpServletRequest request, HttpServletResponse response, String id) throws IOException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		writer.println("<html><head/><body><h3>It's broken, Jimmy =(</h3><p>");
		writer.println(id);
		writer.println("</p><p>");
		writer.println(request.getCharacterEncoding());
		writer.println("</p><p>");
		
		String uri = "file://" + request.getQueryString().substring(3);
		try {
			writer.println(uri + "</p><p>");
			writer.println("URI:  " + URI.create(uri));
			writer.println("</p><p>");
			writer.println("URL:  " + URI.create(uri).toURL());
			writer.println("</p><p>");
			writer.println("FILE1:  " + new File(URI.create(uri)));
			writer.println("</p><p>");
			writer.println("FILE2:  " + new File(URI.create(uri).toURL().toString()));
			writer.println("</p><p> Good URI!!!!!!!");
		} catch (RuntimeException e) {
			writer.println("Bad URI :(   " + uri + "</p><p>");
			writer.println(e);
		}
		
		writer.println("</p></body></html>");
		writer.flush();
	}
}
