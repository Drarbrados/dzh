package tester.servlets.stats;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@Deprecated
public class ShowStatsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

    	RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/stats.jsp");
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}

    }
}
