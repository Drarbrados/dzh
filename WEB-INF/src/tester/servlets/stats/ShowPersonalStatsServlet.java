package tester.servlets.stats;


import tester.dao.StatisticsDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;

import tester.transfers.stats.StatsForTest;
import tester.transfers.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ShowPersonalStatsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
	private static StatisticsDAO statisticsDAO = factory.getStatisticsDAO();

	private User user;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

		user = (User)request.getSession().getAttribute("user");

		List<StatsForTest> stats = null;
		try {
			stats = statisticsDAO.getPersonalStats(user);
		} catch (DAOException e) {
			e.printStackTrace();
		}

		request.setAttribute("stats", stats);


		RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/stats.jsp");
		if (dispatcher != null) {
			dispatcher.forward(request, response);
		}
    }
}
