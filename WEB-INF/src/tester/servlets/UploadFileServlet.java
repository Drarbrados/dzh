package tester.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import tester.dao.PictureDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;


/**
 * Move me somewhere.
 * 
 * @author Dzianis
 *
 */
@MultipartConfig(
		fileSizeThreshold=1024*1024,//1МБ   - файлы больше 1МБ кэшируются на жёстком диске
		maxFileSize=1024*1024*8,// 8MБ      - max размер файла
		maxRequestSize=1024*1024*9// 8MБ    - max размер запроса
)
public class UploadFileServlet extends HttpServlet {
	private static final long serialVersionUID = -7582767752859934669L;
	private static final String pageJSP = "/jsp/uploadAnswer.jsp";
	
	private final PictureDAO pictureDAO = TesterDAOFactory.getFactory().getPictureDAO();
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

		String id = null;
		boolean ok = false;
		Part part = null;
		try{
			String name = request.getParameter("fileName");
			part = request.getPart("filePart");
			id = pictureDAO.saveLikeTemp(
					part.getInputStream(), name );
			
			ok = true;
		}catch (DAOException | NullPointerException | IllegalStateException e) {
			ok = false;
		}
		finally{
			if(part != null)
				part.delete();
		}
		
		request.setAttribute("OK", ok ? "true" : "false");
		request.setAttribute("ID", encodeStringForJavaScript(id));
        
		RequestDispatcher dispatcher = request.getRequestDispatcher(pageJSP);
        if (dispatcher != null)
            dispatcher.forward(request, response);
	}
	
	
	private String encodeStringForJavaScript(String str){
		if(str == null)
			return "";
		
		int index = 0, len = str.length(), code;
		StringBuilder sb = new StringBuilder(len + 4);
		
		while(index < len){// fix bug in JavaScript
			code = str.charAt(index++);
			if(code == '\'' || code == '"' || code == '\\')
				sb.append('\\');
			sb.appendCodePoint(code);
		}
		
		return sb.toString();
	}

}
