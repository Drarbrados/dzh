package tester.tests;

import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.transfers.Exam;
import tester.transfers.questions.Question;
import tester.transfers.tests.SalutableTest;
import tester.transfers.users.User;

import java.util.TreeSet;

/**
 * Created by DzianisH on 21.03.2015.
 */
public class ExamTesting {

    public static void main(String xxx[]) throws IncorrectInputDataException, DAOException {
        TesterDAOFactory factory = TesterDAOFactory.getFactory();
        SalutableTestDAO salutableDAO = factory.getSalutableTestDAO();
        UserDAO userDAO = factory.getUserDAO();

        User user = userDAO.getUser("uppshnica");
        SalutableTest test = salutableDAO.getTestById(10); // 2sem_MathModel 3 1

        Exam exam = test.createExam(user);
        exam.startExam();



        TreeSet<Integer> set = new TreeSet<Integer>();
        Question question;
        System.out.println("Result: ");
        boolean ok = true;
        while((question = exam.getQuestion()) != null){
            System.out.println("Yea " + question.getId());
            ok = ok && set.add(question.getId());
            exam.passQuestion();
        }

        System.err.println(ok ? "OK" : "ERROR");
    }
}
