/**
 * 
 */
package tester.tests;

import java.util.ArrayList;

import tester.dao.QuestionDAO;
import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.AccessLevelException;
import tester.dao.exceptions.AuthenticationException;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.transfers.Exam;
import tester.transfers.tests.SalutableTest;
import tester.transfers.questions.Question;
import tester.transfers.users.User;


public class DebugRunner {
	public static void main(String[] args) throws InterruptedException, DAOException {
		TesterDAOFactory factory = TesterDAOFactory.getFactory();
		System.out.println( factory );
		
		
		UserDAO userdao = factory.getUserDAO();
		SalutableTestDAO  testsdao = factory.getSalutableTestDAO();
		try {
			System.out.println("!!!!!!!!!");
			//	User u = userdao.getUser("Lioha", "ja est Bog!");
			User u = userdao.getUser("korol", "1111");
			System.out.println(u);
			System.out.println(userdao.verify(u));
			
			ArrayList<SalutableTest> list = testsdao.getRootTests(u);
			System.out.println(list.size());
			for(SalutableTest test : list)
				System.out.println(test);
			
			System.out.println("\n childrens of MathMod");
			for(SalutableTest test : testsdao.getChildrenOf(list.get(0)))
				System.out.println(test);
			

			try {
				System.out.println("\n owner of MathMod");
				System.out.println(testsdao.getParentOf(list.get(0)));
			} catch (AccessLevelException e) {
				e.printStackTrace();
			}
			

			System.out.println("\n\n get 1sem\n" + list.get(0));
			SalutableTest test = testsdao.getChildrenOf(list.get(0)).get(0);

			System.out.println("\n\nQuestions of 1 sem:");
			QuestionDAO questiondao = factory.getQuestionDAO();
			for(Question q : questiondao.getTestQuestionsForExam(test)){
				System.out.println(q);
			}

			System.out.println("\n");
			//Exam exam = test.createExam(new User());
			//System.out.println(exam);
			
			

			try {
				System.out.println("\n\n");
				System.out.println(testsdao.getTestById(1));
			} catch (IncorrectInputDataException e) {
				e.printStackTrace();
			}
			
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
