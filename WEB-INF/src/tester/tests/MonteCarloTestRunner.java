package tester.tests;

import java.util.LinkedList;

import tester.transfers.algorithms.service.MonteCarloSimulator;
import tester.transfers.algorithms.service.RandomEvent;

/**
 * 
 * @author DzianisH
 *
 */
public class MonteCarloTestRunner {

	public static void main(String[] args) {
		LinkedList<RandomEvent<?>> hipot = new LinkedList<RandomEvent<?>>();
		LinkedList<RandomEvent<?>> res = new LinkedList<RandomEvent<?>>();
		
		RandomEvent<String> event;
		
		event = new RandomEvent<String>("event1", 0);
		hipot.add(event);
		
		event = new RandomEvent<String>("event2", 0);
		hipot.add(event);
		
		event = new RandomEvent<String>("event3", 0);
		hipot.add(event);
		
		event = new RandomEvent<String>("event4", 0);
		hipot.add(event);
		
		event = new RandomEvent<String>("event5", 10);
		hipot.add(event);
		
		event = new RandomEvent<String>("event6", 0);
		hipot.add(event);
		
		event = new RandomEvent<String>("event7", 0);
		hipot.add(event);
		
		event = new RandomEvent<String>("event8", 0);
		hipot.add(event);
		
		
		System.out.println("\tGet Hipothesis:");
		for(RandomEvent<?> ev : hipot){
			System.out.println(ev);
		}
		
		int trails = 2;
		MonteCarloSimulator.simulate(hipot, res, trails);
		System.out.println("\tGet Result after " + trails + " tials:");
		for(RandomEvent<?> ev : res){
			System.out.println(ev);
		}
	}

}
