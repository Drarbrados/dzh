package tester.tests;

import java.net.URISyntaxException;

import tester.dao.ModerableTestDAO;
import tester.dao.SalutableTestDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.transfers.Exam;
import tester.transfers.tests.ModerableTest;
import tester.transfers.tests.SalutableTest;
import tester.transfers.users.User;

public class JustAClass {

	public static void main(String[] args) throws URISyntaxException, DAOException, IncorrectInputDataException {
		TesterDAOFactory factory = TesterDAOFactory.getFactory();
	/*	SalutableTestDAO testsdao = factory.getSalutableTestDAO();

		SalutableTest test = testsdao.getTestById(10);
		System.out.println(test);
		System.out.println(new Exam(test));
	*/
		ModerableTestDAO moderDAO = factory.getModetableTestDAO();
		UserDAO userDAO = factory.getUserDAO();
		User user = userDAO.getUser("korol");
		System.out.println("Let's go!");
		System.out.println(moderDAO.getTestTreeFor(user).subTreeToString());
	}

}
