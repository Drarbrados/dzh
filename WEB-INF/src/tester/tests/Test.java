package tester.tests;

import tester.dao.GroupDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.DAOException;
import tester.service.FillDB;
import tester.transfers.Group;
import tester.transfers.users.User;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.text.SimpleDateFormat;

public class Test {
    public static void main(String args[]) {

        FillDB.clear();
        FillDB.fill();
        System.out.println("HELLO!!!");
        /*Some tests*/
        TesterDAOFactory factory = TesterDAOFactory.getFactory();

        try {
            UserDAO userDAO = factory.getUserDAO();
            List<User> userList = userDAO.getUsersFromGroup(Group.getRootGroup());
            System.out.println("users in root group: \n\t" + userList);
            GroupDAO groupDAO = factory.getGroupDAO();
            List<Group> groupList = groupDAO.getChildrensFromGroup(Group.getRootGroup());
            System.out.println("groups in root group: \n\t" + groupList);
            userList = userDAO.getUsersFromGroup(groupList.get(1));
            System.out.println("users in group ET: \n\t" + userList);
            //System.out.println("third user in group ET: " + userList.get(2));
            //userList.get(2).setLogin("andrei");
            //userDAO.update(userList.get(2));
            userList = userDAO.getUsersFromGroup(groupList.get(1));
            //System.out.println("third user in group ET after update: " + userList.get(2));

        } catch (DAOException e) {
            e.printStackTrace();
        }


        java.util.Date date1 = new java.util.Date(1323859386753L);
        java.util.Date date2 = new java.util.Date(1423859386753L);


        long diff = date2.getTime() - date1.getTime();

        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long diffSeconds = diff / 1000 % 60;

        System.out.println(new Date().getTime());
        System.out.println("date: " + diffDays + " " + diffHours + " " + diffMinutes + " " + diffSeconds);

        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        System.out.println(format.format(calendar.getTime()));


        /*
        strictfp class Something {

            {
                float a = 123456789;
                float b = 123456788;
                System.out.println("WTF!? " + (a - b));
            }

            strictfp float m1() {
                float a = 123456789;
                float b = 123456788;
                return a - b;
            }

        }

        float a = 123456789;
        float b = 123456788;
        Something s = new Something();
        System.out.println("WTF!? " + (s.m1()));*/


        System.out.println("BYE!!!");
    }
}
