package tester.filters;

import tester.transfers.Exam;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;
import java.io.IOException;
import java.util.Date;
import java.util.regex.Pattern;


public class BlockPagesInTestFilter implements Filter {

    private final String SHOW_QUESTION = "/showQuestion";
    private final String SKIP_QUESTION = "/skipQuestion";
    private final String CHECK_ANSWERS = "/checkAnswers";
    private final String TEST_RESULT = "/testResult";
    private final String IMAGES = "/images";
    private final String CSS = "/css";
    private final String JS = "/js";

    private String tb;

    private final String[] availablePages = {
            SHOW_QUESTION,
            SKIP_QUESTION,
            CHECK_ANSWERS,
            TEST_RESULT,
            IMAGES,
            CSS,
            JS};


    @Override
    public void init(FilterConfig config) throws ServletException {
        tb = config.getServletContext().getContextPath();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        Exam exam = (Exam)request.getSession().getAttribute("exam");
        long startingTime = -1;
        if(exam != null) {
            startingTime = exam.getStartingTime();
        }
        String currentPage = request.getRequestURI();
        String contextPath = request.getContextPath();

        String pageToCompare = currentPage.replaceFirst(tb + "/\\w+", "");
        pageToCompare = currentPage.replaceAll(pageToCompare, "");

        //System.out.println("CURRENTPAGE:   " + currentPage);
        //System.out.println("CONTEXTPATH:   " + contextPath);
        //System.out.println("PAGETOCOMPARE: " + pageToCompare);

        //currentPage = currentPage.substring(0, currentPage.lastIndexOf("/"));


        //System.out.println("currURI   " + tb);


        if(startingTime != -1) {
            boolean eq = false;
            for(String s : availablePages) {
                if(contextPath.concat(s).equals(pageToCompare)) {
                    eq = true;
                    break;
                }
            }
            if(!eq) {
                response.sendRedirect(contextPath.concat(SHOW_QUESTION));
            } else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
