package tester.filters;

import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.transfers.users.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;


public class AuthFilter implements Filter {
    private static final String isAdminAttribute = "isAdmin", isModerAttribute = "isModer";

    private String contextPath;
    private String publicURIs[];

    @Override
    public void init(FilterConfig config) throws ServletException {
        contextPath = config.getServletContext().getContextPath();
        initPublicURIs();
    }

    private void initPublicURIs( ){
        publicURIs = new String[6];// pages '/login____sdsdsgsfdg', '/csssdsdsd', '/jsssss' will accessible too
        publicURIs[0] = contextPath + "/login";
        publicURIs[1] = contextPath + "/css";
        publicURIs[3] = contextPath + "/fonts";
        publicURIs[4] = contextPath + "/imgs";
        publicURIs[2] = contextPath + "/js";
        publicURIs[5] = contextPath + "/filldb";
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        User user = (User) request.getSession().getAttribute("user");
        if (user != null) {
            request.getSession().setAttribute(isAdminAttribute, user.isAdmin());
            request.getSession().setAttribute(isModerAttribute, true);
            chain.doFilter(request, response);
        } else {
            if(isPublicURI(request.getRequestURI())){
                chain.doFilter(request, response);
            } else {
                response.sendRedirect("login");
            }
        }
    }

    @Override
    public void destroy(){

    }


    private boolean isPublicURI(String uri){
        for(String publicURI : publicURIs){
            if(uri.startsWith(publicURI) || uri.equals(publicURI)){
                return true;
            }
        }
        return false;
    }
}
