package tester.service;

public abstract class Time {
	private static final long startTime = System.currentTimeMillis() * 1000_000L;

	/**
	 *
	 * @return  the difference, measured in milliseconds, between
	 *          the current time and midnight, January 1, 1970 UT in nanoseconds
	 */
	public static long getNanoTime(){
		return System.nanoTime() + startTime;
	}

/*	public static long getNanoTimerStart(){
		return startTime;
	}
*/
}
