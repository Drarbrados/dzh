package tester.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * @author Denis
 */
public abstract class PropertiesAccess {
	private static final Logger LOG = Logger.getLogger(PropertiesAccess.class);
	private static final String path;
	
	static{
		File folder = null;
		URI uri = null;
		try {
			uri = PropertiesAccess.class.getResource("").toURI();	
			folder = new File(uri).getParentFile().getParentFile();
		} catch (URISyntaxException e) {
			LOG.error(e, "Can't find path to properties.");
		} catch(RuntimeException re){
			LOG.error(re, "Unexcpected exception.");
		}
		
		path = String.format("%s%c%s%c", folder.getParent(),
				File.separatorChar, "conf", File.separatorChar);
		new File(path).mkdirs();
		LOG.info("Using config files path: %s", path);
	}

	/**
	 * Search and return properties is scope's package.
	 * 
	 * @param scope
	 * @param name
	 * @return new Properties
	 * @throws FileNotFoundException
	 */
	public static Properties loadProperties(String name)
			throws FileNotFoundException {
		
		Properties properties = new Properties();
		loadProperties(properties, name);
		
		return properties;
	}
	
	/**
	 * Load Properties in given object.
	 * 
	 * @param properties
	 * @param scope
	 * @param name
	 * @throws FileNotFoundException
	 */
	public static void loadProperties(Properties properties, String name)
			throws FileNotFoundException {
		
		String uri = String.format("%s%s.properties", path, name);
		FileReader freader = null;
		
		try {
			freader = new FileReader(uri);
			properties.load(freader);
		} catch (FileNotFoundException e) {
			LOG.warn(e, "Can't find file: %s", uri);
			throw e;
		} catch (IOException e) {
			doThrow(e, uri);
		}
		finally{
			if(freader != null)
				try {
					freader.close();
				} catch (IOException e) {
					doThrow(e, uri);
				}
		}
	}
	
	
	private static void doThrow(Throwable th, String name)
			throws FileNotFoundException {
		
		String msg = String.format(
				"Unexpected I/O error in file %s.", name);
		
		FileNotFoundException ex = new FileNotFoundException(msg);
		ex.addSuppressed(th);
		
		LOG.error(th, msg); 
		throw ex;
	}
	
}
