package tester.service;


import org.apache.log4j.Level;

public class Logger {

	private final org.apache.log4j.Logger logger;
//	private volatile  boolean isOn = true;
				// TODO: shut down logger
				// TODO: global shutdown
				// TODO: asinc log methods

	public static Logger getLogger(Class<?> clazz){
		return new Logger(clazz);
	}
	private Logger(Class<?> clazz){
		logger = org.apache.log4j.Logger.getLogger(clazz);
	}
	
	public String fatal(String format, Object... args){
		return fatal(null, format, args);
	}
	public String fatal(Throwable err, String format, Object... args){
		String msg = String.format(format, args);
		if(logger.isEnabledFor(Level.FATAL)){
			logger.fatal(msg, err);
		}
		return msg;
	}
	
	
	public String error(String format, Object... args){
		return error(null, format, args);
	}
	public String error(Throwable err, String format, Object... args){
		String msg = String.format(format, args);
		if(logger.isEnabledFor(Level.ERROR)){
			logger.error(msg, err);
		}
		return msg;
	}
	
	
	public String warn(String format, Object... args){
		return warn(null, format, args);
	}
	public String warn(Throwable err, String format, Object... args){
		String msg = String.format(format, args);
		if(logger.isEnabledFor(Level.WARN)){
			logger.warn(msg, err);
		}
		return msg;
	}
	

	public String info(String format, Object... args){
		return info(null, format, args);
	}
	public String info(Throwable err, String format, Object... args){
		String msg = String.format(format, args);
		if(logger.isInfoEnabled()){
			logger.info(msg, err);
		}
		return msg;
	}

	
	public String debug(String format, Object... args){
		return debug(null, format, args);
	}
	public String debug(Throwable err, String format, Object... args){
		String msg = String.format(format, args);
		if(logger.isDebugEnabled()){
			logger.debug(msg, err);
		}
		return msg;
	}
	

	public String trace(String format, Object... args){
		return trace(null, format, args);
	}
	public String trace(Throwable err, String format, Object... args){
		String msg = String.format(format, args);
		if(logger.isTraceEnabled()){
			logger.trace(msg, err);
		}
		return msg;
	}
}
