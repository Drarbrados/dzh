package tester.service;

import tester.dao.*;
import tester.dao.exceptions.DAOException;
import tester.transfers.Answer;
import tester.transfers.Group;
import tester.transfers.tests.SalutableTest;
import tester.transfers.tests.Test;
import tester.transfers.questions.OneOfSeveralQuestion;
import tester.transfers.questions.Question;
import tester.transfers.questions.SeveralMultipleQuestion;
import tester.transfers.users.Admin;
import tester.transfers.users.User;

public abstract class FillDB {

    public static void fill() {
    	try {
            TesterDAOFactory factory = TesterDAOFactory.getFactory();
            factory.getDatabaseData().truncateDatabase();

            GroupDAO groupDAO = factory.getGroupDAO();
            Group belsutGroup = new Group("BelSUT", 1953, 1);
            Group.setRootGroup(belsutGroup);
            groupDAO.addGroup(belsutGroup); //DEFAULT!
            Group etGroup = new Group("ET", 1978, belsutGroup.getId());
            Group mehiGroup = new Group("Mehi", 1978, belsutGroup.getId());
            Group pgsGroup = new Group("PGS", 1978, belsutGroup.getId());
            Group uppGroup = new Group("UPP", 1953, belsutGroup.getId());
            groupDAO.addGroup(etGroup);
            groupDAO.addGroup(mehiGroup);
            groupDAO.addGroup(pgsGroup);
            groupDAO.addGroup(uppGroup);
            Group em1Group = new Group("EM1", 2011, etGroup.getId());
            Group es1Group = new Group("ES1", 2011, etGroup.getId());
            Group et2Group = new Group("ET2", 2010, etGroup.getId());
            groupDAO.addGroup(em1Group);
            groupDAO.addGroup(es1Group);
            groupDAO.addGroup(et2Group);
            Group pa1Group = new Group("PA1", 2011, pgsGroup.getId());
            Group ps1Group = new Group("PS1", 2011, pgsGroup.getId());
            groupDAO.addGroup(pa1Group);
            groupDAO.addGroup(ps1Group);
            Group up2Group = new Group("UP2", 2010, uppGroup.getId());
            groupDAO.addGroup(up2Group);

            UserDAO userDAO = factory.getUserDAO();
            User root = new Admin(new User("root", "1111", "ROOT", "ROOT", belsutGroup));
            User korol = new User("korol", "1111", "Sasha", "Koroliov", etGroup);
            User olya = new User("olka", "1111", "Olya", "Ramka", etGroup);
            User andreika = new Admin(new User("andreika", "1111", "Andrei", "ZhalezKing", etGroup));
            User mishok = new User("mishok", "1111", "Misha", "Evmen", em1Group);
            User lioha = new User("lioha", "1111", "Aliosha", "Ja_ne_Vor!", em1Group);
            User uppshnica = new User("uppshnica", "1111", "SUppshnica", "Caca", uppGroup);
            User chelovek = new User("chelovek", "1111", "Vasya", "Kompot", et2Group);
            userDAO.addUser(root); //DEFAULT!
            userDAO.addUser(korol);
            userDAO.addUser(olya);
            userDAO.addUser(andreika);
            userDAO.addUser(mishok);
            userDAO.addUser(lioha);
            userDAO.addUser(uppshnica);
            userDAO.addUser(chelovek);

            ModerableTestDAO moderableTestDAO = factory.getModetableTestDAO();
            Test belsutTest = new SalutableTest("BelSUT", 0, 0, 0, 1, false);
            Test.setRootTest(belsutTest);
            moderableTestDAO.addTest(belsutTest); //DEFAULT!
            Test mathModelTest = new Test("MathModel", 0, 0, 0, belsutTest.getId(), false);
            Test historyTest = new Test("History", 0, 0, 0, belsutTest.getId(), false);
            moderableTestDAO.addTest(mathModelTest);
            moderableTestDAO.addTest(historyTest);
            Test mathModelSemOneTest = new Test("1sem_MathModel", 0, 0, 0, mathModelTest.getId(), false);
            Test mathModelSemTwoTest = new Test("2sem_MathModel", 0, 0, 0, mathModelTest.getId(), false);
            moderableTestDAO.addTest(mathModelSemOneTest);
            moderableTestDAO.addTest(mathModelSemTwoTest);
            Test mathModelSemTwoT1Test = new Test("2sem_MathModel 1", 0, 0, 0,
                    mathModelSemTwoTest.getId(), false);
            Test mathModelSemTwoT2Test = new Test(421341234, "2sem_MathModel 2", 0, 0, 0,
                    mathModelSemTwoTest.getId(), false);
            Test mathModelSemTwoT3Test = new Test(421341234, "2sem_MathModel 3", 0, 0, 0,
                    mathModelSemTwoTest.getId(), false);
            Test mathModelSemTwoT4Test = new Test("2sem_MathModel 4", 0, 0, 0,
                    mathModelSemTwoTest.getId(), false);
            moderableTestDAO.addTest(mathModelSemTwoT1Test);
            moderableTestDAO.addTest(mathModelSemTwoT2Test);
            moderableTestDAO.addTest(mathModelSemTwoT3Test);
            moderableTestDAO.addTest(mathModelSemTwoT4Test);
            Test mathModelSemTwoT3T1STest = new Test("2sem_MathModel 3 1", 5 * 60, 2, 1,
                    mathModelSemTwoT3Test.getId(), true);
            Test mathModelSemTwoT3T2STest = new Test("2sem_MathModel 3 2", 5 * 60, 2, 1,
                    mathModelSemTwoT3Test.getId(), false);
            moderableTestDAO.addTest(mathModelSemTwoT3T1STest);
            moderableTestDAO.addTest(mathModelSemTwoT3T2STest);
            //add test with equal name

            QuestionDAO questionDAO = factory.getQuestionDAO();
            Question questionOSQ1 = new OneOfSeveralQuestion("why so serious?",
                    mathModelSemTwoT3T1STest);
            questionOSQ1.setCreator(root);
            Question questionOSQ2 = new OneOfSeveralQuestion("100 - ? = 81",
                    mathModelSemTwoT3T1STest);
            questionOSQ2.setCreator(root);
            Question questionOSQ3 = new OneOfSeveralQuestion("Chto takoje Maple?",
                    mathModelSemTwoT3T1STest);
            questionOSQ3.setCreator(root);
            Question questionSMQ1 = new SeveralMultipleQuestion("Dlia chego nuzhna ruchka?",
                    mathModelSemTwoT3T1STest);
            questionSMQ1.setCreator(root);
            Question questionSMQ2 = new SeveralMultipleQuestion("C++ eto?",
                    mathModelSemTwoT3T1STest);
            questionSMQ2.setCreator(root);
            AnswerDAO answerDAO = factory.getAnswerDAO();
            questionDAO.addQuestion(questionOSQ1);
            Answer answer11 = new Answer("Ja Piero", "false", null, questionOSQ1.getId());
            Answer answer12 = new Answer("I'm BetMan", "true", null, questionOSQ1.getId());
            Answer answer13 = new Answer("Poterial 100$", "false", null, questionOSQ1.getId());
            Answer answer14 = new Answer("Снова Poterial 100$", "false", null, questionOSQ1.getId());
            answerDAO.addAnswer(answer11);
            answerDAO.addAnswer(answer12);
            answerDAO.addAnswer(answer13);
            answerDAO.addAnswer(answer14);
            questionDAO.addQuestion(questionOSQ2);
            Answer answer21 = new Answer("19", "true", null, questionOSQ2.getId());
            Answer answer22 = new Answer("???", "false", null, questionOSQ2.getId());
            Answer answer23 = new Answer("-?", "false", null, questionOSQ2.getId());
            Answer answer24 = new Answer("11", "false", null, questionOSQ2.getId());
            Answer answer25 = new Answer("81", "false", null, questionOSQ2.getId());
            answerDAO.addAnswer(answer21);
            answerDAO.addAnswer(answer22);
            answerDAO.addAnswer(answer23);
            answerDAO.addAnswer(answer24);
            answerDAO.addAnswer(answer25);
            questionDAO.addQuestion(questionOSQ3);
            Answer answer31 = new Answer("Eto OS ot Apple", "false", null, questionOSQ3.getId());
            Answer answer32 = new Answer("Eto CAD dlia matematicheskih rasschiotov", "true", null, questionOSQ3.getId());
            Answer answer33 = new Answer("Ja s GEFa", "false", null, questionOSQ3.getId());
            Answer answer34 = new Answer("Vsio neverno.", "false", null, questionOSQ3.getId());
            answerDAO.addAnswer(answer31);
            answerDAO.addAnswer(answer32);
            answerDAO.addAnswer(answer33);
            answerDAO.addAnswer(answer34);
            questionDAO.addQuestion(questionSMQ1);
            Answer answer41 = new Answer("Chtoby pisat\'", "true", null, questionSMQ1.getId());
            Answer answer42 = new Answer("Para k nozhke", "false", null, questionSMQ1.getId());
            Answer answer43 = new Answer("Otkryvat\' dver", "true", null, questionSMQ1.getId());
            Answer answer44 = new Answer("fasdgagasdg", "false", null, questionSMQ1.getId());
            answerDAO.addAnswer(answer41);
            answerDAO.addAnswer(answer42);
            answerDAO.addAnswer(answer43);
            answerDAO.addAnswer(answer44);
            questionDAO.addQuestion(questionSMQ2);
            Answer answer51 = new Answer("Jazyk programmirovanija", "true", null, questionSMQ2.getId());
            Answer answer52 = new Answer("Novyj vitamin", "false", null, questionSMQ2.getId());
            Answer answer53 = new Answer("NTo chto my izuchaem", "true", null, questionSMQ2.getId());
            Answer answer54 = new Answer("Eto tri simvola", "true", null, questionSMQ2.getId());
            answerDAO.addAnswer(answer51);
            answerDAO.addAnswer(answer52);
            answerDAO.addAnswer(answer53);
            answerDAO.addAnswer(answer54);



        } catch (DAOException e) {
            e.printStackTrace();
        }
    	
    	
    	
     /*
        try {
            TesterDAOFactory factory = TesterDAOFactory.getFactory();
            factory.getDatabaseData().truncateDatabase();

            GroupDAO groupDAO = factory.getGroupDAO();
            Group belsutGroup = new Group("BelSUT", 1953, 1);
            Group.setRootGroup(belsutGroup);
            groupDAO.addGroup(belsutGroup); //DEFAULT!
            Group etGroup = new Group("ET", 1978, belsutGroup.getId());
            Group mehiGroup = new Group("Mehi", 1978, belsutGroup.getId());
            Group pgsGroup = new Group("PGS", 1978, belsutGroup.getId());
            Group uppGroup = new Group("UPP", 1953, belsutGroup.getId());
            groupDAO.addGroup(etGroup);
            groupDAO.addGroup(mehiGroup);
            groupDAO.addGroup(pgsGroup);
            groupDAO.addGroup(uppGroup);
            Group em1Group = new Group("EM1", 2011, etGroup.getId());
            Group es1Group = new Group("ES1", 2011, etGroup.getId());
            Group et2Group = new Group("ET2", 2010, etGroup.getId());
            groupDAO.addGroup(em1Group);
            groupDAO.addGroup(es1Group);
            groupDAO.addGroup(et2Group);
            Group pa1Group = new Group("PA1", 2011, pgsGroup.getId());
            Group ps1Group = new Group("PS1", 2011, pgsGroup.getId());
            groupDAO.addGroup(pa1Group);
            groupDAO.addGroup(ps1Group);
            Group up2Group = new Group("UP2", 2010, uppGroup.getId());
            groupDAO.addGroup(up2Group);

            UserDAO userDAO = factory.getUserDAO();
            User root = new Admin(new User("root", "1111", "ROOT", "ROOT", belsutGroup));
            User korol = new User("korol", "1111", "Sasha", "Koroliov", etGroup);
            User olya = new User("olka", "1111", "Olya", "Ramka", etGroup);
            User andreika = new Admin(new User("andreika", "1111", "Andrei", "ZhalezKing", etGroup));
            User mishok = new User("mishok", "1111", "Misha", "Evmen", em1Group);
            User lioha = new User("lioha", "1111", "Aliosha", "Ja_ne_Vor!", em1Group);
            User uppshnica = new User("uppshnica", "1111", "SUppshnica", "Caca", uppGroup);
            User chelovek = new User("chelovek", "1111", "Vasya", "Kompot", et2Group);
            userDAO.addUser(root); //DEFAULT!
            userDAO.addUser(korol);
            userDAO.addUser(olya);
            userDAO.addUser(andreika);
            userDAO.addUser(mishok);
            userDAO.addUser(lioha);
            userDAO.addUser(uppshnica);
            userDAO.addUser(chelovek);

            ModerableTestDAO moderableTestDAO = factory.getModetableTestDAO();
            Test belsutTest = new Test("BelSUT", 0, 0, 0, 1);
            Test.setRootTest(belsutTest);
            moderableTestDAO.addTest(belsutTest);
            Test mathModelTest = new Test("Мат. Модели", 0, 0, 0, belsutTest.getId());
            Test historyTest = new Test("История", 0, 0, 0, belsutTest.getId());
            moderableTestDAO.addTest(mathModelTest);
            moderableTestDAO.addTest(historyTest);
            Test mathModelSemOneTest = new Test("Мат. Модели, 1 семестр", 0, 0, 0, mathModelTest.getId());
            Test mathModelSemTwoTest = new Test("Мат. Модели, 2 семестр", 0, 0, 0, mathModelTest.getId());
            moderableTestDAO.addTest(mathModelSemOneTest);
            moderableTestDAO.addTest(mathModelSemTwoTest);
            Test mathModelSemTwoT1Test = new Test("Мат. Модели, 2 семестр 1", 0, 0, 0,
                    mathModelSemTwoTest.getId());
            Test mathModelSemTwoT2Test = new Test("Мат. Модели, 2 семестр 2", 0, 0, 0,
                    mathModelSemTwoTest.getId());
            Test mathModelSemTwoT3Test = new Test("Мат. Модели, 2 семестр 3", 0, 0, 0,
                    mathModelSemTwoTest.getId());
            Test mathModelSemTwoT4Test = new Test("Мат. Модели, 2 семестр 4", 0, 0, 0,
                    mathModelSemTwoTest.getId());
            moderableTestDAO.addTest(mathModelSemTwoT1Test);
            moderableTestDAO.addTest(mathModelSemTwoT2Test);
            moderableTestDAO.addTest(mathModelSemTwoT3Test);
            moderableTestDAO.addTest(mathModelSemTwoT4Test);
            Test mathModelSemTwoT3T1STest = new Test("Мат. Модели, 2 семестр 3 1", 5 * 60, 2, 1,
                    mathModelSemTwoT3Test.getId());
            Test mathModelSemTwoT3T2STest = new Test("Мат. Модели, 2 семестр 3 2", 5 * 60, 2, 1,
                    mathModelSemTwoT3Test.getId());
            moderableTestDAO.addTest(mathModelSemTwoT3T1STest);
            moderableTestDAO.addTest(mathModelSemTwoT3T2STest);

            QuestionDAO questionDAO = factory.getQuestionDAO();
            Question questionOSQ1 = new OneOfSeveralQuestion("Why so serious?",
                    mathModelSemTwoT3T1STest);
            Question questionOSQ2 = new OneOfSeveralQuestion("100 - ? = 81",
                    mathModelSemTwoT3T1STest);
            Question questionOSQ3 = new OneOfSeveralQuestion("Что такое Maple?",
                    mathModelSemTwoT3T1STest);
            Question questionSMQ1 = new SeveralMultipleQuestion("Для чего нужна ручка?",
                    mathModelSemTwoT3T1STest);
            Question questionSMQ2 = new SeveralMultipleQuestion("C++ это?",
                    mathModelSemTwoT3T1STest);
            AnswerDAO answerDAO = factory.getAnswerDAO();
            questionDAO.addQuestion(questionOSQ1);
            Answer answer11 = new Answer("Я Пьеро", "false", null, questionOSQ1.getId());
            Answer answer12 = new Answer("Я beta-man", "true", null, questionOSQ1.getId());
            Answer answer13 = new Answer("Потерял 100$", "false", null, questionOSQ1.getId());
            Answer answer14 = new Answer("Снова потерял 100$", "false", null, questionOSQ1.getId());
            answerDAO.addAnswer(answer11);
            answerDAO.addAnswer(answer12);
            answerDAO.addAnswer(answer13);
            answerDAO.addAnswer(answer14);
            questionDAO.addQuestion(questionOSQ2);
            Answer answer21 = new Answer("19", "true", null, questionOSQ2.getId());
            Answer answer22 = new Answer("???", "false", null, questionOSQ2.getId());
            Answer answer23 = new Answer("-?", "false", null, questionOSQ2.getId());
            Answer answer24 = new Answer("11", "false", null, questionOSQ2.getId());
            Answer answer25 = new Answer("81", "false", null, questionOSQ2.getId());
            answerDAO.addAnswer(answer21);
            answerDAO.addAnswer(answer22);
            answerDAO.addAnswer(answer23);
            answerDAO.addAnswer(answer24);
            answerDAO.addAnswer(answer25);
            questionDAO.addQuestion(questionOSQ3);
            Answer answer31 = new Answer("Это ОС от Apple", "false", null, questionOSQ3.getId());
            Answer answer32 = new Answer("Это CAD для математических расчётов", "true", null, questionOSQ3.getId());
            Answer answer33 = new Answer("Я с ГЭФа", "false", null, questionOSQ3.getId());
            Answer answer34 = new Answer("Все предложенные варианты не верны.", "false", null, questionOSQ3.getId());
            answerDAO.addAnswer(answer31);
            answerDAO.addAnswer(answer32);
            answerDAO.addAnswer(answer33);
            answerDAO.addAnswer(answer34);
            questionDAO.addQuestion(questionSMQ1);
            Answer answer41 = new Answer("Что бы писать", "true", null, questionSMQ1.getId());
            Answer answer42 = new Answer("Пара к ножке", "false", null, questionSMQ1.getId());
            Answer answer43 = new Answer("Открыввать дверь", "true", null, questionSMQ1.getId());
            Answer answer44 = new Answer("'абра-кадабра'", "false", null, questionSMQ1.getId());
            answerDAO.addAnswer(answer41);
            answerDAO.addAnswer(answer42);
            answerDAO.addAnswer(answer43);
            answerDAO.addAnswer(answer44);
            questionDAO.addQuestion(questionSMQ2);
            Answer answer51 = new Answer("Язык программирования", "true", null, questionSMQ2.getId());
            Answer answer52 = new Answer("Новый витамин", "false", null, questionSMQ2.getId());
            Answer answer53 = new Answer("Это то, что мы знаем", "true", null, questionSMQ2.getId());
            Answer answer54 = new Answer("Это три символа", "true", null, questionSMQ2.getId());
            answerDAO.addAnswer(answer51);
            answerDAO.addAnswer(answer52);
            answerDAO.addAnswer(answer53);
            answerDAO.addAnswer(answer54);


        } catch (DAOException e) {
            e.printStackTrace();
        }
        */
    }

    public static void clear() {
        try {
            TesterDAOFactory.getFactory().getDatabaseData().truncateDatabase();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }
}
