package tester.transfers.users;

import java.io.Serializable;

public class Admin extends User implements Serializable {
//	public Admin( ){
//		super();
//	}
//	public Admin(int id){
//		super(id);
//	}
	public Admin(User user) {
		super(user.getId(), user.getLogin(), user.getPassword(),
				user.getFname(), user.getLname(), user.getGroup());
	}

	@Override
	public boolean isAdmin( ){
		return true;
	}
	@Override
	public boolean isModer( ){
		return Math.random() > 0.5;
	}
	
	@Override
	public String getRole( ){
		return "ADMIN";
	}
}
