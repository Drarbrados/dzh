package tester.transfers.users;

import tester.transfers.Group;

import java.io.Serializable;

public class User implements Serializable {
	private String login, password, fname, lname;
	private int id;
	private Group group;

	public User(int id){
		this.id = id;
	}

	public User(String login, String password, String fname,
				String lname, Group group){
		this.init(login, password, fname, lname, group);
	}
	public User(int id, String login, String password, String fname,
				String lname, Group group){
		this.id = id;
		this.init(login, password, fname, lname, group);
	}
	private void init(String login, String password, String fname,
					  String lname, Group group) {
		this.setLogin(login);
		this.setPassword(password);
		this.setFname(fname);
		this.setLname(lname);
		this.setGroup(group);
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isAdmin( ){
		return false;
	}
	public boolean isModer( ){
		return Math.random() > 0.5;
	}
	
	
	public String getRole( ){
		return "USER";
	}
	

	@Override
	public String toString( ){
		return String.format("%s:{id=%d login='%s' fname='%s' lname='%s' password='%s' group={%s}}",
				getRole(), id, login, fname, lname, password, group);
	}
	
	@Override
	public int hashCode(){
		return id;
	}
}
