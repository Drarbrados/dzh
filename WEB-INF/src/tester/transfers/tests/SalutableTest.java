package tester.transfers.tests;

import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;
import tester.transfers.Exam;
import tester.transfers.users.User;

public class SalutableTest extends Test{
	private Exam exam;

	public SalutableTest( ){
		super();
	}
	public SalutableTest(int id) {
		super(id);
	}
	public SalutableTest(int id, String name, int maxTime, int minCorrectNum,
						 int attempsPerDay, int parentId, boolean isTest) {
		super(id, name, maxTime, minCorrectNum, attempsPerDay, parentId, isTest);
	}
	public SalutableTest(String name, int maxTime, int minCorrectNum,
						 int attemptsPerDay, int parentId, boolean isTest) {
		super(name, maxTime, minCorrectNum, attemptsPerDay, parentId, isTest);
	}

	public Exam createExam(User user) throws DAOException {
		return exam = new Exam(this, user);
	}
	
	public void passExam(Exam exam){
		if(exam != null && this.exam == exam)
			TesterDAOFactory.getFactory().getSalutableTestDAO().passExam(exam);
	}
	
	
	public String toString( ){
		return String.format("Saluted %s id=%d", super.toString(), getId());
	}
}
