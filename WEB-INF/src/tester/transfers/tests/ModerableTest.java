package tester.transfers.tests;

import tester.transfers.tests.Test;

import tester.transfers.tests.Test;

public class ModerableTest extends Test {
    public ModerableTest( ){
        super();
    }
    public ModerableTest(int id) {
        super(id);
    }
    public ModerableTest(int id, String name, int maxTime, int minCorrectNum,
                int attempsPerDay, int parentId, boolean isTest) {
        super(id, name, maxTime, minCorrectNum, attempsPerDay, parentId, isTest);
    }

    public ModerableTest(String name, int maxTime, int minCorrectNum,
                int attemptsPerDay, int parentId, boolean isTest) {
        super(name, maxTime, minCorrectNum, attemptsPerDay, parentId, isTest);
    }
}
