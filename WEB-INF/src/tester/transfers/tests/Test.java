package tester.transfers.tests;

import tester.transfers.Group;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Test implements Comparable<Test>{
	private volatile String title;
	private volatile int maxTime, attemptsPerDay, parentId, minCorrectNum, questionsNumber;
	private volatile boolean isTest;
	private volatile int id;
	private static Test rootTest;
	private static Set<Integer> availableGroups = new HashSet<>();
///	private volatile boolean enable = false;// запретить изменения

	public int compareTo(Test test) {
		return -this.title.compareTo(test.getTitle());
	}
	public boolean equals(Test test) {
		return this.title.equals(test.getTitle());
	}

	public boolean addAvailableGroup(Group group) {
		return availableGroups.add(group.getId());
	}
	public boolean removeAvailableGroup(Group group) {
		return availableGroups.remove(group.getId());
	}
	public void clearAvailableGroupSet() {
		availableGroups.clear();
	}
	public boolean checkAvailableGroupSet(Group group) {
		return availableGroups.contains(group.getId());
	}


	@Deprecated
	public static void setRootTest(Test test) {
		Test.rootTest = test;
	}
	@Deprecated
	public static Test getRootTest() {
		return rootTest;
	}

	@Deprecated
	public Test() {

	}
	public Test(int id) {
		this.id = id;
	}
	public Test(int id, String name, int maxTime, int minCorrectNum,
				int attemptsPerDay, int parentId, boolean isTest) {
		this.id = id;
		this.init(name, maxTime, minCorrectNum, attemptsPerDay, parentId, isTest);
	}
	@Deprecated
	public Test(String name, int maxTime, int minCorrectNum,
				int attemptsPerDay, int parentId, boolean isTest) {
		this.init(name, maxTime, minCorrectNum, attemptsPerDay, parentId, isTest);
	}
	private void init(String name, int maxTime, int minCorrectNum,
				 int attemptsPerDay, int parentId, boolean isTest) {
		this.setTitle(name);
		this.setMaxTime(maxTime);
		this.setMinCorrectNum(minCorrectNum);
		this.setAttemptsPerDay(attemptsPerDay);
		this.setParentId(parentId);
		this.isTest = isTest;
	}


	public int getQuestionsNumber() {
		return questionsNumber;
	}
	public void setQuestionsNumber(int number) {
		this.questionsNumber = number;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getMaxTime() {
		return maxTime;
	}
	
	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}
	
	public int getAttemptsPerDay() {
		return attemptsPerDay;
	}

	public void setAttemptsPerDay(int attemptsPerDay) {
		this.attemptsPerDay = attemptsPerDay;
	}

	public int getParentId() {
		return parentId;
	}
	public int getId() {
		return id;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getMinCorrectNum() {
		return minCorrectNum;
	}

	public void setMinCorrectNum(int minCorrectNum) {
		this.minCorrectNum = minCorrectNum;
	}

	public boolean isTest() {
		return isTest;
	}

	public void setTest(boolean test) {
		isTest = test;
	}


	public String toString( ){
		return String.format("%s: title='%s' time=%d att/day=%d patentId=%d minN=%d"
				, (isTest ? "Test" : "Topic"),
				title, maxTime, attemptsPerDay, parentId, minCorrectNum);
	}
}
