package tester.transfers.tests;

import tester.transfers.users.User;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is composite for Test hierarchic.
 *
 * Created by DzianisH on 17.02.2015.
 */
public class TestTreeNode <T extends Test> implements Comparable<TestTreeNode<T>> {
    private final User SCOPE_OWNER;
    private T nodeValue;
    private TestTreeNode<T> parentNode;
    private List<TestTreeNode<T>> children = new ArrayList<TestTreeNode<T>>();
    private boolean isRoot, isAccessibleNode = true;

    public TestTreeNode(User scopeOwner){
        SCOPE_OWNER = scopeOwner;
    }
    public TestTreeNode(User scopeOwner, T value){
        this(scopeOwner);
        nodeValue = value;
    }

    /**
     * @return User, which it attached to this tree scope of tests.
     */
    public User getScopeOwner() {
        return SCOPE_OWNER;
    }

    /**
     * @return  parent node or null if this node is root.
     */
    public T getNodeValue() {
        return nodeValue;
    }

    /**
     * Set test in this node.
     * @param nodeValue
     */
    public void setNodeValue(T nodeValue) {
        this.nodeValue = nodeValue;
    }

    /**
     * By default is <tt>true</tt>;
     * @return Is owner users has access right to this node.
     */
    public boolean isNodeAccessible() {
        return isAccessibleNode;
    }

    public void setNodeAccessible(boolean isAccessibleNode) {
        this.isAccessibleNode = isAccessibleNode;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

    /**
     *
     * @return  parent or null, if it is a root of tree.
     */
    public TestTreeNode<T> getParentNode() {
        return isRoot() ? this : parentNode;
    }

    public void setParentNode(TestTreeNode<T> parentNode) {
        this.parentNode = parentNode;
    }

    /**
     * @param node
     * @return <tt>true</tt> if this collection changed as a result of the
     *         call
     */
    public boolean addChildNode(TestTreeNode<T> node){
        return children.add(node);
    }
    /**
     * @param node
     * @return <tt>true</tt> if this collection changed as a result of the
     *         call
     */
    public boolean removeChildNode(TestTreeNode<T> node) {
        return children.remove(node);
    }

    /**
     *
     * @param index number of child node
     * @return removed node or null
     */
    public TestTreeNode<T> removeChildNode(int index) throws IndexOutOfBoundsException{
        return children.remove(index);
    }

    public List<TestTreeNode<T>> getChildrenNodes( ){
        return children;
    }

    public int getChildrenNodesNumber( ){
        return children.size();
    }

    /**
     * @param index
     * @return
     * @throws IndexOutOfBoundsException   это RuntimeException
     */
    public TestTreeNode<T> getChildNode(int index) throws IndexOutOfBoundsException {
        return children.get(index);
    }

    /**
     * I want be optimized..
     *
     * @param id of requested node
     * @return node or null.
     */
    public TestTreeNode<T> subTreeSearch(int id) {
        //TODO: optimize search in test tree.
        
        if(getNodeValue().getId() == id)
            return this;
        TestTreeNode<T> requestedNode = null;
        for(TestTreeNode<T> node : children){
            requestedNode = node.subTreeSearch(id);

            if(requestedNode != null)
                break;
        }

        return requestedNode;
    }

    @Override
    public int compareTo(TestTreeNode<T> other) {
        int thisId = getNodeValue() != null ? getNodeValue().getId() : 0;
        int otherId = other.getNodeValue() != null ? other.getNodeValue().getId() : 0;

        return thisId - otherId;
    }

    @Override
    public String toString( ){
        if(!isRoot()) {
            return String.format("Node: {Value:{%s}}, Parent: {%s}", nodeValue, getParentNode());
        }
        return String.format("Node: {Value:{%s}}", nodeValue);
    }

    /**
     * For debug
     * @return
     */
    public String subTreeToString( ){
        return "\n'R' means root\n'-' means not accessible\n'+' means accessible.\n" +
                subTreeToStringBuilder(0).toString();
    }

    private StringBuilder subTreeToStringBuilder(int deep){
        StringBuilder sb = new StringBuilder(children.size()*10);
      //  sb.append('{');

        if(isRoot())
            sb.append('R');
        else
            appendTabs(sb, deep);
        sb.append(isAccessibleNode ? '+' : '-');
        sb.append(nodeValue.getId());
        sb.append(":{ ");
        sb.append(getNodeValue().getTitle());
        sb.append('\n');

        if(children.size() == 0){
            sb.setLength(sb.length() - 1);
            sb.append(" }");
            return sb;
        }

        for(TestTreeNode<T> child : children){
            sb.append(child.subTreeToStringBuilder(deep + 1));
            sb.append(",\n");
        }
        sb.setLength(sb.length()-2);
        sb.append('\n');
        appendTabs(sb, deep);
        sb.append('}');
        return sb;
    }

    private void appendTabs(StringBuilder sb, int numb) {
        sb.append(' ');
        while (--numb >= 0) {
            sb.append(numb < 1 ? numb == 0 ? "|_ " : "|__" : "___");
        }
    }
}
