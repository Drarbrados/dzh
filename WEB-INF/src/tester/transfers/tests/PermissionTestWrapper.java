package tester.transfers.tests;

/**
 * Created by DzianisH on 28.03.2015.
 */
public class PermissionTestWrapper{
    private Test test;
    private boolean accessible;

    public PermissionTestWrapper(Test test, boolean isAccessible) {
        this.test = test;
        accessible = isAccessible;
    }
    public PermissionTestWrapper(Test test) {
        this.test = test;
    }


    public String getTitle() {
        return test.getTitle();
    }

    public void setTitle(String title) {
        test.setTitle(title);
    }

    public int getMaxTime() {
        return test.getMaxTime();
    }

    public void setMaxTime(int maxTime) {
        test.setMaxTime(maxTime);
    }

    public int getAttempsPerDay() {
        return test.getAttemptsPerDay();
    }

    public void setAttempsPerDay(int attempsPerDay) {
        test.setAttemptsPerDay(attempsPerDay);
    }

    public int getParentId() {
        return test.getParentId();
    }
    public int getId() {
        return test.getId();
    }

    public void setParentId(int parentId) {
        test.setParentId(parentId);
    }
    public void setId(int id) {
        test.setId(id);
    }

    public int getMinCorrectNum() {
        return test.getMinCorrectNum();
    }

    public void setMinCorrectNum(int minCorrectNum) {
        test.setMinCorrectNum(minCorrectNum);
    }

    public boolean isTest() {
        return test.isTest();
    }

    public void setTest(boolean test) {
        this.test.setTest(test);
    }


    public String toString( ){
        return test.toString();
    }

    public boolean isAccessible() {
        return accessible;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }
}
