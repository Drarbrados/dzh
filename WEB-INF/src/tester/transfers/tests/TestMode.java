package tester.transfers.tests;

public class TestMode {

    public static final TestMode TEST_MODE = new TestMode("TEST");
    public static final TestMode TRAINING_MODE = new TestMode("TRAINING");
    private String mode;

    private TestMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }
    public TestMode getTestMode() {
        return TEST_MODE;
    }
    public TestMode getTrainingMode() {
        return TRAINING_MODE;
    }
    public boolean equals(Object testMode) {
        return mode.equals(((TestMode)testMode).getMode());
    }
}
