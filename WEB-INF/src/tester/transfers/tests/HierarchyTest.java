package tester.transfers.tests;

import tester.transfers.tests.SalutableTest;

/**
 * Created by andrei on 13.2.15.
 */
public class HierarchyTest {
    @Deprecated // TODO: Нужно разделять контроллер и представление
    private final String TEST = "тест";
    @Deprecated
    private final String TOPIC = "раздел";

    private int id;
    private int parentId;
    private String title;
    private int offset;
    private int questionCount;
    private String type;
    private boolean accessible;

    public HierarchyTest(Test test, int offset, int questionCount, boolean isAccessibleForUser) {
        this.id = test.getId();
        this.parentId = test.getParentId();
        this.title = test.getTitle();
        this.offset = offset;
        this.questionCount = questionCount;
        if(test.isTest()) {
            type = TEST;
        } else {
            type = TOPIC;
        }
        accessible = isAccessibleForUser;
    }

    public int getId() {
        return id;
    }
    public int getParentId() {
        return parentId;
    }
    public String getTitle() {
        return title;
    }
    public int getOffset() {
        return offset;
    }
    public int getQuestionCount() {
        return questionCount;
    }
    public String getType() {
        return type;
    }

    public boolean isAccessible() {
        return accessible;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }
}