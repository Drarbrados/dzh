package tester.transfers.algorithms;

import java.util.*;

import static java.lang.Math.exp;

import tester.service.Logger;
import tester.transfers.algorithms.service.MonteCarloSimulator;
import tester.transfers.algorithms.service.RandomEvent;
import tester.transfers.questions.Question;
/**
 *
 * @author DzianisH
 *
 */
public class GaussByDifficultyAlgorithm extends AbstractAlgorithmStrategy{
	private static final Logger LOG = Logger.getLogger(GaussByDifficultyAlgorithm.class);

	private static final String MU_FIELD = "mu";
	private static final String SIGMA_FIELD = "sigma";
	private static final String RESULT_QUESTIONS_NUMBER_FIELD =
			"sample_size";
	
	private double mu, sigma;
	private int resNumb;
	
	@Override
	public void init(Map<String, String> dafaults)
			throws IllegalArgumentException {
		String val = dafaults.get(RESULT_QUESTIONS_NUMBER_FIELD);
		if(val == null){
			throw new IllegalArgumentException("Can't find all paramenters for init.");
		}
		resNumb = Integer.parseInt(val);
		
		val = dafaults.get(MU_FIELD);
		if(val == null){
			throw new IllegalArgumentException("Can't find all paramenters for init.");
		}
		mu = Double.parseDouble(val);
		
		val = dafaults.get(SIGMA_FIELD);
		if(val == null){
			throw new IllegalArgumentException("Can't find all paramenters for init.");
		}
		sigma = Double.parseDouble(val);
	}

	@Override
	public List<Question> doAlgorithm(List<Question> questions) {
		throwIfBadInitValues(questions);

		LinkedList<Question> list = new LinkedList<Question>();
		
		ArrayList<RandomEvent<?>> hypoth =
				new ArrayList<RandomEvent<?>>();
		LinkedList<RandomEvent<?>> result =
				new LinkedList<RandomEvent<?>>();
		
		RandomEvent<Question> event;
		double $D2 = 2.0*sigma*sigma;
		double x;
		for(Question question : questions){
			event = new RandomEvent<Question>(question);
			x = question.getDifficulty();
			// real Gauss is divide by sigma*sqrt(2*PI), but it not necessarily,
			// because of normalization in Monte-Carlo
			event.setProbability(  exp(-(x-mu)*(x-mu)/$D2)  );
			hypoth.add(event);
		}
		
		MonteCarloSimulator.simulate(hypoth, result, resNumb);
		
		for(RandomEvent<?> winEvent : result){
			list.add((Question)winEvent.getEvent());
		}
		
		return list;
	}

	private void throwIfBadInitValues(List<Question> questions){
		if(resNumb > questions.size()){
			String msg = LOG.error(
					"Noooooooooo. Can't get %d questions, because have only %d.",
					resNumb, questions.size()
			);
			throw new RuntimeException(msg);
		}
	}

	@Override
	public List<String> getAllParameterNames() {
		LinkedList<String> params = new LinkedList<String>();
		if(locale.equals("RUS")) {
			params.add("Количество Вопросов");
			params.add("Мат. Ожидание");
			params.add("Дисперсия");
		} else {
			params.add(RESULT_QUESTIONS_NUMBER_FIELD);
			params.add(MU_FIELD);
			params.add(SIGMA_FIELD);
		}
		return params;
	}

	@Override
	public String getName() {
		if(locale.equals("RUS")) {
			return "Выбор по Гауссу";
		} else {
			return "Gauss";
		}
	}

	public String toString() {
		return "DGauss";
	}


	@Override
	public String getDescription() {
		return "Гаусс !";
	}

}
