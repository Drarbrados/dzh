package tester.transfers.algorithms;

public abstract class AbstractAlgorithmStrategy implements Algorithm {

	protected static String locale = "ENG";

	public static void setLocale(String locale) {
		AbstractAlgorithmStrategy.locale = locale;
	}

	@Override
	public void reset() {
	}

	@Override
	public void destroy() {
	}


}
