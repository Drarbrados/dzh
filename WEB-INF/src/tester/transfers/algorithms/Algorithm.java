package tester.transfers.algorithms;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import tester.transfers.questions.Question;

public interface Algorithm{
	public void init(Map<String, String> defaults) throws IllegalArgumentException;
	public List<Question> doAlgorithm(List<Question> questions);
	/**
	 * Reset all defaults to start point but not destroy.
	 * This class will reused after new initing.
	 */
	public void reset( );
	/**
	 * Reset all defaults and destroy this class.
	 */
	public void destroy( );

	public List<String> getAllParameterNames();
	public String getName();
	
	@Deprecated
	public String getDescription( );
}
