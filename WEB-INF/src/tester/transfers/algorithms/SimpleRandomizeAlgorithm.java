package tester.transfers.algorithms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import tester.service.Logger;
import tester.transfers.questions.Question;

/**
 * 
 * @author DzianisH
 *
 */

public class SimpleRandomizeAlgorithm extends AbstractAlgorithmStrategy {
	private static final Logger LOG = Logger.getLogger(SimpleRandomizeAlgorithm.class);
	private static final String RESULT_QUESTIONS_NUMBER =
			"sample_size";
	
	private int resNumb;
	private static Random rand = new Random();
	
	@Override
	public void init(Map<String, String> dafaults) throws NumberFormatException, IllegalArgumentException{
		String val = dafaults.get(RESULT_QUESTIONS_NUMBER);
		if(val == null){
			throw new IllegalArgumentException("Can't find all paramenters for init.");
		}
		resNumb = Integer.parseInt(val);
	}

	@Override
	public List<Question> doAlgorithm(List<Question> questions) {
		throwIfBadInitValues(questions);

		LinkedList<Question> list = new LinkedList<Question>();
		questions = new ArrayList<Question>(questions);
		while(list.size() < resNumb) {
			list.add(questions.remove(rand.nextInt(questions.size())));
		}
		return list;
	}

	private void throwIfBadInitValues(List<Question> questions){
		if(resNumb > questions.size()){
			String msg = LOG.error(
					"Noooooooooo. Can't get %d questions, because have only %d.",
					resNumb, questions.size()
			);
			throw new RuntimeException(msg);
		}
	}

	@Override
	public List<String> getAllParameterNames() {
		LinkedList<String> params = new LinkedList<String>();
		if(locale.equals("RUS")) {
			params.add("Количество Вопросов");
		} else {
			params.add(RESULT_QUESTIONS_NUMBER);
		}
		return params;
	}

	@Override
	public String getName() {
		if(locale.equals("RUS")) {
			return "Случайный Выбор";
		} else {
			return "Random Select";
		}
	}

	@Override
	public void destroy( ){
	}

	public String toString() {
		return "SRandom";
	}

	@Override
	public String getDescription() {
		return "Простой рандомайз.";
	}

}
