package tester.transfers.algorithms.service;

import java.awt.Toolkit;
import java.util.List;

import tester.service.Logger;

/**
 * @author DzianisH
 * 14.02.2015
 */
public abstract class MonteCarloSimulator {
	private static final Logger LOG = Logger.getLogger(MonteCarloSimulator.class);

	public static void simulate(List<RandomEvent<?>> hypothesis,
			List<RandomEvent<?>> result, int trails){
		
		if(trails >= hypothesis.size() || hypothesis.size() == 0){// smthg unexplained, like ufo
			RandomEvent<?> tempEvent;
			while((trails = hypothesis.size()) != 0){// move all from H to result
				tempEvent = hypothesis.remove(trails - 1);
				result.add(tempEvent);
			}
			return;
		}
		
		
		double sumP = getSummaryProbability(hypothesis);
		double randomValue, threshold;
		RandomEvent<?> winner;
		int i, len;
		
		while(--trails > -1){// repeat trails times
			sumP = normalize(hypothesis, sumP);
			System.out.println("\n   hipothesis:");
			for(RandomEvent<?> ev : hypothesis){
				System.out.println(ev);
			}
			
			threshold = 0.0; i = -1; len = hypothesis.size();
			winner = hypothesis.get(0);
			
			randomValue = Math.random();// do random
			System.out.println("\nnow randomValue:  " + randomValue);
			
			while(threshold < randomValue && ++i < len){// '(++i) < len'  means '(i+1) < hypothesis.size()'
				winner = hypothesis.get(i);
				threshold += winner.getProbability();
			}

			System.out.println("now i:  " + i);
			if(i == -1)// -1  and  size()   values is possible
				i = 0;
			else if(i == len)
				i = len - 1;
			System.out.println("now i:  " + i);
			
			if(hypothesis.remove(i) != winner){
				LOG.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! %s is not on %d",
						winner, i);
				System.out.println("!!!!!!!!!!!");
				
				Toolkit.getDefaultToolkit().beep();// :)
			}
			sumP -= winner.getProbability();
			System.out.println("Winner is:  " + winner);
			System.out.println("now sumP:  " + sumP);
			result.add(winner);
		}
	}
	
	private static double getSummaryProbability(List<RandomEvent<?>> hypothesis){
		double sumP = 0.0, tmp;
		for(RandomEvent<?> event : hypothesis){
			tmp = event.getProbability();
			if(Double.isNaN(tmp) || Double.isInfinite(tmp))
				tmp = 0.0;
			sumP += tmp;
		}
		return sumP;
	}
	
	// if summary P equals zero  it set  P = 1/hypothesis.size() for every event
	private static double normalize(List<RandomEvent<?>> hypothesis, double normBy){
		double tmp, summary = 0.0;
		if(Math.abs(normBy) < 1e-9){
			normBy = 1.0 / hypothesis.size();
			for(RandomEvent<?> event : hypothesis){
				summary += normBy;
				event.setProbability(normBy);
			}
		}
		else
			for(RandomEvent<?> event : hypothesis){
				tmp = event.getProbability() / normBy;
				summary += tmp;
				event.setProbability(tmp);
			}
		return summary;
	}
}
