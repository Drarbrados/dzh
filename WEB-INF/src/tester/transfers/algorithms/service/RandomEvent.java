package tester.transfers.algorithms.service;

/**
 * 
 * @author DzianisH
 * 14.02.2015
 * @param <E> is type of event, that probably may occur.
 */
public class RandomEvent<E> {
	private double probability;
	private E event;
	
	public RandomEvent( ){
	}
	
	public RandomEvent(E event){
		this.event = event;
	}
	
	public RandomEvent(E event, double chance){
		this.event = event;
		probability = chance;
	}
	
	public double getProbability() {
		return probability;
	}
	public void setProbability(double probability) {
		this.probability = probability;
	}
	public E getEvent() {
		return event;
	}
	public void setEvent(E event) {
		this.event = event;
	}
	
	
	public String toString( ){
		return String.format("{Event: '%s'; P: %f;}", event, probability);
	}
}
