package tester.transfers;

public class Answer implements Comparable<Answer>{
	private int id;
	private String text;
	private String comb = "false";
	private String pictureId;
	private int questionId;
	private int pictureSize;

	private long trueNumber, falseNamber;

	public void setPictureSize(int size) {
		this.pictureSize = size;
	}
	public int getPictureSize() {
		return pictureSize;
	}

	public Answer( ) {
	}
	
	@Deprecated
	public Answer(int id) {
		this.id = id;
	}

	public Answer(String text) {
		this.text = text;
	}
	public Answer(String text, String comb) {
		this.text = text;
		this.comb = comb;
	}

	public Answer(int id, String text, String comb, String pictureId, int questionId) {
		this.id = id;
		this.init(text, comb, pictureId, questionId);
	}
	public Answer(String text, String comb, String pictureId, int questionId) {
		this.init(text, comb, pictureId, questionId);
	}
	private void init(String text, String comb, String pictureId, int questionId) {
		this.setText(text);
		this.setComb(comb);
		this.setPictureId(pictureId);
		this.setQuestionId(questionId);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}


	public int getId( ){
		return id;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public void setComb(String comb) {
		this.comb = comb;
	}
	
	public String getText() {
		return text;
	}
	
	public String getComb() {
		return comb;
	}
	
	@Override
	public int compareTo(Answer other) {
		return this.id - other.id;
	}
	

	@Override
	public String toString( ){
		return String.format("id=%d text='%s' comb='%s'", id, text, comb);
	}
	
	public long getTrueNumber() {
		return trueNumber;
	}

	public void setTrueNumber(long trueNumber) {
		this.trueNumber = trueNumber;
	}

	public long getFalseNamber() {
		return falseNamber;
	}

	public void setFalseNamber(long falseNamber) {
		this.falseNamber = falseNamber;
	}
	
	/**
	 * 1 => Super hard.
	 * 0 => Super easy. 
	 * @return [0; 1] value.
	 */
	public double getDifficulty( ){
		return falseNamber/((double)trueNumber + falseNamber);
	}

	public String getPictureId() {
		return pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}
}
