package tester.transfers;

public class QuestionsGroup {
	private int testId, id, algorithmId;

	public QuestionsGroup(){
	}
	
	public QuestionsGroup(int id, int algorithmId, int test){
		testId = test;
		this.algorithmId = algorithmId;
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTestId() {
		return testId;
	}
	public void setTestId(int testId) {
		this.testId = testId;
	}
	public int getAlgorithmId() {
		return algorithmId;
	}
	public void setAlgorithmId(int algorithmId) {
		this.algorithmId = algorithmId;
	}
}
