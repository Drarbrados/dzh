package tester.transfers.stats.objects;

import tester.transfers.users.User;

public class UserStat {

    private int userId;
    private GroupStat groupStat;
    private String firstName;
    private String lastName;

    public UserStat(User user) {
        //check null
        this.groupStat = new GroupStat(user.getGroup());
        this.firstName = user.getFname();
        this.lastName = user.getLname();
        this.userId = user.getId();
    }

    public GroupStat getGroupStat() {
        return groupStat;
    }
    public int getUserId() {
        return userId;
    }
    public String getUsername() {
        return String.format("%s %s", this.firstName, this.lastName);
    }
}
