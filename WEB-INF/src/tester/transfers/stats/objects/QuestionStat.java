package tester.transfers.stats.objects;

import tester.transfers.Answer;
import tester.transfers.questions.Question;

import java.util.ArrayList;
import java.util.List;

public class QuestionStat {

    private int questionId;
    private String questionText;
    private List<AnswerStat> answers = new ArrayList<AnswerStat>();
    private boolean result;
    private List<Boolean> answerChecks = new ArrayList<Boolean>();

    public QuestionStat(Question question, boolean result) {
        //check null
        this.result = result;
        this.questionText = question.getText();
        this.questionId = question.getId();
        int i = 0;
        for(Answer answer : question.getAvailableAnswers()) {
            answers.add(new AnswerStat(answer, answerChecks.get(i)));
            i++;
        }
    }
}
