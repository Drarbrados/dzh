package tester.transfers.stats.objects;

import tester.transfers.Answer;

public class AnswerStat {

    private int answerId;
    private String answerText;
    private boolean result;

    public AnswerStat(Answer answer, boolean result) {
        //check null
        this.answerId = answer.getId();
        this.answerText = answer.getComb();
        this.result = result;
    }

    public int getAnswerId() {
        return answerId;
    }
    public String getAnswerText() {
        return answerText;
    }
    public boolean isAnswerCorrect() {
        return result;
    }

}
