package tester.transfers.stats.objects;

import tester.transfers.tests.Test;
import tester.transfers.tests.TestMode;

public class TestStat {

    private int testId;
    private String testName;
    private int mark;
    private TestMode testMode;
    private int parentId;

    public TestStat(Test test, int mark, TestMode mode) {
        //check null
        this.mark = mark;
        this.testId = test.getId();
        this.testName = test.getTitle();
        this.testMode = mode;
        this.parentId = test.getParentId();
    }
    public TestStat(String testTitle, int testId, int parentId, int mark, TestMode mode) {
        //check null
        this.mark = mark;
        this.testId = testId;
        this.testName = testTitle;
        this.testMode = mode;
        this.parentId = parentId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setTestName(String newName) {
        this.testName = newName;
    }

    public int getTestId() {
        return testId;
    }
    public String getTestName() {
        return testName;
    }
    public int getMark() {
        return mark;
    }
    public TestMode getTestMode() {
        return testMode;
    }
}
