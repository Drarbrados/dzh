package tester.transfers.stats.objects;

import tester.transfers.Group;

public class GroupStat {

    private int groupId;
    private String groupName;

    public GroupStat(Group group) {
        this.groupId = group.getId();
        this.groupName = group.getName();
    }

    public String getGroupName() {
        return groupName;
    }
    public int getGroupId() {
        return groupId;
    }
}
