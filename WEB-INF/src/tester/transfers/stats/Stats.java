package tester.transfers.stats;

import tester.transfers.stats.objects.GroupStat;
import tester.transfers.stats.objects.UserStat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Stats {

    private GroupStat groupStat;
    private UserStat userStat;
    private Date date;

    public Stats(UserStat user, Date date) {
        //check null
        this.groupStat = user.getGroupStat();
        this.userStat = user;
        this.setDate(date);
    }
    private void setDate(Date date) {
        this.date = date;
    }

    public GroupStat getGroupStat() {
        return groupStat;
    }
    public UserStat getUserStat() {
        return userStat;
    }
    public String  getDate() {
        return new SimpleDateFormat("yyyy-MM-dd [HH:mm]").format(date);
    }
}
