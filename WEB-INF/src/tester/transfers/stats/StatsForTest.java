package tester.transfers.stats;

import tester.transfers.stats.objects.GroupStat;
import tester.transfers.stats.objects.TestStat;
import tester.transfers.stats.objects.UserStat;

import java.text.SimpleDateFormat;
import java.util.Date;


public class StatsForTest {

    private Stats stats;
    private TestStat testStat;

    public StatsForTest(Stats stats, TestStat testStat) {
        //check null
        this.setStats(stats);
        this.testStat = testStat;
    }
    private void setStats(Stats stats) {
        this.stats = stats;
    }
    public void setTestStat(TestStat test) {
        this.testStat = test;
    }

    public GroupStat getGroupStat() {
        return stats.getGroupStat();
    }
    public UserStat getUserStat() {
        return stats.getUserStat();
    }
    public String getDate() {
        return stats.getDate();
    }
    public TestStat getTestStat() {
        return testStat;
    }
}
