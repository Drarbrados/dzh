package tester.transfers.stats;

import tester.transfers.stats.objects.GroupStat;
import tester.transfers.stats.objects.QuestionStat;
import tester.transfers.stats.objects.TestStat;
import tester.transfers.stats.objects.UserStat;

import java.util.ArrayList;
import java.util.List;

public class StatsForQuestion {

    private List<QuestionStat> questionsStat = new ArrayList<QuestionStat>();
    private StatsForTest statsTest;

    public StatsForQuestion(StatsForTest statsTest, List<QuestionStat> questionsStat) {
        //check null
        this.statsTest = statsTest;
        this.questionsStat = questionsStat;
    }

    public GroupStat getGroupStat() {
        return statsTest.getGroupStat();
    }
    public UserStat getUserStat() {
        return statsTest.getUserStat();
    }
    public String getDate() {
        return statsTest.getDate();
    }
    public TestStat getTestStat() {
        return statsTest.getTestStat();
    }
    public List<QuestionStat> getQuestionsStat() {
        return questionsStat;
    }
}
