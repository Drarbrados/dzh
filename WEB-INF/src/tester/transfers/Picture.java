package tester.transfers;

import java.io.File;

import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.ElementIsMissingException;

public class Picture {
	private String id, name;
//	private int time
	
	public Picture(){
	}
	
	public Picture(String id, String name){
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/* такие методы можно наделать в половите трансферов! удобно. */
	public File getLikeFile( ) throws ElementIsMissingException{
		return TesterDAOFactory.getFactory().getPictureDAO().getPictureFile(id);
	}
}
