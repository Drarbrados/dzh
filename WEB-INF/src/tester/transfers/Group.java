package tester.transfers;

public class Group {
	private int id;
	private String name;
	private int year, gParentId;
	private static Group rootGroup;
	private boolean isVisible = false;

	public void setVisible() {
		isVisible = true;
	}
	public void setInvisible() {
		isVisible = false;
	}
	public boolean isVisible() {
		return isVisible;
	}

	public Group(int id) {
		this.id = id;
	}

	public Group(String groupName, int year, int gParentId){
		this.init(groupName, year, gParentId);
	}
	public Group(int id, String groupName, int year, int gParentId){
		this.id = id;
		this.init(groupName, year, gParentId);
	}
	private void init(String groupName, int year, int gParentId) {
		this.setName(groupName);
		this.setYear(year);
		this.setParentGroupId(gParentId);
	}

	public static void setRootGroup(Group group) {
		Group.rootGroup = group;
	}
	public static Group getRootGroup() {
		return rootGroup;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getParentGroupId() {
		return gParentId;
	}

	public void setParentGroupId(int gParentId) {
		this.gParentId = gParentId;
	}
	
	public String toString( ){
		return String.format("id=%d name='%s' year=%d, parandId=%d"
				, id, name, year, gParentId);
	}

	public boolean equals(Group group) {
		return this.id == group.getId();
	}
	@Override
	public int hashCode() {
		return this.id;
	}

}
