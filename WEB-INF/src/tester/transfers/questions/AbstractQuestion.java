package tester.transfers.questions;

import java.util.ArrayList;
import java.util.Collections;

import tester.transfers.Answer;
import tester.transfers.users.User;
import tester.transfers.tests.Test;


public abstract class AbstractQuestion implements Question{
	private int id, prize = 1;
	private long trueNumber, falseNumber;
	private volatile String text;
	private String pictureId;
	private int pictureSize;
	//////// make me concurrent!!!!!!!!!!!!!
	private ArrayList<Answer> availableAnswers = new ArrayList<Answer>();
	private ArrayList<Answer> userAnswers = new ArrayList<Answer>();
	private Test test;
	private Customization customization = new Customization();
	private String modifyDate, createDate;
	private User modifier, creator;
	public static final String ONE_OF_SEVERAL_Q_TYPE = "OneOfSeveral";
	public static final String MULTIPLE_Q_TYPE = "Multiple";
	public static final String STRING_Q_TYPE = "String";
	public static final String CORRELATE_Q_TYPE = "Correlate";

	public String getStringQType() {
		int type = this.getType();
		switch (type) {
			case 1: return ONE_OF_SEVERAL_Q_TYPE;
			case 2: return MULTIPLE_Q_TYPE;
			case 3: return STRING_Q_TYPE;
			case 4: return CORRELATE_Q_TYPE;
			default: return null;
		}
	}

	public void setPictureSize(int size) {
		this.pictureSize = size;
	}
	public int getPictureSize() {
		return pictureSize;
	}

	public void setModifyDate(String date) {
		this.modifyDate = date;
	}
	public String getModifyDate() {
		return modifyDate;
	}

	public void setCreateDate(String date) {
		this.createDate = date;
	}
	public String getCreateDate() {
		return createDate;
	}

	public void setCreator(User user) {
		this.creator = user;
	}
	public void setModifier(User user) {
		this.modifier = user;
	}
	public User getCreator() {
		return creator;
	}
	public User getModifier() {
		return modifier;
	}

	public Customization getCustomization() {
		return customization;
	}
	public void setCustomization(Customization customization) {
		this.customization = customization;
	}

	public AbstractQuestion( ) {
	}
	
	
	public AbstractQuestion(int id, String text) {
		this.text = text;
		this.id = id;
	}
	public AbstractQuestion(int id, String text, int prize) {
		this(id, text);
		this.prize = prize;
	}

	public AbstractQuestion(int id, String text, Test test) {
		this.text = text;
		this.id = id;
		this.test = test;
	}
	public AbstractQuestion(int id, String text, Test test, int prize) {
		this(id, text, test);
		this.prize = prize;
	}
	public AbstractQuestion(String text, Test test) {
		this.text = text;
		this.test = test;
	}
	public AbstractQuestion(String text, Test test, int prize) {
		this(text, test);
		this.prize = prize;
	}

	@Override
	public void setTest(Test test) {
		this.test = test;
	}
	@Override
	public Test getTest() {
		return test;
	}
	@Override
	public abstract int getType();


	@Override
	public void setTrueNumber(long numb){
		trueNumber = numb;
	}
	@Override
	public long getTrueNumber( ){
		return trueNumber;
	}
	@Override
	public void setFalseNumber(long numb){
		falseNumber = numb;
	}
	@Override
	public long getFalseNumber(){
		return falseNumber;
	}
	
	
	@Override
	public int getId( ){
		return id;
	}
	@Override
	public void setId(int id){
		this.id = id;
	}
	@Override
	public String getText() {
		return text;
	}
	@Override
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public ArrayList<Answer> getAvailableAnswers() {
		return new ArrayList<Answer>(availableAnswers);
	}
	@Override
	public void setAvailableAnswers(ArrayList<Answer> answers) {
		availableAnswers = answers;
	}
	@Override
	public boolean isContainsInAvailable(Answer answ){
		return availableAnswers.contains(answ);
	}

	@Override
	public boolean addAvailableAnswer(Answer answ) {
		return availableAnswers.add(answ);
	}


	@Override
	public boolean removeAvailableAnswer(Answer answ) {
		return availableAnswers.remove(answ);
	}

	@Override
	public void removeAllAvailableAnswers() {
		availableAnswers.clear();
	}

	@Override
	public ArrayList<Answer> getUserAnswers(){
		return userAnswers;
	}
	
	@Override
	public boolean addUserAnswer(Answer answ) {
		return userAnswers.add(answ);
	}


	@Override
	public boolean removeUserAnswer(Answer answ) {
		return userAnswers.remove(answ);
	}


	@Override
	public void setComplete() {
		Collections.shuffle(availableAnswers);
	}
	
	@Override
	public int compareTo(Question other) {
		return this.id - other.getId();
	}
	
	
	public String toString( ){
		StringBuilder sb = new StringBuilder("");
		for(Answer answer : availableAnswers){
			sb.append(answer.toString());
			sb.append(';');
		}
		if(sb.length() > 0) {
			sb.setCharAt(sb.length() - 1, '}');
		}

		return String.format("%s id=%d text='%s' trueNumber=%d falseNumber=%d diffic=%f Answers={%s",
				this.getClass().getSimpleName(), id, text, trueNumber, falseNumber, getDifficulty(), sb.toString());
	}

	@Override
	public String getPictureId() {
		return pictureId;
	}

	@Override
	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

	@Override
	public int getPrize( ){
		return prize;
	}
	@Override
	public void setPrize(int prize){
		this.prize = prize;
	}
	
	
	/**
	 * 1 => Super hard.
	 * 0 => Super easy. 
	 * @return [0; 1] value.
	 */
	@Override
	public double getDifficulty( ){
		if(trueNumber == falseNumber)
			return 0.5;
		return falseNumber/((double)trueNumber + falseNumber);
	}
	
}
