package tester.transfers.questions;

import tester.transfers.Answer;
import tester.transfers.tests.Test;

import java.util.ArrayList;

public class StringQuestion extends AbstractQuestion {

	{
		getCustomization().setAnswersInRow(1);
		getCustomization().setAnswerAreaSize(50);
		getCustomization().setAnswerAreaLeftOffset(25);
	}

	@Deprecated
	public StringQuestion() {
	}

	public int getType() {
		return 3;
	}

	@Deprecated
	public StringQuestion(int id, String text) {
		super(id, text);
	}

	public StringQuestion(int id, String text, Test test) {
		super(id, text, test);
	}
	public StringQuestion(String text, Test test) {
		super(text, test);
	}

	@Override
	public boolean isAnswersCorrect(){
		ArrayList<Answer> list = getUserAnswers();
		String userAnswer = list.get(0).getText().trim();
		if(list.size() != 1) {
			return false;
		}
		for(Answer answer : getAvailableAnswers()) {
			if(userAnswer.equals(answer.getText().trim())) {
				return true;
			}
		}
		return false;
	}

}
