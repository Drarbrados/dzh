package tester.transfers.questions;

import java.util.ArrayList;

import tester.transfers.Answer;
import tester.transfers.tests.Test;

public class OneOfSeveralQuestion extends AbstractQuestion {

	{
		getCustomization().setAnswersInRow(1);
		getCustomization().setAnswerAreaSize(100);
		getCustomization().setAnswerAreaLeftOffset(0);
	}

	@Deprecated
	public OneOfSeveralQuestion( ) {
	}

	public int getType() {
		return 1;
	}

	@Deprecated
	public OneOfSeveralQuestion(int id, String text) {
		super(id, text);
	}

	public OneOfSeveralQuestion(int id, String text, Test test) {
		super(id, text, test);
	}
	public OneOfSeveralQuestion(String text, Test test) {
		super(text, test);
	}

	@Override
	public boolean isAnswersCorrect(){
		ArrayList<Answer> list = getUserAnswers();
		if(list.size() != 1)
			return false;
		Answer answ = list.get(0);

		return isContainsInAvailable(answ) && "true".equals(answ.getComb());
	}

}
