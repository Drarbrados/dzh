package tester.transfers.questions;

import java.util.ArrayList;

import tester.transfers.Answer;
import tester.transfers.users.User;
import tester.transfers.tests.Test;

public interface Question extends Comparable<Question> {
	public String getStringQType();
	public Customization getCustomization();
	public void setCustomization(Customization customization);
	public String getModifyDate();
	public void setModifyDate(String date);
	public String getCreateDate();
	public void setCreateDate(String date);
	public void setCreator(User user);
	public void setModifier(User user);
	public User getCreator();
	public User getModifier();
	public void setPictureSize(int size);
	public int getPictureSize();
	public int getId( );
	public void setId(int id);
	public String getText();
	public void setText(String text);
	public String getPictureId();
	public void setPictureId(String pictureId);
	public void setPrize(int prize);// prize - number of points for correct answer this question
	public int getPrize( );
	// 5 statistics
	public void setTrueNumber(long numb);
	public long getTrueNumber( ); //T
	public void setFalseNumber(long numb);
	public long getFalseNumber( );//F
	/** 
	 * 1 => Super hard. <br>
	 * 0 => Super easy. 
	 * 
	 * @return vale=[0; 1];  Difficulty = False/(True+False).
	 */
	public double getDifficulty();//Diff
	
	// this methods work with available answers in this question
	public ArrayList<Answer> getAvailableAnswers();
	public void setAvailableAnswers(ArrayList<Answer> answers);
	public boolean addAvailableAnswer(Answer answ);
	public boolean removeAvailableAnswer(Answer answ);
	public void removeAllAvailableAnswers();
	public boolean isContainsInAvailable(Answer answ);
	
	// this methods work with answers, selected by user
	public ArrayList<Answer> getUserAnswers();
	public boolean addUserAnswer(Answer answ);
	public boolean removeUserAnswer(Answer answ);
	
	
	public void setTest(Test test);
	public Test getTest();
	public int getType();
	

	/**
	 * if Object set like complete you can't call
	 * any set-methods except *UserAnswer(Answer answ).
	 * Completed methods will throw runtime ObjectCanNotBeChangedException.
	 */
	public void setComplete( );// сейчас в этом методе перемешивается коллекция доступных ответов
	
	/**
	 * Check user's answers!
	 * @return
	 */
	public boolean isAnswersCorrect( );
}
