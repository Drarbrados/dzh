package tester.transfers.questions;

import tester.transfers.Answer;
import tester.transfers.tests.Test;

import java.util.ArrayList;
import java.util.Collections;

public class CorrelateQuestion extends AbstractQuestion {

	{
		getCustomization().setAnswersInRow(2);
		getCustomization().setAnswerAreaSize(50);
		getCustomization().setAnswerAreaLeftOffset(25);
	}

	@Deprecated
	public CorrelateQuestion() {
	}
	@Deprecated
	public CorrelateQuestion(int id, String text) {
		super(id, text);
	}

	@Override
	public void setComplete() {
		ArrayList<Answer> answers = this.getAvailableAnswers();
		ArrayList<Answer> answersOdd = new ArrayList<>();
		ArrayList<Answer> answersEven = new ArrayList<>();
		int size = answers.size();
		for(int i = 0; i < size; i++) {
			if(i % 2 == 0) {
				answersOdd.add(answers.get(i));
			} else {
				answersEven.add(answers.get(i));
			}
		}
		Collections.shuffle(answersOdd);
		Collections.shuffle(answersEven);
		answers.clear();
		for(int i = 0; i < size / 2; i++) {
			answers.add(answersOdd.get(i));
			answers.add(answersEven.get(i));
		}
		this.setAvailableAnswers(answers);
	}

	public CorrelateQuestion(int id, String text, Test test) {
		super(id, text, test);
	}
	public CorrelateQuestion(String text, Test test) {
		super(text, test);
	}

	public int getType() {
		return 4;
	}

	@Override
	public boolean isAnswersCorrect() {
		ArrayList<Answer> answers = getUserAnswers();
		if(answers.size() %2 != 0 || answers.size() == 0) {
			return false;
		}
		for(int i = 0; i < answers.size(); i += 2) {
			if(Integer.parseInt(answers.get(i).getComb()) !=
					Integer.parseInt(answers.get(i + 1).getComb()) - 1) {
				return false;
			}
		}
		return true;
	}
}
