package tester.transfers.questions;

/**
 * Created by andrei on 11.2.15.
 */
public class Customization {

    private int answersInRow = 1;
    private int answerAreaSize = 100;
    private int answerAreaLeftOffset = 0;

    public void setAnswersInRow(int count) {
        this.answersInRow = count;
    }
    public void setAnswerAreaSize(int size) {
        this.answerAreaSize = size;
    }
    public void setAnswerAreaLeftOffset(int offset) {
        this.answerAreaLeftOffset = offset;
    }

    public int getAnswersInRow() {
        return answersInRow;
    }
    public int getAnswerAreaSize() {
        return answerAreaSize;
    }
    public int getAnswerAreaLeftOffset() {
        return answerAreaLeftOffset;
    }
}
