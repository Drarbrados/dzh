package tester.transfers.questions;

import java.util.TreeSet;

import tester.transfers.Answer;
import tester.transfers.tests.Test;

public class SeveralMultipleQuestion extends AbstractQuestion {

	{
		getCustomization().setAnswersInRow(1);
		getCustomization().setAnswerAreaSize(100);
		getCustomization().setAnswerAreaLeftOffset(0);
	}

	@Deprecated
	public SeveralMultipleQuestion( ) {
	}
	@Deprecated
	public SeveralMultipleQuestion(int id, String text) {
		super(id, text);
	}


	public SeveralMultipleQuestion(int id, String text, Test test) {
		super(id, text, test);
	}
	public SeveralMultipleQuestion(String text, Test test) {
		super(text, test);
	}

	public int getType() {
		return 2;
	}

	@Override
	public boolean isAnswersCorrect() {
		TreeSet<Answer> answers = new TreeSet<Answer>(getUserAnswers());
		
		for(Answer curr : getAvailableAnswers())
			if("true".equals(curr.getComb())){// curr нужно было отметить
				if(!answers.remove(curr))
					return false;// но пользователь не отметил
			}
			else// сurr не нужно было отмечать
				if(answers.remove(curr))
					return false;// но пользователь отметил
		
		return answers.isEmpty();// должно быть всегда true.
	}
}
