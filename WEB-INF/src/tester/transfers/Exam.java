package tester.transfers;

import java.util.*;

import tester.dao.QuestionDAO;
import tester.dao.StatisticsDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.exceptions.DAOException;
import tester.transfers.users.User;
import tester.dao.AlgorithmDAO;
import tester.transfers.algorithms.Algorithm;
import tester.transfers.questions.Question;
import tester.transfers.tests.SalutableTest;

//TODO: Remove all System.out.println_s;
/**
 * It's not a transfer!
 * @author DzianisH
 *
 */
public class Exam {
	private static TesterDAOFactory factory = TesterDAOFactory.getFactory();
	private static StatisticsDAO statisticsDAO = factory.getStatisticsDAO();
	private static QuestionDAO questionDAO = factory.getQuestionDAO();
	private volatile boolean training = true;
	private SalutableTest test;
	private int statisticsId;
	private ArrayList<Question> questions;
	private Question question;
	private User user;
	private int mark;
	private long startingTime = -1;

	/*public Exam(SalutableTest test, User user) {
		this.test = test;
		this.user = user;
		questions = questionDAO.getTestQuestionsForExam(test);
		Collections.shuffle(questions);
	}*/

	public void расскажиМнеОКодКонвеншене(){
	}

	public Exam(SalutableTest st, User user) throws DAOException{
		this.user = user;
		test = st;
		TesterDAOFactory factory = TesterDAOFactory.getFactory();
		AlgorithmDAO algorithmDAO = factory.getAlgorithmDAO();
		QuestionDAO questionDAO = factory.getQuestionDAO();
		
		questions = new ArrayList<Question>();
		ArrayList<QuestionsGroup> qglist = factory.getQuestionsGroupDAO()
				.getQuestionsGroupsFor(st);
		
		ArrayList<Question> questionsForQGroup;
		TreeSet<Question> questionsSet = new TreeSet<Question>();
		for(QuestionsGroup qg : qglist){
			System.out.println("Hi");
			// get all q, attached to qg   and get get algorithm for it
			questionsForQGroup = questionDAO.getQuestionsFor(qg);

			removeRepeatingFromList(questionsForQGroup, questionsSet);
			Algorithm algorithm = algorithmDAO.getAlgorithmFor(qg);

			System.out.println(questionsForQGroup.size());
			//do algorithm and save result 
			for(Question selectedQuestion : algorithm.doAlgorithm(questionsForQGroup)){
				System.out.println("Hi!!");
				questions.add(selectedQuestion);
				questionsSet.add(selectedQuestion);
			}
			algorithm.destroy();

			System.out.println("\tGet questions:");
			printList(questions);
		}
//		questions = factory.getQuestionDAO().getTestQuestionsForExam(st);
	}

	private void removeRepeatingFromList(
			ArrayList<Question> questionsList, TreeSet<Question> questionsSet) {

		Iterator<Question> iterator = questionsList.iterator();

		System.out.println("\tBEFORE:");
		printList(questionsList);
		while(iterator.hasNext()){
			Question question = iterator.next();
			if(questionsSet.contains(question)){
				System.out.println(question.getId() + " It repeat!");
				iterator.remove();
			}
			question.setComplete();// Remove me... no, I'm flush answers, don't remove me
		}

		System.out.println("\tAFTER:");
		printList(questionsList);
	}

	private void printList(List<Question> list){
		for(Question q : list)
			System.out.print(q.getId() + ", ");
		System.out.println("\b\b    \n");
	}

	public Question getQuestion() {
		if(questions.size() > 0) {
			question = questions.get(0);
			return questions.get(0);
		} else {
			return null;
		}
	}

	public boolean checkAnswers() {
        /*No realization yet*/
		return false;
	}
	public void removeCurrentQuestion() {
		questions.remove(question);
	}
	public void moveCurrentQuestionToEnd() {
		questions.remove(question);
		questions.add(question);
	}

	public int getMark() {
		return mark;
	}

	public String toString() {
		return test.getTitle() + " [" + user.getFname() + " " + user.getLname() + "]";
	}

	public boolean isTraining() {
		return training;
	}
	
	public void setTraining(boolean training) {
		this.training = training;
	}

	public User getUser() {
		return this.user;
	}

	public void startExam() {
		if(!training) {
			startingTime = new Date().getTime();
		}
		try {
			statisticsId = statisticsDAO.addTestStat(this);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}


	public long getStartingTime() {
		return startingTime;
	}
	public void resetTimer() {
		this.startingTime = -1;
	}


	public void passQuestion() {
		try {
			if (question.isAnswersCorrect()) {
				mark += question.getPrize();
				statisticsDAO.addQuestionStat(this, question.getPrize());
			} else {
				statisticsDAO.addQuestionStat(this, 0);
			}
		} catch (DAOException e) {
			e.printStackTrace();
		}
		this.removeCurrentQuestion();
	}

	public SalutableTest getTest() {
		return this.test;
	}

	public long getTestTime() {
		return this.test.getMaxTime();
	}

	public int getStatisticsId() {
		return statisticsId;
	}
}
