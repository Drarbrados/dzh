package tester.transfers.exceptions;

/**
 * We don't need it. We can call setter any time we need.
 * (so that we use beans)
 * @author Denis
 *
 */
@Deprecated
public class ObjectCannotBeChangedException extends RuntimeException {
	private static final long serialVersionUID = -3591797770642998424L;
	
	public ObjectCannotBeChangedException( ){ }
	public ObjectCannotBeChangedException(String msg){ super(msg); }

}
