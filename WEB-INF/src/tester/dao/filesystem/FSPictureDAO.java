package tester.dao.filesystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import tester.dao.PictureDAO;
import tester.dao.exceptions.ConfigurationsError;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.ElementIsMissingException;
import tester.service.Logger;
import tester.service.PropertiesAccess;
import tester.service.Time;
import tester.transfers.Picture;

// TODO: same ids for temp and perm dirs.
public class FSPictureDAO implements PictureDAO {
	private static final Logger LOG = Logger.getLogger(FSPictureDAO.class);
	private static final AtomicInteger ids = new AtomicInteger();
	
	private static final String
			tempIdFormat = "t%h=%d_%s",// uniqueId, time, realName
			permIdFormat = "P%H=%d_%s";
	
	private static final String temp, perm;
	private final DeleteTempFilesScheduler fileGC;
	
	static{
		File folder = null;
		URI uri = null;
		try {
			uri = FSPictureDAO.class.getResource("").toURI();
			folder = new File(uri).getParentFile().getParentFile().getParentFile().getParentFile();
		} catch (URISyntaxException e) {
			LOG.error(e, "Can't find path to properties.");
			new ConfigurationsError("Can't init FSPictureDAO class.", e);
		} catch(RuntimeException re){
			LOG.error(re, "Unexcpected exception.");
			new ConfigurationsError("Can't init FSPictureDAO class.", re);
		}
		String path = String.format("%s%c%s%c", folder.getParent(),
				File.separatorChar, "images", File.separatorChar);
		System.out.println("PATH: " + path);
		new File(path).mkdirs();

		temp = String.format("%s%s%c", path, "temp", File.separatorChar);
		LOG.info("Using temporary files path: '%s'", temp);
		
		perm = String.format("%s%s%c", path, "perm", File.separatorChar);
		LOG.info("Using permanent files path: '%s'", perm);
		

		new File(temp).mkdirs();
		new File(perm).mkdirs();
	}
	
	public FSPictureDAO( ){
		Properties map = null;
		try {
			map = PropertiesAccess.loadProperties("Global");
		} catch (FileNotFoundException e1) {
			LOG.warn(e1, "Can't find file Global.properties "
					+ "to refresh temp directory.");
		}
		
		boolean refresh = Boolean.parseBoolean(
				map.getProperty("RefreshTempFilesDirectoryOnStrartup", "false")
			);
		if(refresh)
			if(!refreshDirectory(new File(temp)))
				LOG.warn("Can't refresh directory '%s'.", temp);
		
		int time = 1200;
		try{
			time = Integer.parseInt(map.getProperty("DeleteFilesInTempDirectoryDelay", "1200"));
		}
		catch(NumberFormatException e){
			LOG.warn("Bad value in property DeleteFilesInTempDirectoryDelay=%s",
					map.getProperty("DeleteFilesInTempDirectoryDelay"));
		}
		fileGC = new DeleteTempFilesScheduler(time);
		fileGC.start();
	}

	@Override
	public boolean compareTempPerm(String f1, String f2) {
		if(f1 == null || f2 == null) {
			return false;
		}
		if(f1.substring(1).equals(f2.substring(1))) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String saveLikeTemp(InputStream in, String name) throws DAOException {
		String id = null;
		try {
			id = saveFile(in, name, false);
		} catch (IOException e) {
			throw new DAOException(e);
		}
		return id;
	}

	@Override
	public String saveLikePerm(InputStream in, String name) throws DAOException {
		String id = null;
		try {
			id = saveFile(in, name, true);
		} catch (IOException e) {
			throw new DAOException(e);
		}
		return id;
	}
	
	@Override
	public String saveLikeTemp(File pict, String name) throws DAOException {
		String id = null;
		try {
			id = saveFile(pict, name, false);
		} catch (IOException e) {
			throw new DAOException(e);
		}
		return id;
	}
	
	@Override
	public String saveLikePerm(File pict, String name) throws DAOException {
		String id = null;
		try {
			id = saveFile(pict, name, true);
		} catch (IOException e) {
			throw new DAOException(e);
		}
		return id;
	}

	
	
	@Override
	public Picture getPicture(String id) throws ElementIsMissingException {
		getPictureFile(id);// проверка на существование
		
		int tmp = id.indexOf('_') + 1;
		if(tmp == 0){
			throw new ElementIsMissingException(String.format(
					"Invalid id='%s'.", id));
		}
		String name = id.substring(tmp);
		return new Picture(id, name);
	}
	
	// Делать matcher на id ?
	@Override
	public File getPictureFile(String id) throws ElementIsMissingException {
		String path = (id.charAt(0) == 'P' ? perm : temp) + id;
		File file = new File(path);
		if(!file.exists()){
			throw new ElementIsMissingException(String.format(
					"Invalid id='%s'. File not Excists.", id));
		}
		return file;
	}
	

	@Override
	public void removePicture(String id){
		if(id.startsWith("WEB-INF") || id.indexOf('/') != -1 ||
				id.indexOf('\\') != -1 || id.indexOf(File.separatorChar) != -1)
			return;
		
		if(id.charAt(0) == 't')
			new File(temp + id).delete();
		else if(id.charAt(0) == 'P')
			new File(perm + id).delete();
	}
	
	
	
	
	
	/**
	 * Save file and return id;
	 * @param in
	 * @return id of saved file
	 */
	private String saveFile(InputStream in, String name, boolean permDir) throws IOException{
		String id = generateId(name, permDir);
		String path = (permDir ? perm : temp) + id;
		
		File file = new File(path);
		FileOutputStream out = null;
		String msg = null;
		try {
			if(!file.createNewFile()){
				msg = String.format("Can't save file, because file %s alrady exists.", path);
				throw new IOException(msg);
			}
			
			out = new FileOutputStream(file);
			int byt;
			while( (byt = in.read()) != -1)
				out.write(byt);
		} catch (IOException e){
			if(msg != null)
				LOG.error(msg);
			else
				LOG.error(e, "Unsuccessfull file saving in '%s'.", path);
			throw e;
		}
		finally{
			if(out != null){
				out.flush();
				out.close();
			}
		}
		
		return id;
	}
	
	private String saveFile(File file, String name, boolean permDir) throws IOException{
		String id = generateId(name, permDir);
		String path = (permDir ? perm : temp) + id;
		
		File dest = new File(path);
		if(dest.exists()){
			throw new IOException(String.format(
					"Can't save file, because file %s alrady exists.", path)
					);
		}
		if(!file.renameTo(dest)){
			throw new IOException(String.format(
					"Unexcpected exception when moving file to '%s'", path)
					);
		}
		
		return id;
	}
	
	
	
	
	private String generateId(String name, boolean permDir){
		int tmp1 = name.lastIndexOf('/'), tmp2 = name.lastIndexOf('\\'),
				tmp3 = name.lastIndexOf(File.separatorChar);// можно сделать в 1 цикл!  ускорение до 3х раз
		int tmp = Math.max(Math.max(tmp1, tmp2), tmp3);
		if(tmp != -1)
			name = name.substring(tmp + 1);
		
		return String.format(permDir ? permIdFormat : tempIdFormat,
				ids.incrementAndGet() ^ 0x716ea23b,
				Time.getNanoTime(),
				name);
	}

	private static boolean refreshDirectory(File directory) {
		boolean ok = true;
	    if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null != files){
	            for(int i=0; i < files.length; i++) {
	                if(files[i].isDirectory()) {
	                	ok &= refreshDirectory(files[i]);
	                }
	                ok &= files[i].delete();
	            }
	        }
	    }
	    return ok;
	}
	
	
	
	public static class DeleteTempFilesScheduler extends Thread {
		private static final Logger LOG = Logger.getLogger(DeleteTempFilesScheduler.class);
		private volatile int delay;// seconds
		private volatile boolean working = true;
		
		public DeleteTempFilesScheduler(int delaySeconds){
			delay = delaySeconds;
			
			setPriority(Thread.MIN_PRIORITY);
			setDaemon(true);
			LOG.info("Temp directory will be refreshed every %d seconds.", delay);
		}
		
		public void run( ){
			while(working){
				try {
					Thread.sleep(delay * 500L);
				} catch (InterruptedException e) {
					LOG.warn("DeleteTempFilesScheduler's thread was interrupted.");
					Thread.currentThread().interrupt();
				}
				doWork();
			}
		}
		
		// this guy want 30% CPU :(
		private void doWork( ){
			File dir = new File(temp);
			if( !(dir.exists() && dir.isDirectory()) ){// if not exists or not directory
				try {
					dir.mkdirs();
				} catch (RuntimeException e) {
				}
				return;
			}
			
			long fTime, maxTime = Time.getNanoTime() - delay * 1000_000_000L;
			int i1, i2, len;
			for(String fileName : dir.list()){
				i2 = -1; i1 = 0;
				len = fileName.length();
				for(int i = 0; i < len; ++i){
					if(fileName.charAt(i) == '='){
						i1 = i + 1;
						break;
					}
				}
				if(i1 == 0){
					remove(fileName);
					LOG.warn("Bad file found in temp directory: '%s'", fileName);
					continue;
				}
					
				for(int i = i1; i < len; ++i){
					if(fileName.charAt(i) == '_'){
						i2 = i;
						break;
					}
				}
				if(i2 == -1 || i2 <= i1){
					remove(fileName);
					LOG.warn("Bad file found in temp directory: '%s'", fileName);
					continue;
				}
				
				try{
					fTime = Long.parseLong( fileName.substring(i1, i2) );
					if(fTime <= maxTime)
						remove(fileName);
				}
				catch(NumberFormatException | IndexOutOfBoundsException e){
					remove(fileName);
					LOG.warn("Bad file found in temp directory: '%s'", fileName);
				}
			}
		}
		
		private void remove(String fileName){
			try{
				File file = new File(temp + fileName);
				if(file.isDirectory())
					refreshDirectory(file);
				file.delete();
			}
			catch(RuntimeException e){
			}
		}
	}
}
