package tester.dao;

import tester.dao.exceptions.DAOException;
import tester.transfers.Exam;
import tester.transfers.stats.StatsForTest;
import tester.transfers.users.User;

import java.util.ArrayList;


public interface StatisticsDAO {

    public int addTestStat(Exam exam) throws DAOException;
    public void addQuestionStat(Exam exam, int result) throws DAOException;

    public ArrayList<StatsForTest> getPersonalStats(User user) throws DAOException;
//    public void getQuestionStatsInScope(User user);

}
