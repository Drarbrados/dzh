package tester.dao.exceptions;
/**
 * Deprecated.
 * Use DAOException or ElementIsMissingException.
 */
@Deprecated
public class IncorrectInputDataException extends Exception {
	private static final long serialVersionUID = 3591797770642998424L;
	
	public IncorrectInputDataException() {
		super();
	}

	public IncorrectInputDataException(Throwable cause) {
		super(cause);
	}

	public IncorrectInputDataException(String message) {
		super(message);
	}
	
	public IncorrectInputDataException(String message, Throwable cause) {
		super(message, cause);
	}
}
