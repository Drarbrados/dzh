package tester.dao.exceptions;

/**
 * Deprecated.
 * Use ElementIsMissingException.
 */
@Deprecated
public class AuthenticationException extends IncorrectInputDataException {
	private static final long serialVersionUID = 3591797770642998424L;
	
	public AuthenticationException() {
		super();
	}

	public AuthenticationException(Throwable cause) {
		super(cause);
	}

	public AuthenticationException(String message) {
		super(message);
	}
	
	public AuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}
}
