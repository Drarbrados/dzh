package tester.dao.exceptions;

public class ConnectionException extends Error {
	private static final long serialVersionUID = 3591797733132998424L;
	
	public ConnectionException() {
		super();
	}

	public ConnectionException(Throwable cause) {
		super(cause);
	}

	public ConnectionException(String message) {
		super(message);
	}
	
	public ConnectionException(String message, Throwable cause) {
		super(message, cause);
	}
}
