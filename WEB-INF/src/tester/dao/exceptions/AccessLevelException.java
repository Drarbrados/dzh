package tester.dao.exceptions;

public class AccessLevelException extends ElementIsMissingException {
	private static final long serialVersionUID = 3591797723132998424L;
	
	public AccessLevelException() {
		super();
	}

	public AccessLevelException(Throwable cause) {
		super(cause);
	}

	public AccessLevelException(String message) {
		super(message);
	}
	
	public AccessLevelException(String message, Throwable cause) {
		super(message, cause);
	}

}
