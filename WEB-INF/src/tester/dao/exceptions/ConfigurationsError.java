package tester.dao.exceptions;

public class ConfigurationsError extends Error {
	private static final long serialVersionUID = 1789231231231L;

	public ConfigurationsError() {
		super();
	}

	public ConfigurationsError(Throwable cause) {
		super(cause);
	}

	public ConfigurationsError(String message) {
		super(message);
	}
	
	public ConfigurationsError(String message,Throwable cause) {
		super(message, cause);
	}
}
