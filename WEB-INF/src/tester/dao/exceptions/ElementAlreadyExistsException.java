package tester.dao.exceptions;

public class ElementAlreadyExistsException extends DAOException {
	private static final long serialVersionUID = 3512312712132998424L;
	
	public ElementAlreadyExistsException() {
		super();
	}

	public ElementAlreadyExistsException(Throwable cause) {
		super(cause);
	}

	public ElementAlreadyExistsException(String message) {
		super(message);
	}
	
	public ElementAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
