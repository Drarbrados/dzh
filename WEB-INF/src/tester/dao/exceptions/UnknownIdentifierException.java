package tester.dao.exceptions;
/**
 * Deprecated.
 * Use ElementIsMissingException.
 */
@Deprecated
public class UnknownIdentifierException extends Exception {
	private static final long serialVersionUID = 1L;

	public UnknownIdentifierException() {
		super();
	}

	public UnknownIdentifierException(Throwable cause) {
		super(cause);
	}

	public UnknownIdentifierException(String message) {
		super(message);
	}
	
	public UnknownIdentifierException(String message,Throwable cause) {
		super(message, cause);
	}

}
