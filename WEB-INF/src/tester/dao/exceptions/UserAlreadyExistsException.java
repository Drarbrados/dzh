package tester.dao.exceptions;
/**
 * Deprecated.
 * Use ElementAlreadyExistsException.
 */
@Deprecated
public class UserAlreadyExistsException extends IncorrectInputDataException {
	private static final long serialVersionUID = 3591797770642998424L;
	
	public UserAlreadyExistsException() {
		super();
	}

	public UserAlreadyExistsException(Throwable cause) {
		super(cause);
	}

	public UserAlreadyExistsException(String message) {
		super(message);
	}
	
	public UserAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}

}
