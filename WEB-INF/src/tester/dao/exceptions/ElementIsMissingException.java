package tester.dao.exceptions;

public class ElementIsMissingException extends DAOException {
	private static final long serialVersionUID = -45454512132998424L;
	
	public ElementIsMissingException() {
		super();
	}

	public ElementIsMissingException(Throwable cause) {
		super(cause);
	}

	public ElementIsMissingException(String message) {
		super(message);
	}
	
	public ElementIsMissingException(String message, Throwable cause) {
		super(message, cause);
	}
}
