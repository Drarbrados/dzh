package tester.dao;

import tester.dao.exceptions.AuthenticationException;
import tester.dao.exceptions.DAOException;
import tester.transfers.Group;
import tester.transfers.users.User;

import java.util.List;

public interface UserDAO {
	public User getUser(String login, String password)
			throws AuthenticationException;
	public boolean verify(User user);
	public void addUser(User user) throws DAOException;
	public User getUser(String login);
	public User getUser(int useId);
	public List<User> getUsersFromGroup(Group group) throws DAOException;
	public void update(User user) throws DAOException;
}
