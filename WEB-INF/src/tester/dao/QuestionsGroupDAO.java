package tester.dao;

import java.util.ArrayList;

import tester.dao.exceptions.DAOException;
import tester.transfers.QuestionsGroup;
import tester.transfers.tests.Test;

public interface QuestionsGroupDAO {
	ArrayList<QuestionsGroup> getQuestionsGroupsFor(Test test) throws DAOException;
	ArrayList<QuestionsGroup> getQuestionsGroupsFor(int testId) throws DAOException;
}
