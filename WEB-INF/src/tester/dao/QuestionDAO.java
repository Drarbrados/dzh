package tester.dao;

import java.util.ArrayList;

import tester.dao.exceptions.DAOException;
import tester.transfers.tests.ModerableTest;
import tester.transfers.QuestionsGroup;
import tester.transfers.tests.SalutableTest;
import tester.transfers.questions.Question;
import tester.transfers.tests.Test;

public interface QuestionDAO {
	/**
	 * Move me to SalutableTestDAO !! or remove me!!
	 * @param stest
	 * @return
	 */
	@Deprecated
	public ArrayList<Question> getTestQuestionsForExam(SalutableTest stest);
	
	public ArrayList<Question> getQuestionsFor(Test stest);
	public ArrayList<Question> getQuestionsForTest(int testId);

	public ArrayList<Question> getQuestionsFor(QuestionsGroup questionsGroup);
	public ArrayList<Question> getQuestionsForQuestionsGroup(int qgroupId);
	
	public Question getQuestionById(int questionId);

	public void updateQuestionData(Question question) throws DAOException;
	public void addQuestion(Question question) throws DAOException;


	public int getQuestionsNumberFor(Test test);
}
