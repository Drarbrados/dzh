package tester.dao;

import java.io.FileNotFoundException;
import java.util.Properties;

import tester.dao.exceptions.ConfigurationsError;
import tester.service.Logger;
import tester.service.PropertiesAccess;

public abstract class TesterDAOFactory {
	private static final Logger LOG = Logger.getLogger(TesterDAOFactory.class);
	private static final TesterDAOFactory factory;
	
	static{

		Properties map = null;
		try {
			map = PropertiesAccess.loadProperties("Global");
		} catch (FileNotFoundException e1) {
			LOG.fatal(e1, "Can't find file Global.properties %s",
					"to instanse new factory.");
			throw new ConfigurationsError(
					"Can't find config file to instanse new factory.", e1);
		}
		String factoryName = map.getProperty("FactoryName");
		
		TesterDAOFactory fact = null;
		try {// create factory object
			
			fact = (TesterDAOFactory) Class.forName(factoryName).newInstance();
		
		} catch (InstantiationException e) {
			LOG.fatal(e, "This Class %s represents an abstract class, %s%s%s%s",
					factoryName,
					"an interface, an array class, a primitive type, or void; ",
					"or if the class has no nullary constructor; ",
					"or if the instantiation fails for some other reason.");
			throw new ConfigurationsError(
					"Invalide configs to create new factory.", e);
		} catch (IllegalAccessException e) {
			LOG.fatal(e, "The Class %s or its nullary constructor is not accessible.",
					factoryName);
			throw new ConfigurationsError(
					"Invalide configs to create new factory.", e);
		} catch(ClassNotFoundException e) {
			LOG.fatal(e, "Missing Class's name, but found '%s'. %s",
					factoryName, "Class not found.");
			throw new ConfigurationsError(
					"Invalide configs to create new factory.", e);
		} catch (ClassCastException e){
			LOG.fatal(e, "Class %s must extends %s .", factoryName,
					TesterDAOFactory.class.getName());
			throw new ConfigurationsError(
					"Invalide configs to create new factory.", e);
		}
		finally {
			factory = fact;
			LOG.info("Currently using DAO-factory: %s", factory);
		}

		for(int i = 0; i < 16; ++i)// I want gc :)
			System.gc();
		
		LOG.info("Model ready to work.");
	}
	
	
	public static TesterDAOFactory getFactory( ){	return factory; }	
	protected TesterDAOFactory( ){ }
	
	public abstract UserDAO getUserDAO( );
	public abstract GroupDAO getGroupDAO( );
	public abstract ModerableTestDAO getModetableTestDAO( );
	public abstract SalutableTestDAO getSalutableTestDAO( );
	public abstract QuestionDAO getQuestionDAO( );
	public abstract QuestionsGroupDAO getQuestionsGroupDAO( );
	public abstract AnswerDAO getAnswerDAO( );
	public abstract PictureDAO getPictureDAO( );
	public abstract StatisticsDAO getStatisticsDAO( );
	public abstract AlgorithmDAO getAlgorithmDAO( );
	
	public abstract Data getDatabaseData( );
	
	
	public String toString( ){
		return getClass().getName();
	}
}
