package tester.dao;

import java.io.File;
import java.io.InputStream;

import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.ElementIsMissingException;
import tester.transfers.Picture;

// TODO: Make object Picture. Temp/Perm is just a flag. Do single method save();.
public interface PictureDAO {
	public String saveLikeTemp(InputStream in, String name) throws DAOException;
	public String saveLikePerm(InputStream in, String name) throws DAOException;
	
	public String saveLikeTemp(File pict, String name) throws DAOException;
	public String saveLikePerm(File pict, String name) throws DAOException;
	
	public Picture getPicture(String id) throws ElementIsMissingException;
	public File getPictureFile(String id)  throws ElementIsMissingException;

	public boolean compareTempPerm(String f1, String f2);

	
	public void removePicture(String id);
}
