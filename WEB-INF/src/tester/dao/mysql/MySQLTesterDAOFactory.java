package tester.dao.mysql;

import tester.dao.AlgorithmDAO;
import tester.dao.Data;
import tester.dao.TesterDAOFactory;
import tester.dao.filesystem.FSPictureDAO;
import tester.service.Logger;

public class MySQLTesterDAOFactory extends TesterDAOFactory {
	private static final Logger LOG = Logger.getLogger(MySQLTesterDAOFactory.class);
	
	private final MySQLUserDAO user = new MySQLUserDAO();
	private final MySQLGroupDAO group = new MySQLGroupDAO();
	private final MySQLModerableTestDAO mtest = new MySQLModerableTestDAO();
	private final MySQLSalutableTestDAO stest = new MySQLSalutableTestDAO();
	private final MySQLQuestionDAO question = new MySQLQuestionDAO(user);
	private final MySQLQuestionsGroupDAO qgroup = new MySQLQuestionsGroupDAO();
	private final FSPictureDAO picture = new FSPictureDAO();
	private final MySQLStatisticsDAO statistics = new MySQLStatisticsDAO(stest);
	private final MySQLAnswerDAO answers = new MySQLAnswerDAO();
	private final MySQLAlgorithmDAO algorithms = new MySQLAlgorithmDAO();
	private final MySQLData data = new MySQLData();
	
	public MySQLTesterDAOFactory( ){
		try {
			Class.forName("tester.dao.mysql.service.ConnectionManager");
		} catch (ClassNotFoundException | RuntimeException e) {
			LOG.warn(e, "Can't load class.");
		}
		try {
			Class.forName("tester.dao.mysql.service.SQLPattern");
		} catch (ClassNotFoundException | RuntimeException e) {
			LOG.warn(e, "Can't load class.");
		}
		try {
			Class.forName("tester.dao.mysql.service.Connector");
		} catch (ClassNotFoundException | RuntimeException e) {
			LOG.warn(e, "Can't load class.");
		}
		LOG.info("%s was successfully runned.", MySQLTesterDAOFactory.class.getName());
	}
	
	@Override
	public MySQLUserDAO getUserDAO() {
		return user;
	}


	@Override
	public MySQLGroupDAO getGroupDAO() {
		return group;
	}

	@Override
	public MySQLModerableTestDAO getModetableTestDAO() {
		return mtest;
	}

	@Override
	public MySQLSalutableTestDAO getSalutableTestDAO() {
		return stest;
	}

	@Override
	public MySQLQuestionDAO getQuestionDAO() {
		return question;
	}

	@Override
	public MySQLQuestionsGroupDAO getQuestionsGroupDAO() {
		return qgroup;
	}

	@Override
	public FSPictureDAO getPictureDAO() {
		return picture;
	}

	@Override
	public MySQLStatisticsDAO getStatisticsDAO() {
		return statistics;
	}

	@Override
	public MySQLAnswerDAO getAnswerDAO() {
		return answers;
	}
	
	@Override
	public AlgorithmDAO getAlgorithmDAO() {
		return algorithms;
	}

	@Override
	public Data getDatabaseData() {
		return data;
	}
}
