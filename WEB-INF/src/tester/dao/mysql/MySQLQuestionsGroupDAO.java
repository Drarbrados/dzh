package tester.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tester.dao.QuestionsGroupDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.SQLPattern;
import tester.transfers.QuestionsGroup;
import tester.transfers.tests.Test;

public class MySQLQuestionsGroupDAO implements QuestionsGroupDAO {

	@Override
	public ArrayList<QuestionsGroup> getQuestionsGroupsFor(Test test)
			throws DAOException{
		return getQuestionsGroupsFor(test.getId());
	}

	@Override
	public ArrayList<QuestionsGroup> getQuestionsGroupsFor(int testId)
			 throws DAOException {
		ArrayList<QuestionsGroup> list = new ArrayList<QuestionsGroup>();
		
		String select= String.format(
				"SELECT qGroupId, testId, algorithmId FROM QuestionsGroups WHERE testId=%d",
				testId
				);
		QuestionsGroup qgroup = null;
		try (SQLPattern.Answer answ = SQLPattern.sendQuery(select)){
			ResultSet rs = answ.getResultSet();
			while(rs.next()){
				qgroup = new QuestionsGroup();
				qgroup.setId(rs.getInt(1));
				qgroup.setTestId(rs.getInt(2));
				qgroup.setAlgorithmId(rs.getInt(3));
				list.add(qgroup);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		
		return list;
	}

}
