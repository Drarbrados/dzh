package tester.dao.mysql;

import tester.dao.SalutableTestDAO;
import tester.dao.StatisticsDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.dao.mysql.service.SQLPattern;
import tester.transfers.Exam;
import tester.transfers.stats.Stats;
import tester.transfers.stats.StatsForTest;
import tester.transfers.stats.objects.TestStat;
import tester.transfers.stats.objects.UserStat;
import tester.transfers.tests.TestMode;
import tester.transfers.users.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

public class MySQLStatisticsDAO implements StatisticsDAO {

    private SalutableTestDAO salutedTestDAO;

    public MySQLStatisticsDAO(SalutableTestDAO test) {
        salutedTestDAO = test;
    }

    @Override
    public int addTestStat(Exam exam)
            throws DAOException {

        String insert = String.format("INSERT INTO Tester.TestsStatistics(%s) " +
                        "VALUES (%d, %d, %s, CURDATE(), CURTIME())",
                        "userId, testId, isTraining, dayD, timeT",
                exam.getUser().getId(),
                exam.getTest().getId(),
                SQLPattern.parseToSQL(Boolean.toString(exam.isTraining())));
        String select = String.format(
                "SELECT MAX(statisticsId) FROM Tester.TestsStatistics WHERE " +
                "userId=%d AND testId=%d",
                exam.getUser().getId(), exam.getTest().getId());

        int statisticsId = 0;
        try(SQLPattern.Answer answ = SQLPattern.sendUpdateAndQuery(insert, select)) {
            answ.next();
            //TODO: Make me thread-safe!
            statisticsId = answ.getInt("MAX(statisticsId)");
        }
        return statisticsId;
    }

    @Override
    public void addQuestionStat(Exam exam, int result) throws DAOException {
        String insert = String.format("INSERT INTO Tester.QuestionsStatistics(%s) " +
                        "VALUES (%d, %d, %d)",
                        "questionId, testStatisticsId, result",
                exam.getQuestion().getId(),
                exam.getStatisticsId(),
                result);
        SQLPattern.sendUpdate(insert);
    }

    @Override
    public ArrayList<StatsForTest> getPersonalStats(User user) throws DAOException {

        String select = String.format("SELECT %s FROM Tester.TestsStatisticsView WHERE " +
                "userId=%d",
                "statisticsId, testId, dayD, timeT, isTraining, result",
                user.getId());

        ArrayList<StatsForTest> stats = new ArrayList<>();
        try(SQLPattern.Answer answer = SQLPattern.sendQuery(select)) {
            while (answer.next()) {
                Date date = answer.getDate("dayD");
                Time time = answer.getTime("timeT");
                java.util.Date resultDate = new Date(date.getTime() + time.getTime());
                Stats s = new Stats(new UserStat(user), resultDate);
                TestMode tm;
                if(answer.getString("isTraining").equals("true")) {
                    tm = TestMode.TRAINING_MODE;
                } else {
                    tm = TestMode.TEST_MODE;
                }

             //   int result = this.getResultMark(answer.getInt("statisticsId"));
                int result = answer.getInt("result");
                TestStat ts = new TestStat(salutedTestDAO.getTestById(answer.getInt("testId")),
                        result, tm);
                stats.add(new StatsForTest(s, ts));
            }
        } catch (IncorrectInputDataException e) {
            throw new DAOException(e);
        }


        //TODO: hard refactor
        boolean exit = true;
        while(true){
            for (int i = 0; i < stats.size(); i++) {
                exit = true;
                TestStat test = stats.get(i).getTestStat();
                for (int j = i + 1; j < stats.size(); j++) {
                    if(test.getTestName().equals(stats.get(j).getTestStat().getTestName())) {
                        if(test.getTestId() != stats.get(j).getTestStat().getTestId()) {
                            //System.out.println("one " + i + " " + j);
                            TestStat testJ = stats.get(j).getTestStat();
                            String name = testJ.getTestName();
                            testJ = new TestStat(salutedTestDAO.getParentOf(testJ.getParentId()),
                                    testJ.getMark(), testJ.getTestMode());
                            testJ.setTestName(testJ.getTestName().concat("/" + name));
                            stats.get(j).setTestStat(testJ);
                            exit = false;
                        }
                    }
                }
                if(!exit) {
                    for (int j = i; j < stats.size(); j++) {
                        if(test.getTestName().equals(stats.get(j).getTestStat().getTestName())) {
                            if(test.getTestId() == stats.get(j).getTestStat().getTestId()) {
                                //System.out.println("two " + i + " " + j);
                                TestStat testJ = stats.get(j).getTestStat();
                                String name = testJ.getTestName();
                                testJ = new TestStat(salutedTestDAO.getParentOf(testJ.getParentId()),
                                        testJ.getMark(), testJ.getTestMode());
                                testJ.setTestName(testJ.getTestName().concat("/" + name));
                                stats.get(j).setTestStat(testJ);
                            }
                        }
                    }
                }
            }
            if(exit) {
                break;
            }
        }

        return stats;
    }
/*
    @Deprecated //Use TestStatisticsView
    private int getResultMark(int testStatisticsId) throws DAOException {

        String select = String.format("SELECT result FROM Tester.QuestionsStatistics WHERE " +
                        "testStatisticsId=%d",
                testStatisticsId);

        int result = 0;
        try(SQLPattern.Answer answer = SQLPattern.sendQuery(select)) {
            while (answer.next()) {
                result += answer.getInt("result");
            }
        }
        return result;
    }
*/
}
