package tester.dao.mysql.service;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Use SQLPattern.
 * @author Sergey on 11.01.2015.
 */
public class Connector {
    private static final String LOG_MESSAGE = "Hren ego znaet chto za exception. Was only logged.";

    private Connection cn;
    private Statement st;
    private List<PreparedStatement> psList = new ArrayList<PreparedStatement>();
    private ResultSet rs;

    public Connector() {
    }

    public void connection(){
        cn = ConnectionManager.getConnectionManager().getConnection();
    }

    public Statement getStatement() throws SQLException {
        if (st == null) {
            st = cn.createStatement();
        }
        return st;
    }

    public PreparedStatement getPreparedStatement(String sql) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(sql);
        psList.add(ps);
        return ps;
    }

    public ResultSet getResultSet() throws SQLException {
        closeResultSet();
        rs = st.getResultSet();
        return rs;
    }

    public ResultSet getResultSet(PreparedStatement ps) throws SQLException {
        closeResultSet();
        rs = ps.executeQuery();
        return rs;
    }

    public void closePreparedStatements() {
        if (psList.size() != 0) {
            for(PreparedStatement ps : psList) {
                try {
                    ps.close();
                } catch (SQLException e) {
//                    LOG.warn(e, LOG_MESSAGE);
                }
            }
        }
    }

    public void closeResultSet() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
//                LOG.warn(e, LOG_MESSAGE);
            }
        }
    }

    public void closeStatement() {
        if (st != null) {
            try {
                st.close();
                st = null;
            } catch (SQLException e) {
//                LOG.warn(e, LOG_MESSAGE);
            }
        }
    }

    public void disconnection() {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
//                LOG.warn(e, LOG_MESSAGE);
            }
        }
        if (psList.size() != 0) {
            for(PreparedStatement ps : psList) {
                try {
                    ps.close();
                } catch (SQLException e) {
//                    LOG.warn(e, LOG_MESSAGE);
                }
            }
        }
        if (cn != null) {
        	ConnectionManager.getConnectionManager().refundConnection(cn);
        }
    }
}
