package tester.dao.mysql.service;

import tester.dao.exceptions.ConfigurationsError;
import tester.dao.exceptions.ConnectionException;
import tester.service.Logger;
import tester.service.PropertiesAccess;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Refactor me!
 * @author User
 *
 */

public class ConnectionManager {
	private static final Logger LOG = Logger.getLogger(ConnectionManager.class);
	private static final ConnectionManager manager;
	
	// .properties settings
	private final String adrres, login, password;
	private final int limit, startConnNumb;
	
	// java's variables
	private final Queue<Connection> connections = 
			new ConcurrentLinkedQueue<Connection>();
	private final AtomicInteger connExists;
	private final Semaphore semaphore;
	
	static{
		manager = new ConnectionManager( );
	}
	
	public static ConnectionManager getConnectionManager( ){
		return manager;
	}
	
	
	{// read all defaults from  ../config.properties
		Properties map = null;
		try {
			map = PropertiesAccess.loadProperties("MySQL-5.1_configs");
		} catch (FileNotFoundException e1) {
			LOG.fatal(e1, "Can't find file fatrory.properties %s",
					"to instanse new factory.");
			throw new ConfigurationsError(
					"Can't find config file to init MySQLFactoryDAO.", e1);
		}
		
		//set all parameters
		adrres = String.format("%s?%s", map.getProperty("databaseName"),
				map.getProperty("connectionProperties", ""));
		login = map.getProperty("adminLogin", "root");
		password = map.getProperty("adminPassword");
		
		try{
			limit = Integer.parseInt(
					map.getProperty("connectionsLimit", "50"));
		}
		catch(NumberFormatException e){
			LOG.fatal(e, "Can't load property.");
			throw new ConnectionException("Can't load property.", e);
		}
		
		try{
			startConnNumb = Integer.parseInt(
				map.getProperty("startupConnectionsNumber", "15"));
		}
		catch(NumberFormatException e){
			LOG.fatal(e, "Can't load property.");
			throw new ConnectionException("Can't load property.", e);
		}
		

		LOG.info("Database adress: %s", adrres);
		LOG.info("User's login: %s", login);
		LOG.info("User's password: %s", password);
		LOG.info("Connections on startup: %d",
				startConnNumb);
		LOG.info("Max connections number: %d", limit);
	}
	
	// create all variables  (connections and queues)
	ConnectionManager( ){
		try{
			Class.forName("org.gjt.mm.mysql.Driver");
			for(int i = 0; i < startConnNumb; ++i){
				connections.add(createConnection() );
			}
		}
		catch(SQLException sqlex){
			LOG.error(sqlex,
					"Exception was generate, while creating a connection to MySQL server"
					);
			if(connections.isEmpty()){
				String msg =  String.format(
						"Haven't any connections to database %s login=%s password=%s.",
						adrres, login, password);
				LOG.fatal(msg);
				Error err =  new ConnectionException(msg, sqlex);
				throw err;
			}
			else
				LOG.error("Can't create %d connections on startup; %s %d.",
						startConnNumb,
						"current number: ", connections.size());
		} catch (ClassNotFoundException noclasse){
			String msg = String.format("%s %s",
					"Class org.gjt.mm.mysql.Driver is not in classpass.",
					"Can't load any connection to database.");
			Error err = new ConnectionException(msg);
			LOG.fatal(noclasse, msg);
			throw err;
		}
		
		semaphore = new Semaphore(connections.size());
		connExists = new AtomicInteger(connections.size());
	}
	
	/**
	 * 
	 * @return connection from cache
	 * @throws InterruptedException if thread interrupted when it wait.
	 *  for connection from cache
	 */
	public Connection getConnectionInterruptedly( ) throws InterruptedException{
		Connection cn = null;
		semaphore.acquire();
		cn = connections.poll();
		return cn;
	}
	
	/**
	 * 
	 * @return connection from cache.
	 */
	public Connection getConnection( ){
		Connection cn = null;
		boolean ok = false;
		while(!ok) {
			try {
				ok = true;
				cn = getConnectionInterruptedly();
			} catch (InterruptedException e) {
				ok = false;
				e.printStackTrace();
			}
		}
		return cn;
	}
	
	/**
	 * @param returned connection to cache.
	 */
	public void refundConnection(Connection cn){
	/*	for(Connection c : connections)
			if(c == cn){
				semaphore.release();
				return;
			}*/
		if(cn != null){
			connections.add(cn);
			semaphore.release();
		}
	}
	
	@SuppressWarnings("unused")
	private void addNewConnection( ){
		if(connExists.get() >= limit)
			return;
		try {
			connections.add(createConnection( ) );
			connExists.incrementAndGet();
			semaphore.release();
		} catch (SQLException e) {
			LOG.warn("unsuccessful attempt to create new connection.%s%d", 
					" Having not less than: ", connections.size());
		}
	}
	
	private Connection createConnection( ) throws SQLException{
		return DriverManager.getConnection(adrres, login, password);
	}
	
	
	
	@Override
	public ConnectionManager clone( ){
		return this;
	}
}
