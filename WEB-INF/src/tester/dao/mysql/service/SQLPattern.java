package tester.dao.mysql.service;

import java.sql.*;
import java.util.Collection;

import tester.dao.exceptions.DAOException;
import tester.service.Logger;

public abstract class SQLPattern {
	private static final Logger LOG = Logger.getLogger(SQLPattern.class);
	private final static ConnectionManager manager =
			ConnectionManager.getConnectionManager();
	
	public static Answer sendQuery(String sql) throws DAOException {
		Connection cn = null;
		Statement st = null;
		ResultSet res = null;
		boolean ok = false;

						//	System.out.println(sql);////
		try{
			cn = manager.getConnection();
			st = cn.createStatement();
			res = st.executeQuery(sql);
			ok = true;
		}
		catch(SQLException ex){
			DAOException e = new DAOException("Unexpected exception in query.", ex);
			LOG.error(ex, "Unexpected exception in query. Using querty: %s", sql);
			throw e;
		}
		finally{
			if(!ok) {
				Answer.close(cn, st, res);
			}
		}
		
		return new Answer(cn, st, res);
	}
	
	/**
	 * Executes the SQL statement in this PreparedStatement object, which may be any kind of SQL statement. Some prepared statements return multiple results; the execute method handles these complex statements as well as the simpler form of statements handled by the methods executeQuery and executeUpdate. 
	 * The execute method returns a boolean to indicate the form of the first result. You must call either the method getResultSet or getUpdateCount to retrieve the result; you must call getMoreResults to move to any subsequent result(s).

	 * @param sql
	 * @return true if the first result is a ResultSet object; false if the first result is an update count or there is no result 
	 * @throws SQLException
	 */
	public static boolean sendUpdate(String sql) throws DAOException{
		Connection cn = null;
		Statement st = null;
		boolean answ = false;

						//	System.out.println(sql);////
		try{
			cn =  manager.getConnection();
			st = cn.createStatement();
			answ = st.execute(sql);
		}
		catch(SQLException ex){
			DAOException e = new DAOException("Unexpected exception in update.", ex);
			LOG.error(ex, "Unexpected exception in update. Using query: %s", sql);
			throw e;
		}
		finally{
			Answer.close(cn, st, null);
		}
		
		return answ;
	}
	
	public static void sendUpdates(Collection<String> updates) throws DAOException{
		if(updates.size() == 0)
			return;
		Connection cn = null;
		Statement st = null;

						//	System.out.println(sql);////
		String currQuery = "";
		try{
			cn =  manager.getConnection();
			st = cn.createStatement();
			for(String update : updates)
				//st.addBatch(update);
				st.execute(currQuery = update);
		}
		catch(SQLException ex){
			DAOException e = new DAOException("Unexpected exception in update.", ex);
			LOG.error(ex, "Unexpected exception in update. Using query: %s", currQuery);
			throw e;
		}
		finally{
			Answer.close(cn, st, null);
		}
	}
	
	/**
	 * Equals to<br>public static Answer sendUpdateAndQuery(String update, String query){<br>
	 * 	 sendUpdate(update);<br>
	 * 	 return sendQuery(query);<br>
	 * } // but a little faster :) <br> <br>
	 * 
	 * At first sends update, than sends query. Query's answer returned.
	 * 
	 * @param update
	 * @param query
	 * @return querys's answer.
	 */
	public static MultyAnswer sendUpdateAndQuery(String update, String query)
				throws DAOException{
		Connection cn = null;
		Statement st = null;
		ResultSet res = null;
		boolean fst = false;
		boolean exInUpdate = true;

	//			System.out.println(update);///
	//			System.out.println(query);///
		try{
			cn =  manager.getConnection();
			st = cn.createStatement();
			System.out.println(update);
			fst = st.execute(update);
			exInUpdate = false;
			res = st.executeQuery(query);
		}
		catch(SQLException ex){
			Answer.close(cn, st, res);
			DAOException e = new DAOException("Unexpected exception in query or update.", ex);
			LOG.error(ex,
					"Unexpected exception in %s. Using query: '%s' and using update '%s'."
					, (exInUpdate ? "Update" : "Query"), query, update);
			throw e;
		}
		
		return new MultyAnswer(cn, st, res, fst);
	}
	
	
	
	
	public static Answer sendUpdatesAndQuery(Collection<String> updates, String query)
			throws DAOException{
	Connection cn = null;
	Statement st = null;
	ResultSet res = null;

//			System.out.println(update);///
//			System.out.println(query);///
	String currSql = "";
	try{
		cn =  manager.getConnection();
		st = cn.createStatement();
		for(String update : updates)
			st.execute(currSql = update);
		currSql = query;
		res = st.executeQuery(query);
	}
	catch(SQLException ex){
		Answer.close(cn, st, res);
		DAOException e = new DAOException("Unexpected exception in sql.", ex);
		LOG.error(ex,
				"Unexpected exception in sql. Exception in query: %s.", currSql);
		throw e;
	}
	
	return new Answer(cn, st, res);
}
	
	
	
	/**
	 * Parse chars ' " \ to SQL format and enclose string in quotes ('). <br>
	 * Example: <br> John "don't eat" Jack's jam :\ <br>=><br>
	 *  John \"don\'t eat\" Jack\'s jam :\\
	 * @param str
	 * @return parsed string enclosed in single quotes or '' if null.
	 */
	public static String parseToSQL(String str){
		if(str == null)
			return "NULL";
		
		StringBuilder sb = new StringBuilder(str.length() + 6);
		sb.append('\'');
		for(int i = 0, len = str.length(); i < len; ++i){
			char ch = str.charAt(i);
			if(ch == '\\' || ch == '\'' || ch == '"')
				sb.append('\\');
			sb.append(ch);
		}
		sb.append('\'');
		
		return sb.toString();
	}
	
	
	
	
	
	
	/**
	 * Simple bean for Connection, Statement and ResultSet objects.
	 * Not thread-safe!!
	 * @author Denis  12.01.2015
	 */
	public static class Answer implements AutoCloseable{
		private Connection connection;
		private Statement statement;
		private ResultSet result;
		
		public Answer( ){ }
		public Answer(Connection cn, Statement st, ResultSet rs){
			connection = cn;
			statement = st;
			result = rs;
		}

		public Connection getConnection() {
			return connection;
		}
		public void setConnection(Connection connection) {
			this.connection = connection;
		}
		public Statement getStatement() {
			return statement;
		}
		public void setStatement(Statement statement) {
			this.statement = statement;
		}
		public ResultSet getResultSet() {
			return result;
		}
		public void setResultSet(ResultSet result) {
			this.result = result;
		}
		
		@Override
		public void close( ){
			close(connection, statement, result);
			result = null;
			statement = null;
			connection = null;
		}
		
		private static void close(Connection connection, Statement statement,
						ResultSet result){
			if(result != null)
				try {
					result.close();
				} catch (SQLException e) {
				}
			if(statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
				}
			
			manager.refundConnection(connection);
		}



		public boolean next() throws DAOException{
			try {
				return result.next();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}
		public boolean last() throws DAOException {
			try {
				return result.last();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}
		public int getInt(String row) throws DAOException {
			try {
				return result.getInt(row);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return -1;
		}
		public String getString(String row) throws DAOException {
			try {
				return result.getString(row);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return "";
		}
		public Date getDate(String row) throws DAOException {
			try {
				return result.getDate(row);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return new Date(0);
		}
		public Time getTime(String row) throws DAOException {
			try {
				return result.getTime(row);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return new Time(0);
		}
	}
	/**
	 * Add boolean answer for SQL update.
	 * @author Dzianis.
	 */
	public static class MultyAnswer extends Answer{
		private boolean firstAnswer;
		public MultyAnswer( ){ }
		public MultyAnswer(boolean firstAnswer){
			this.setWasFirstAnswer(firstAnswer);
		}
		public MultyAnswer(Connection cn, Statement st, ResultSet rs){
			super(cn, st, rs);
		}
		public MultyAnswer(Connection cn, Statement st, ResultSet rs,
				boolean firstAnswer){
			super(cn, st, rs);
			this.setWasFirstAnswer(firstAnswer);
		}
		public boolean isWasFirstAnswer() {
			return firstAnswer;
		}
		public void setWasFirstAnswer(boolean firstAnswer) {
			this.firstAnswer = firstAnswer;
		}
	}
}
