package tester.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tester.dao.SalutableTestDAO;
import tester.dao.exceptions.AccessLevelException;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.dao.mysql.service.SQLPattern;
import tester.service.Logger;
import tester.transfers.Exam;
import tester.transfers.tests.SalutableTest;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

public class MySQLSalutableTestDAO implements SalutableTestDAO {
	private static final Logger LOG = Logger.getLogger(MySQLSalutableTestDAO.class);
	private SalutableTestHandler handler = new SalutableTestHandler();

	@Override
	public ArrayList<SalutableTest> getAllTests(User user) {
		return new ArrayList<SalutableTest>();
	}

	@Override
	public ArrayList<SalutableTest> getRootTests(User user) {
		return getRootTests(user.getGroup().getId());
	}
	
	@Override
	public ArrayList<SalutableTest> getRootTests(int groupId) {
		ArrayList<SalutableTest> list = new ArrayList<>();
		SQLPattern.Answer answ = null;
		
		String sql = String.format(
				"SELECT %s FROM Tests WHERE testId IN (SELECT testId FROM GroupsConnect WHERE groupId=%d);",
			//	"testId, tParent, testName, minCorrectNum, testTime, attemptsPerDay, itTest",
				TestHandler.ALL_FIELDS,
				groupId);

		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet set = answ.getResultSet();
			while(set.next())
				list.add(handler.parseRowToTest(set));
			
		} catch (SQLException e) {
			LOG.error(e, "Unexpected error.");
		} catch (DAOException e) {
		}
		finally{
			if(answ != null)
				answ.close();
		}
		return list;
	}

	@Override
	public ArrayList<SalutableTest> getChildrenOf(SalutableTest test) {
		return getChildrenOf(test.getId());
	}
	
	@Override
	public ArrayList<SalutableTest> getChildrenOf(int testId) {
		ArrayList<SalutableTest> list = new ArrayList<>();
		SQLPattern.Answer answ = null;
		
		String sql = String.format(
				"SELECT %s FROM Tests WHERE tParent=%d AND testId!=tParent",
				//	"testId, tParent, testName, minCorrectNum, testTime, attemptsPerDay, itTest",
				TestHandler.ALL_FIELDS,
				testId);
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet set = answ.getResultSet();
			while(set.next())
				list.add(handler.parseRowToTest(set));
			
		} catch (SQLException e) {
			LOG.error(e, "Unexpected error.");
		} catch (DAOException e) {
		}
		finally{
			if(answ != null)
				answ.close();
		}
		return list;
	}
	


	@Override
	public SalutableTest getTestById(int testId)
			throws IncorrectInputDataException {
		SalutableTest test = null;
		SQLPattern.Answer answ = null;
		
		String sql = String.format(
				"SELECT %s FROM Tests WHERE testId=%d",
				//	"testId, tParent, testName, minCorrectNum, testTime, attemptsPerDay, itTest",
				TestHandler.ALL_FIELDS,
				testId);	
		Throwable t = null;
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet set = answ.getResultSet();
			if(set.next()) {
				test = handler.parseRowToTest(set);
			}
		} catch (SQLException e) {
			t = e;
			LOG.error(e, "Unexpected error.");
		} catch (DAOException e) {
			t = e;
		}
		finally{
			if(answ != null)
				answ.close();
		}
		
		if(test == null) {
			if (t != null) {
				throw new IncorrectInputDataException(String.format(
						"Can't find test with id=%d", testId), t);
			} else {
				throw new IncorrectInputDataException(String.format(
						"Can't find test with id=%d", testId));
			}
		}
		return test;
	}

	/**  do AccessLevel!!!!!!!!!!!!
	 */
	@Override// do AccessLevel!!!!!!!!!!!!
	public SalutableTest getParentOf(SalutableTest test) throws AccessLevelException{
		SalutableTest owner = null;
		SQLPattern.Answer answ = null;
		
		String sql = String.format(
				"SELECT %s FROM Tests WHERE testId=%d",
				//	"testId, tParent, testName, minCorrectNum, testTime, attemptsPerDay, itTest",
				TestHandler.ALL_FIELDS,
				test.getParentId());
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet set = answ.getResultSet();
			if(set.next()) {
				owner = handler.parseRowToTest(set);
			}
		} catch (SQLException e) {
			LOG.error(e, "Unexpected error.");
		} catch (DAOException e) {
		}
		finally{
			if(answ != null)
				answ.close();
		}
		return owner;
	}

	@Override// do AccessLevel!!!!!!!!!!!!
	public SalutableTest getParentOf(int parentId) throws AccessLevelException{
		SalutableTest owner = null;
		SQLPattern.Answer answ = null;

		String sql = String.format(
				"SELECT %s FROM Tests WHERE testId=%d",
				"testId, tParent, testName, minCorrectNum, testTime, attemptsPerDay",
				parentId);
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet set = answ.getResultSet();
			if(set.next()) {
				owner = handler.parseRowToTest(set);
			}
		} catch (SQLException e) {
			LOG.error(e, "Unexcpected error.");
		} catch (DAOException e) {
		}
		finally{
			if(answ != null)
				answ.close();
		}
		return owner;
	}

	@Override
	public void passExam(Exam exam) {
		throw new RuntimeException("Fuck off, baby.");
	}


	@Override
	public TestTreeNode<SalutableTest> getTestTreeFor(User user) throws DAOException {
		return handler.getTestTreeFor(user, "getAllOpenToPassTestsForUserOrderById", LOG);
	}

	/*
	@Override
	public void addTest(Test test) throws DAOException {
		String insert = String.format("INSERT INTO Tester.Tests(%s) VALUES (%s, %d, %s, %s, %s, %s)",
				"login, groupId, userFName, userLName, role, password",
				SQLPattern.parseToSQL(user.getLogin()),
				user.getGroup().getId(),
				SQLPattern.parseToSQL(user.getFname()),
				SQLPattern.parseToSQL(user.getLname()),
				role,
				SQLPattern.parseToSQL(user.getPassword()));
		String select= String.format("SELECT userId FROM Tester.Users WHERE login = %s",
				SQLPattern.parseToSQL(user.getLogin()));

		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.next();
			user.setId(rs.getInt("userId"));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}*/


	/*
	@Deprecated
	private void setTestModeForTest(SalutableTest test, Connection cn)
			throws SQLException {
		String sql = String.format(
				"SELECT COUNT(tParent) FROM Tests WHERE tParent=%d", test.getId());
		PreparedStatement st = cn.prepareStatement(sql);
		ResultSet set = st.executeQuery();
		set.next();
		System.out.println("\t" + test.getTitle() + " is " + set.getInt(1));
		test.setTest(set.getInt(1) == 0);
	}*/


	private class SalutableTestHandler extends TestHandler<SalutableTest>{
		@Override
		protected SalutableTest createNewTest() {
			return new SalutableTest();
		}

		@Override
		protected TestTreeNode<SalutableTest> createNewNode(User user, SalutableTest test) {
			return new TestTreeNode<SalutableTest>(user, test);
		}
	}
}
