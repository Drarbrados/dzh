package tester.dao.mysql;

import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.SQLPattern;
import tester.service.Logger;
import tester.transfers.tests.Test;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by DzianisH on 29.03.2015.
 */
abstract class TestHandler <T extends Test> {
    static final String TEST_ID_FIELD = "testId";
    static final String PARENT_TEST_ID_FIELD = "tParent";
    static final String TEST_NAME_FIELD = "testName";
    static final String MIN_CORRECT_NUMBER_FIELD = "minCorrectNum";
    static final String TEST_TIME_FIELD = "testTime";
    static final String ATTEMPTS_PER_DAY_FIELD = "attemptsPerDay";
    static final String IS_IT_TEST_FIELD = "isTest";
    static final String ALL_FIELDS;

    static{
        ALL_FIELDS = String.format("%s, %s, %s, %s, %s, %s, %s",
                TEST_ID_FIELD,
                PARENT_TEST_ID_FIELD,
                TEST_NAME_FIELD,
                MIN_CORRECT_NUMBER_FIELD,
                TEST_TIME_FIELD,
                ATTEMPTS_PER_DAY_FIELD,
                IS_IT_TEST_FIELD
        );
    }

    public TestTreeNode<T> getTestTreeFor(User user, String SqlProc, Logger log) throws DAOException {

        String select = String.format("CALL %s(%d, '%s');",
                SqlProc, user.getId(), TestHandler.ALL_FIELDS);

        // search & add collection
        LinkedList<TestTreeNode<T>> list = new LinkedList<>();

        TestTreeNode<T> node, parent;

        try(SQLPattern.Answer answ = SQLPattern.sendQuery(select)){
            ResultSet res = answ.getResultSet();

            T test = null;
            if(res.getMetaData().getColumnCount() != 0) {
                TreeMap<Integer, TestTreeNode<T>> tree = new TreeMap<>();
                while (res.next()) {
                    test = parseRowToTest(res);
                    node = createNewNode(user, test);
                    node.setNodeAccessible(true);
                    node.setRoot(false);

                    parent = tree.get(test.getParentId());
                    if (parent != null) {
                        parent.addChildNode(node);
                        node.setParentNode(parent);
                    } else {
                        list.add(node);// top of user's scope
                    }
                    tree.put(test.getId(), node);

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        }

        if(list.size() == 0)
            return null;

        Iterator<TestTreeNode<T>> iter = list.iterator();
        while(iter.hasNext()) {// remove all not tops of user's access rules it.
            node = iter.next();

            if (node.getParentNode() != null) {// if it not 'temp' root
                iter.remove();
                System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA  " + node);
                if(log != null)
                    log.warn("Incorrect logic, node {%s} have parent. It haven't be happened. May be bugs..",
                        node.toString());
            }
        }
        iter = null;


        // now get all parents for tests in list
        // attaching
        node = attachAllAncestorsAndGetRoot(list, user, log);
        node.setParentNode(null);
        node.setRoot(true);
        return node;
    }


    TestTreeNode<T> attachAllAncestorsAndGetRoot(List<TestTreeNode<T>> list, User user, Logger log)
            throws DAOException {

        String select = String.format("CALL getAllAncestorsForTestsOrderById('%s', '%s')",
                splitTestIds(list), ALL_FIELDS);

        System.out.println(select);
        TestTreeNode<T> root = null;
        TreeMap<Integer, TestTreeNode<T>> tree = new TreeMap<>();
        try (SQLPattern.Answer answ = SQLPattern.sendQuery(select)) {
            ResultSet res = answ.getResultSet();

            T test = null;
            TestTreeNode<T> node, parent;
            while (res.next()) {
                test = parseRowToTest(res);
                node = createNewNode(user, test);
                node.setNodeAccessible(false);
                node.setRoot(false);

                parent = tree.get(test.getParentId());
                if (parent != null) {
                    parent.addChildNode(node);
                    node.setParentNode(parent);
                } else {// it is only root
                    //	if(root == null)// =(   it can be not only
                    if(root != null && log != null) {
                        log.error("Bad Tree of test. Already have root node: {%s}. But found new root: {%s}.",
                                root.toString(), node.toString());
                    }
                    root = node;
                }
                //	System.out.println("!!!" + node);
                tree.put(test.getId(), node);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        TestTreeNode<T> parent;
        for (TestTreeNode<T> node : list) {
            parent = tree.get(node.getNodeValue().getParentId());
            if (parent != null) {
                parent.addChildNode(node);
                node.setParentNode(parent);
            }
        }

        if (root == null)
            root = list.get(0);
        return root;
    }

    private <T extends Test> String splitTestIds(List<TestTreeNode<T>> list){
        StringBuilder sb = new StringBuilder(list.size()*3);
        for(TestTreeNode<T> node : list) {
            sb.append(node.getNodeValue().getId());
            sb.append(',');
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }


    /**
     * Do all setters for test and return it.
     * @param res
     * @return
     * @throws SQLException
     */
    T parseRowToTest(ResultSet res) throws SQLException {
        T test = createNewTest();

        test.setId(res.getInt(TEST_ID_FIELD));
        test.setAttemptsPerDay(res.getInt(ATTEMPTS_PER_DAY_FIELD));
        test.setMaxTime(res.getInt(TEST_TIME_FIELD));
        test.setMinCorrectNum(res.getInt(MIN_CORRECT_NUMBER_FIELD));
        test.setParentId(res.getInt(PARENT_TEST_ID_FIELD));
        test.setTitle(res.getString(TEST_NAME_FIELD));
        test.setTest(res.getBoolean(IS_IT_TEST_FIELD));

        return test;
    }


    abstract protected T createNewTest( );
    abstract protected TestTreeNode<T> createNewNode(User user, T test);
}
