package tester.dao.mysql;

import tester.dao.ModerableTestDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.SQLPattern;
import tester.service.Logger;
import tester.transfers.Group;
import tester.transfers.questions.StringQuestion;
import tester.transfers.tests.ModerableTest;
import tester.transfers.tests.Test;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.Admin;
import tester.transfers.users.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


/**
 *
 */
public class MySQLModerableTestDAO implements ModerableTestDAO {
	private static final Logger LOG = Logger.getLogger(MySQLModerableTestDAO.class);
	private ModerableTestHandler handler = new ModerableTestHandler();

/*
	@Override
	public ArrayList<ModerableTest> getAllTests(User user) {
		return new ArrayList<ModerableTest>();
	}

	@Override
	public ArrayList<ModerableTest> getRootTests(User user) {
		return new ArrayList<ModerableTest>();
	}

	@Override
	public ArrayList<ModerableTest> getChildrenOf(ModerableTest test) {
		return new ArrayList<ModerableTest>();
	}

	@Override
	public ModerableTest getOwnerOf(ModerableTest test)
			throws AccessLevelException {
		return test;
	}
*/

	@Override
	public void addTest(Test test) throws DAOException {
		
		String name = SQLPattern.parseToSQL(test.getTitle());
		String insert = String.format("INSERT INTO Tester.Tests(%s) VALUES (%d, %s, %d, %d, %d, %b)",
				"tParent, testName, minCorrectNum, testTime, attemptsPerDay, isTest",
				test.getParentId(),
				name,
				test.getMinCorrectNum(),
				test.getMaxTime(),
				test.getAttemptsPerDay(),
				test.isTest()
		);
	///	System.out.println(insert);
		String select= String.format(
				"SELECT testId FROM Tester.Tests WHERE testName=%s AND tParent=%d",
				name, test.getParentId());
	///	System.out.println(select);
		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.next();
			test.setId(rs.getInt("testId"));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}

	@Override
	public void update(Test test) throws DAOException {

		String name = SQLPattern.parseToSQL(test.getTitle());
		String insert = String.format("UPDATE Tester.Tests SET tParent = %d, testName = %s, " +
						"minCorrectNum = %d, testTime = %d, attemptsPerDay = %d, isTest = %b " +
						"WHERE testId = %d",
				test.getParentId(),
				name,
				test.getMinCorrectNum(),
				test.getMaxTime(),
				test.getAttemptsPerDay(),
				test.isTest(),
				test.getId()
		);
		String select= String.format(
				"SELECT testId FROM Tester.Tests WHERE testName=%s AND tParent=%d" +
						" ORDER BY testId",
				name, test.getParentId());
		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.last();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}
/*
	@Override
	public List<ModerableTest> getRootTestsFor(User user) {
		return null;
	}

	@Override
	public List<ModerableTest> getChildrenOf(ModerableTest test) {
		return null;
	}

	@Override
	public ModerableTest getParentOf(ModerableTest test) throws AccessLevelException {
		return null;
	}
*/
	@Override
	public void getTestVisibilityById(Test test) throws DAOException {
		SQLPattern.Answer answ = null;
		String select = String.format(
				"SELECT groupId FROM Tester.GroupsConnect WHERE testId=%d",
				test.getId());
		try {
			answ = SQLPattern.sendQuery(select);
			test.clearAvailableGroupSet();
			while(answ.next()) {
				int groupId = answ.getInt("groupId");
				test.addAvailableGroup(new Group(groupId));
			}
		} finally{
			if(answ != null) {
				answ.close();
			}
		}
	}

	@Override
	public TestTreeNode<ModerableTest> getTestTreeFor(User user) throws DAOException {
		return handler.getTestTreeFor(user, "getAllOpenToModerateTestsForUserOrderById", LOG);
	}


	@Override
	public void openToGroup(Test test, Group group) throws DAOException {
		String insert = String.format("INSERT INTO Tester.GroupsConnect(%s) VALUES (%d, %d)",
				"groupId, testId",
				group.getId(),
				test.getId()
		);
		SQLPattern.sendUpdate(insert);
		test.addAvailableGroup(group);
	}

	@Override
	public void closeToGroup(Test test, Group group) throws DAOException {
		String delete = String.format("DELETE FROM Tester.GroupsConnect WHERE " +
						"groupId=%d AND testId=%d",
				group.getId(),
				test.getId()
		);
		SQLPattern.sendUpdate(delete);
		test.removeAvailableGroup(group);
	}


	private class ModerableTestHandler extends TestHandler<ModerableTest>{
		@Override
		protected ModerableTest createNewTest() {
			return new ModerableTest();
		}

		@Override
		protected TestTreeNode<ModerableTest> createNewNode(User user, ModerableTest test) {
			return new TestTreeNode<ModerableTest>(user, test);
		}
	}
}
