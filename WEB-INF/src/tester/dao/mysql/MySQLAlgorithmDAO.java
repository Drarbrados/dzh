package tester.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import tester.dao.AlgorithmDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.ElementIsMissingException;
import tester.dao.mysql.service.SQLPattern;
import tester.transfers.QuestionsGroup;
import tester.transfers.algorithms.Algorithm;

public class MySQLAlgorithmDAO implements AlgorithmDAO {

	@Override
	public Algorithm getAlgorithmFor(QuestionsGroup qgroup)
			throws DAOException, ElementIsMissingException{
		String selectAlg = String.format(
				"SELECT javaTemplate FROM Algorithms WHERE algorithmId=%d;",
				qgroup.getAlgorithmId());
		
		Algorithm algorithm = null;
		try (SQLPattern.Answer answ = SQLPattern.sendQuery(selectAlg)){
			ResultSet rs = answ.getResultSet();
			rs.next();
			selectAlg = rs.getString(1);
			
			algorithm = (Algorithm) Class.forName(selectAlg).newInstance();
		} catch (SQLException | InstantiationException
				| IllegalAccessException | ClassNotFoundException
				| RuntimeException e) {
			throw new DAOException(e);
		}
		if(algorithm == null)
			throw new ElementIsMissingException("Can't find algorithm where id=" +
					qgroup.getAlgorithmId());


		initAlgorithmIn(qgroup.getId(), algorithm);// init
		
		return algorithm;
	}


	private void initAlgorithmIn(int qgroupId, Algorithm algorithm) throws DAOException{

		String selectParams = String.format(
				"SELECT paramName, paramValue FROM AlgorithmParameters WHERE qGroupId=%d;",
				qgroupId);

		// init algorithm
		try (SQLPattern.Answer answ = SQLPattern.sendQuery(selectParams)){
			ResultSet rs = answ.getResultSet();
			HashMap<String, String> map = new HashMap<String, String>();

			while(rs.next()){
				map.put(rs.getString(1), rs.getString(2));
			}

			algorithm.init(map);
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
/*
	@Override
	public Algorithm getAlgorithmById(int id)
			throws DAOException, ElementIsMissingException{
		
		String selectAlg = String.format(
				"SELECT javaTemplate FROM Algorithms WHERE algorithmId=%d",
				id );
		String selectParams = String.format(
				"SELECT * from algorithmParameters",
				id );
		
		Algorithm algorithm = null;
		try (SQLPattern.Answer answ = SQLPattern.sendQuery(selectAlg)){
			ResultSet rs = answ.getResultSet();
			rs.next();
			selectAlg = rs.getString(1);
			
			algorithm = (Algorithm) Class.forName(selectAlg).newInstance();
		} catch (SQLException | InstantiationException
				| IllegalAccessException | ClassNotFoundException
				| RuntimeException e) {
			throw new DAOException(e);
		}
		
		if(algorithm == null)
			throw new ElementIsMissingException("Can't find algorithm where id=" + id);
		
		return algorithm;
	}
*/
	/*
	@Override
	public Algorithm getAlgorithmForQuestionsGroup(int qgroupIid)
			throws DAOException, ElementIsMissingException{
		
		String selectAlg = String.format(
				"SELECT javaTemplate FROM Algorithms WHERE algorithmId=%d",
				id );
		String selectParams = String.format(
				"SELECT * from algorithmParameters",
				id );
		
		Algorithm algorithm = null;
		try (SQLPattern.Answer answ = SQLPattern.sendQuery(selectAlg)){
			ResultSet rs = answ.getResultSet();
			rs.next();
			selectAlg = rs.getString(1);
			
			algorithm = (Algorithm) Class.forName(selectAlg).newInstance();
		} catch (SQLException | InstantiationException
				| IllegalAccessException | ClassNotFoundException
				| RuntimeException e) {
			throw new DAOException(e);
		}
		
		if(algorithm == null)
			throw new ElementIsMissingException("Can't find algorithm where id=" + id);
		
		return algorithm;
	}
	*/
}
