package tester.dao.mysql;

import tester.dao.Data;
import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.Connector;

import java.sql.SQLException;
import java.sql.Statement;

public class MySQLData implements Data {

	@Override
    public void truncateDatabase() throws DAOException {

        Connector connector = new Connector();
        connector.connection();
        
        Statement st = null;
        try {
            st = connector.getStatement();
            st.addBatch("SET FOREIGN_KEY_CHECKS = 0");
            st.addBatch("TRUNCATE TABLE Tester.AlgorithmParameters");
            st.addBatch("TRUNCATE TABLE Tester.Algorithms ");
            st.addBatch("TRUNCATE TABLE Tester.Answers");
            st.addBatch("TRUNCATE TABLE Tester.Groups ");
            st.addBatch("TRUNCATE TABLE Tester.GroupsConnect");
            st.addBatch("TRUNCATE TABLE Tester.Moderators");
            //st.addBatch("TRUNCATE TABLE Tester.QuestionTypes");
            st.addBatch("TRUNCATE TABLE Tester.Questions");
            st.addBatch("TRUNCATE TABLE Tester.QuestionsConnect");
            st.addBatch("TRUNCATE TABLE Tester.QuestionsGroups");
            st.addBatch("TRUNCATE TABLE Tester.QuestionsStatistics");
            st.addBatch("TRUNCATE TABLE Tester.Tests");
            st.addBatch("TRUNCATE TABLE Tester.TestsStatistics");
            st.addBatch("TRUNCATE TABLE Tester.Users");
            st.addBatch("SET FOREIGN_KEY_CHECKS = 1");
            st.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            connector.closeStatement();
            connector.disconnection();
        }
    }
}
