package tester.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tester.dao.UserDAO;
import tester.dao.exceptions.AuthenticationException;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.UserAlreadyExistsException;
import tester.dao.mysql.service.SQLPattern;
import tester.service.Logger;
import tester.transfers.Group;
import tester.transfers.users.User;
import tester.transfers.users.Admin;

public class MySQLUserDAO implements UserDAO {
	private static final Logger LOG = Logger.getLogger(MySQLUserDAO.class);

	@Override
	public User getUser(String login, String password)
			throws AuthenticationException {
		User user = null;
		SQLPattern.Answer answ = null;
		Throwable t = null;
		
		String sql = String.format(
				"SELECT %s FROM Users, Groups WHERE Groups.groupId=Users.groupId AND Users.login=%s AND Users.password=%s;",
				"Users.userId, Groups.groupId, Groups.groupName, Groups.entrYear, Groups.gParent, Users.userFName, Users.userLName, Users.role",
				SQLPattern.parseToSQL(login),
				SQLPattern.parseToSQL(password));
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet res = answ.getResultSet();
			res.next();
			int id = res.getInt(1),
					idg = res.getInt(2);
			user = new User(id, login, password, res.getString("userFName"),
					res.getString("userLName"), new Group(idg, res.getString("groupName"),
					res.getInt("entrYear"), res.getInt("gParent")));
			if("ADMIN".equals(res.getString("role"))) {
				user = new Admin(user);
			}
		} catch (SQLException e) {
			t = e;
			LOG.error(e, "Unexcpected error.");
		} catch (DAOException e) {
			t = e;
		}
		finally{
			if(answ != null)
				answ.close();
		}
		
		if(user == null){
			AuthenticationException aut = new AuthenticationException(
					String.format("User with login='%s' & password='%s' %s",
							login, password,
							"is not found."));
			if(t != null)
				aut.addSuppressed(t);
			throw aut;
		}
		return user;
	}

	@Override
	public User getUser(String login) {
		User user = null;
		Throwable t = null;

		String sql = String.format(
				"SELECT %s FROM Users, Groups WHERE Groups.groupId=Users.groupId AND Users.login=%s;",
				"Users.userId, Groups.groupId, Groups.groupName, Groups.entrYear, Groups.gParent, " +
						"Users.userFName, Users.userLName, Users.role, Users.password",
				SQLPattern.parseToSQL(login));
		
		try(SQLPattern.Answer answ = SQLPattern.sendQuery(sql)) {
			ResultSet res = answ.getResultSet();
			res.next();
			int id = res.getInt(1),
					idg = res.getInt(2);
			user = new User(id, login, res.getString("password"), res.getString("userFName"),
					res.getString("userLName"), new Group(idg, res.getString("groupName"),
					res.getInt("entrYear"), res.getInt("gParent")));
			if("ADMIN".equals(res.getString("role"))) {
				user = new Admin(user);
			}
		} catch (SQLException e) {
			t = e;
			LOG.error(e, "Unexcpected error.");
		} catch (DAOException e) {
			t = e;
		}

		return user;
	}

	@Override
	public User getUser(int userId) {
		User user = null;
		SQLPattern.Answer answ = null;
		Throwable t = null;

		String sql = String.format(
				"SELECT %s FROM Users, Groups WHERE Groups.groupId=Users.groupId AND Users.userId=%d;",
				"Users.userId, Groups.groupId, Groups.groupName, Groups.entrYear, Groups.gParent, " +
				"Users.login, Users.userFName, Users.userLName, Users.role, Users.password",
				userId);
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet res = answ.getResultSet();
			res.next();
			int id = res.getInt("Users.userId"),
					idg = res.getInt("Groups.groupId");
			user = new User(userId, res.getString("login"), res.getString("password"), res.getString("userFName"),
					res.getString("userLName"), new Group(idg, res.getString("groupName"),
					res.getInt("entrYear"), res.getInt("gParent")));
			if("ADMIN".equals(res.getString("role"))) {
				user = new Admin(user);
			}
		} catch (SQLException e) {
			t = e;
			LOG.error(e, "Unexcpected error.");
		} catch (DAOException e) {
			t = e;
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}

		return user;
	}




	@Override
	public boolean verify(User user) {
		SQLPattern.Answer answ = null;
		boolean very = true;
		
		String sql = String.format(
				"SELECT COUNT(userId) FROM Users WHERE login=%s AND password=%s AND userId=%d AND userFName=%s AND userLName=%s AND groupId=%d;",
				SQLPattern.parseToSQL(user.getLogin()),
				SQLPattern.parseToSQL(user.getPassword()),
				user.getId(),
				SQLPattern.parseToSQL(user.getFname()),
				SQLPattern.parseToSQL(user.getLname()),
				user.getGroup().getId());
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet res = answ.getResultSet();
			
			res.next();
			very = 1 == res.getInt(1);
		} catch (SQLException e) {
			very = false;
			LOG.error(e, "Unexcpected error.");
		} catch (DAOException e) {
			very = false;
		}
		finally{
			if(answ != null)
				answ.close();
		}
		return very;
	}


	@Override
	public void addUser(User user) throws DAOException {

		String login = SQLPattern.parseToSQL(user.getLogin());
		String insert = String.format("INSERT INTO Tester.Users(%s) VALUES (%s, %d, %s, %s, %s, %s)",
				"login, groupId, userFName, userLName, role, password",
				login,
				user.getGroup().getId(),
				SQLPattern.parseToSQL(user.getFname()),
				SQLPattern.parseToSQL(user.getLname()),
				SQLPattern.parseToSQL(user.getRole()),
				SQLPattern.parseToSQL(user.getPassword()));
		String select= String.format("SELECT userId FROM Tester.Users WHERE login = %s",
				login);

		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.next();
			user.setId(rs.getInt("userId"));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}


	@Override
	public void update(User user) throws DAOException{
		String update = String.format(
				"UPDATE Tester.Users SET login=%s, password=%s, userFName=%s, " +
						"userLName=%s, role=%s WHERE userId=%d",
				SQLPattern.parseToSQL(user.getLogin()),
				SQLPattern.parseToSQL(user.getPassword()),
				SQLPattern.parseToSQL(user.getFname()),
				SQLPattern.parseToSQL(user.getLname()),
				SQLPattern.parseToSQL(user.getRole()),
				user.getId());
		SQLPattern.sendUpdate(update);
	}

	@Override
	public List<User> getUsersFromGroup(Group group) throws DAOException {

		String select = String.format("SELECT userId, login, password, " +
				"userFName, userLName, role FROM Tester.Users WHERE " +
				"groupId = %d", group.getId());
		SQLPattern.Answer answer = SQLPattern.sendQuery(select);
		ResultSet rs = answer.getResultSet();
		List<User> result = new ArrayList<User>();
		try {
			while (rs.next()) {/* трндц.. )   я хочу это развидеть*/
				User user = new User(rs.getInt("userId"), rs.getString("login"),
						rs.getString("password"), rs.getString("userFName"),
						rs.getString("userLName"), group);
				if(!"ADMIN".equals(rs.getString("role"))) {
					result.add(user);
				} else {
					result.add(new Admin(user));
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			if(answer != null) {
				answer.close();
			}
		}
		return result;
	}
}
