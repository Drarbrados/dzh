package tester.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tester.dao.AnswerDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.SQLPattern;
import tester.service.Logger;
import tester.transfers.Answer;
import tester.transfers.questions.Question;

public class MySQLAnswerDAO implements AnswerDAO {
	private static final Logger LOG = Logger.getLogger(MySQLAnswerDAO.class);
	
	@Override
	public ArrayList<Answer> getAllAnswersFor(Question question) {
		return getAllAnswersFor(question.getId());
	}
	
	@Override
	public ArrayList<Answer> getAllAnswersFor(int id) {
		ArrayList<Answer> list = new ArrayList<Answer>();
		SQLPattern.Answer answ = null;
		
		String sql = String.format(
				"SELECT %s FROM Answers WHERE Answers.questionId=%d",
				"answerId, answerText, trueCombination, numOfTrue, numOfFalse, pictureId, pictureSize",
				id);
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet res = answ.getResultSet();
			while(res.next()) {
				list.add(parseRowToAnswer(res));
			}
		} catch (SQLException e) {
			LOG.error(e, "Unexpected error.");
		} catch (DAOException e) {
		}
		finally{
			if(answ != null)
				answ.close();
		}
		return list;
	}
	
	private Answer parseRowToAnswer(ResultSet res) throws SQLException{
		Answer answ = new Answer(res.getInt(1));
		answ.setText(res.getString(2));
		answ.setComb(res.getString(3));
		answ.setTrueNumber(res.getLong(4));
		answ.setFalseNamber(res.getLong(5));
		answ.setPictureId(res.getString(6));
		answ.setPictureSize(res.getInt("pictureSize"));
		return answ;
	}

	@Override
	public void addAnswer(Answer answer) throws DAOException {
	
		String text = SQLPattern.parseToSQL(answer.getText());
		String insert = String.format("INSERT INTO Tester.Answers(%s) VALUES (%d, %s, %s, %s, %d)",
				"questionId, answerText, trueCombination, pictureId, pictureSize",
				answer.getQuestionId(),
				text,
				SQLPattern.parseToSQL(answer.getComb()),
				SQLPattern.parseToSQL(answer.getPictureId()),
				answer.getPictureSize());
		String select= String.format(
				"SELECT answerId FROM Tester.Answers WHERE questionId=%d AND answerText=%s",
				answer.getQuestionId(), text);
		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.next();
			answer.setId(rs.getInt("answerId"));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}

}
