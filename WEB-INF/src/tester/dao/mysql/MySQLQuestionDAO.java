package tester.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import tester.dao.QuestionDAO;
import tester.dao.TesterDAOFactory;
import tester.dao.UserDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.SQLPattern;
import tester.service.Logger;
import tester.transfers.Answer;
import tester.transfers.tests.ModerableTest;
import tester.transfers.QuestionsGroup;
import tester.transfers.tests.SalutableTest;
import tester.transfers.questions.Question;
import tester.transfers.tests.Test;

public class MySQLQuestionDAO implements QuestionDAO{
	private static final Logger LOG = Logger.getLogger(MySQLQuestionDAO.class);
	private static UserDAO userDAO;

	public MySQLQuestionDAO(UserDAO user) {
		this.userDAO = user;
	}

	@Override
	public ArrayList<Question> getQuestionsFor(Test test) {
		return getQuestionsForTest(test.getId());
	}


	@Override
	public ArrayList<Question> getQuestionsForTest(int testId) {
		ArrayList<Question> list = new ArrayList<Question>();
		SQLPattern.Answer answ = null;
		
		String sql = String.format(
				"SELECT %s FROM Questions, QuestionTypes WHERE Questions.testId=%d AND%s",
				"questionId, questionText, javaTemplate, numOfTrue, numOfFalse, pictureId, pictureSize, " +
				"prize, modifyDate, createDate, creatorId, modifierId, " +
				"cust_answersInRow, cust_AnswerAreaSize, cust_AnswerAreaLeftOffset",
				testId,
				" QuestionTypes.qTypeId=Questions.qTypeId");
		
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet res = answ.getResultSet();
			while(res.next()){
				Question q = parseRowToQuestion(res);
				list.add(q);
			}
		} catch (SQLException | IllegalArgumentException | IllegalAccessException
				| InstantiationException | ClassNotFoundException | SecurityException | DAOException e) {
			LOG.error(e, "Unexcpected error.");
		}
		finally{
			if(answ != null)
				answ.close();
		}
		
		return list;
	}

	public int getQuestionsNumberFor(Test test){
		int answer = -1;
		String query = "SELECT COUNT(questionId) AS count FROM Questions WHERE testId=" + test.getId();

		try(SQLPattern.Answer answ = SQLPattern.sendQuery(query)){
			answ.next();
			answer = answ.getInt("count");
		}
		catch(DAOException e){
			LOG.error(e, "Unexpected error. Using query: \"%s\"", query);
			answer = -1;
		}

		return answer;
	}
	
	

	@Override
	public ArrayList<Question> getTestQuestionsForExam(SalutableTest stest) {
		ArrayList<Question> list = getQuestionsForTest(stest.getId());

		while(list.size() > 3){// algorithm
			int x = (int) (Math.random()*list.size());
			list.remove(x);
		}
		
		return list;
	}

	private Question parseRowToQuestion(ResultSet res) throws SQLException,
		IllegalArgumentException, IllegalAccessException, InstantiationException,
		ClassNotFoundException, SecurityException {
		
		Question qw = null;

		qw = (Question) Class.forName(res.getString("javaTemplate")).newInstance();
		qw.setId(res.getInt("questionId"));
		qw.setText(res.getString("questionText"));
		qw.setTrueNumber(res.getLong("numOfTrue"));
		qw.setFalseNumber(res.getLong("numOfFalse"));
		qw.setPictureId(res.getString("pictureId"));
		qw.setPictureSize(res.getInt("pictureSize"));
		qw.setPrize(res.getInt("prize"));
		qw.getCustomization().setAnswersInRow(res.getInt("cust_AnswersInRow"));
		qw.getCustomization().setAnswerAreaSize(res.getInt("cust_AnswerAreaSize"));
		qw.getCustomization().setAnswerAreaLeftOffset(res.getInt("cust_AnswerAreaLeftOffset"));
		qw.setModifyDate(res.getString("modifyDate"));
		qw.setCreateDate(res.getString("createDate"));
		qw.setCreator(userDAO.getUser(res.getInt("creatorId")));
		qw.setModifier(userDAO.getUser(res.getInt("modifierId")));

		for(Answer answ : TesterDAOFactory.getFactory().getAnswerDAO().getAllAnswersFor(qw)) {
			qw.addAvailableAnswer(answ);
		}
//		qw.setComplete();
		return qw;
	}


	@Override
	public void addQuestion(Question question) throws DAOException {
		String text = SQLPattern.parseToSQL(question.getText());
		String pictureId = SQLPattern.parseToSQL(question.getPictureId());
		String date = SQLPattern.parseToSQL(
				new java.sql.Date(new java.util.Date().getTime()).toString());
		String insert = String.format("INSERT INTO Tester.Questions(%s) " +
						"VALUES (%d, %s, %d, %s, %d, %d, %s, %s, %d, %d, %d, %d, %d)",
				"testId, questionText, qTypeId, pictureId, pictureSize, prize, " +
				"modifyDate, createDate, modifierId, creatorId, " +
				"cust_answersInRow, cust_answerAreaSize, cust_answerAreaLeftOffset",
				question.getTest().getId(),
				text,
				question.getType(),
				pictureId,
				question.getPictureSize(),
				question.getPrize(),
				date,
				date,
				question.getCreator().getId(),
				question.getCreator().getId(),
				question.getCustomization().getAnswersInRow(),
				question.getCustomization().getAnswerAreaSize(),
				question.getCustomization().getAnswerAreaLeftOffset());
		String select= String.format(
				"SELECT questionId FROM Tester.Questions WHERE questionText=%s AND testId=%d" +
						" ORDER BY questionId",
				text, question.getTest().getId());
		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.last();
			question.setId(rs.getInt("questionId"));
			question.setModifyDate(new java.sql.Date(new java.util.Date().getTime()).toString());
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}

	public void updateQuestionData(Question question) throws DAOException {

		System.out.println("QAA: " + question.getAvailableAnswers());
		removeAnswersFromQuestion(question);

		String text = SQLPattern.parseToSQL(question.getText());
		String pictureId = SQLPattern.parseToSQL(question.getPictureId());
		String date = SQLPattern.parseToSQL(
				new java.sql.Date(new java.util.Date().getTime()).toString());
		String insert = String.format("UPDATE Tester.Questions " +
						"SET testId=%d, questionText=%s, qTypeId=%d, " +
						"pictureId=%s, pictureSize=%d, prize=%s, modifyDate=%s, createDate=%s, " +
						"modifierId=%d, creatorId=%d, cust_answersInRow=%d, " +
						"cust_answerAreaSize=%d, cust_answerAreaLeftOffset=%d WHERE " +
						"questionId=%d",
				question.getTest().getId(),
				text,
				question.getType(),
				pictureId,
				question.getPictureSize(),
				question.getPrize(),
				date,
				SQLPattern.parseToSQL(question.getCreateDate()),
				question.getModifier().getId(),
				question.getCreator().getId(),
				question.getCustomization().getAnswersInRow(),
				question.getCustomization().getAnswerAreaSize(),
				question.getCustomization().getAnswerAreaLeftOffset(),
				question.getId());
		String select= String.format(
				"SELECT questionId FROM Tester.Questions WHERE questionText=%s AND testId=%d" +
						" ORDER BY questionId",
				text, question.getTest().getId());
		SQLPattern.Answer answ = null;
		try {
			answ = SQLPattern.sendUpdateAndQuery(insert, select);
			ResultSet rs = answ.getResultSet();
			rs.last();
			question.setModifyDate(new java.sql.Date(new java.util.Date().getTime()).toString());
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		finally{
			if(answ != null) {
				answ.close();
			}
		}
	}

	public Question getQuestionById(int questionId) {
		Question question = null;
		SQLPattern.Answer answ = null;

		String sql = String.format(
				"SELECT * FROM Questions, QuestionTypes WHERE questionId=%d " +
						"AND %s",
				questionId,
				"QuestionTypes.qTypeId=Questions.qTypeId");
		Throwable t = null;
		try {
			answ = SQLPattern.sendQuery(sql);
			ResultSet set = answ.getResultSet();
			if(set.next()) {
				question = parseRowToQuestion(set);
			}
		} catch (SQLException e) {
			LOG.error(e, "Unexcpected error.");
		} catch (DAOException e) {
			LOG.error(e, "Unexcpected error.");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally{
			if(answ != null) {
				answ.close();
			}
		}
		return question;
	}

	@Override
	public ArrayList<Question> getQuestionsFor(QuestionsGroup questionsGroup){
		return getQuestionsForQuestionsGroup(questionsGroup.getId());
	}

	@Override
	public ArrayList<Question> getQuestionsForQuestionsGroup(int qgroupId){
		ArrayList<Question> list = new ArrayList<Question>();
		
		String select = String.format(
				"SELECT %s FROM Questions, QuestionTypes WHERE QuestionTypes.qTypeId=Questions.qTypeId AND Questions.questionId IN (SELECT questionId FROM QuestionsConnect WHERE qGroupId=%d)",
				"questionId, questionText, javaTemplate, numOfTrue, numOfFalse, pictureId, pictureSize, prize, " +
				"modifyDate, createDate, creatorId, modifierId, cust_answersInRow, cust_AnswerAreaSize, cust_AnswerAreaLeftOffset",
				qgroupId);
		
		System.out.println(select);
		
		try (SQLPattern.Answer answ = SQLPattern.sendQuery(select)){
			ResultSet rs = answ.getResultSet();
			while(rs.next()){
				list.add(parseRowToQuestion(rs));
			}
		} catch (SQLException | IllegalArgumentException 
				| IllegalAccessException | InstantiationException 
				| ClassNotFoundException | SecurityException | DAOException e) {
			LOG.error(e, "Unexpected error.");
		}
		
		return list;
	}
	

	
	private void removeAnswersFromQuestion(Question question) {
		for(Answer answer : question.getAvailableAnswers()) {
			String update = String.format("DELETE FROM Tester.Answers WHERE answerId=%d",
					answer.getId());
			try {
				SQLPattern.sendUpdate(update);
			} catch (DAOException e) {
				e.printStackTrace();
			}
		}
		question.removeAllAvailableAnswers();
	}

}
