package tester.dao.mysql;

import tester.dao.GroupDAO;
import tester.dao.exceptions.DAOException;
import tester.dao.mysql.service.SQLPattern;
import tester.transfers.Group;
import tester.transfers.users.Admin;
import tester.transfers.users.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLGroupDAO implements GroupDAO {

    public void addGroup(Group group) throws DAOException {

    	String name = SQLPattern.parseToSQL(group.getName());
        String insert = String.format("INSERT INTO Tester.Groups(%s) VALUES (%s, %d, %d)",
                "groupName, entrYear, gParent",
                name,
                group.getYear(),
                group.getParentGroupId());
        String select = String.format(
        		"SELECT groupId FROM Tester.Groups WHERE groupName=%s AND entrYear=%d",
        		name, group.getYear() );

        SQLPattern.Answer answ = null;
        try {
            answ = SQLPattern.sendUpdateAndQuery(insert, select);
            ResultSet rs = answ.getResultSet();
            rs.next();
            group.setId(rs.getInt("groupId"));
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        finally{
            if(answ != null) {
                answ.close();
            }
        }
    }

    public Group getGroupById(int groupId) throws DAOException {
        Group group = null;
        SQLPattern.Answer answ = null;
        String select = String.format(
                "SELECT * FROM Tester.Groups WHERE groupId=%d",
                groupId);
        try {
            answ = SQLPattern.sendQuery(select);
            answ.next();
            int parentId = answ.getInt("gParent");
            int entrYear = answ.getInt("entrYear");
            String groupName = answ.getString("groupName");
            group = new Group(groupId, groupName, entrYear, parentId);
        } finally{
            if(answ != null) {
                answ.close();
            }
        }
        return group;
    }

    public List<Group> getChildrensFromGroup(Group group) throws DAOException {

        String select = String.format("SELECT groupId, groupName, entrYear " +
                "FROM Tester.Groups WHERE gParent = %d", group.getId());
        SQLPattern.Answer answer = SQLPattern.sendQuery(select);
        List<Group> result = new ArrayList<Group>();
        try {
            while (answer.next()) {
                if(answer.getInt("groupId") != group.getId()) {
                    result.add(new Group(answer.getInt("groupId"), answer.getString("groupName"),
                            answer.getInt("entrYear"), group.getId()));
                }
            }
        } finally {
            if(answer != null) {
                answer.close();
            }
        }
        return result;
    }
}
