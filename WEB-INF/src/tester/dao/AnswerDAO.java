package tester.dao;

import java.util.ArrayList;

import tester.dao.exceptions.DAOException;
import tester.transfers.Answer;
import tester.transfers.questions.Question;

public interface AnswerDAO {
	ArrayList<Answer> getAllAnswersFor(Question question);

	ArrayList<Answer> getAllAnswersFor(int questionId);

	public void addAnswer(Answer answer) throws DAOException;
}
