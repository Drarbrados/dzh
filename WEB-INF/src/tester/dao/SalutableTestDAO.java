package tester.dao;

import java.util.ArrayList;

import tester.dao.exceptions.AccessLevelException;
import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.IncorrectInputDataException;
import tester.transfers.Exam;
import tester.transfers.tests.SalutableTest;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

/**
 * Tests for saluting
 * @author User
 *
 */
//TODO: Remove all not used methods
public interface SalutableTestDAO {
	@Deprecated
	public ArrayList<SalutableTest> getAllTests(User user);
	/**
	 * @return All heads of saluted test's tree. 
	 */
	public ArrayList<SalutableTest> getRootTests(User user);
	public ArrayList<SalutableTest> getChildrenOf(SalutableTest test);
	public ArrayList<SalutableTest> getChildrenOf(int testId);
	public SalutableTest getTestById(int testId)
			throws IncorrectInputDataException;
	
	public SalutableTest getParentOf(SalutableTest test)
			throws AccessLevelException;
	public SalutableTest getParentOf(int parentId)
			throws AccessLevelException;
	
	@Deprecated
	public void passExam(Exam exam);
	public ArrayList<SalutableTest> getRootTests(int groupId);


	public TestTreeNode<SalutableTest> getTestTreeFor(User user) throws DAOException;
	//public void addTest(Test test) throws DAOException;


//	public TestTreeNode<SalutableTest> getTestTreeFor(User user);
}
