package tester.dao;

import tester.dao.exceptions.DAOException;
import tester.dao.exceptions.ElementIsMissingException;
import tester.transfers.QuestionsGroup;
import tester.transfers.algorithms.Algorithm;

public interface AlgorithmDAO {
	public Algorithm getAlgorithmFor(QuestionsGroup qgroup)  throws DAOException,
		ElementIsMissingException;
	//public Algorithm getAlgorithmForQuestionsGroup(int qgroupIid) throws DAOException,
	//		ElementIsMissingException;
}
