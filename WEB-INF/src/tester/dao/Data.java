package tester.dao;

import tester.dao.exceptions.DAOException;

public interface Data {

    public void truncateDatabase() throws DAOException;
}
