package tester.dao;

import tester.dao.exceptions.DAOException;
import tester.transfers.Group;

import java.util.List;

public interface GroupDAO {

    public void addGroup(Group group) throws DAOException;
    public List<Group> getChildrensFromGroup(Group group) throws DAOException;
    public Group getGroupById(int groupId) throws DAOException;
}
