package tester.dao;

import tester.dao.exceptions.AccessLevelException;
import tester.dao.exceptions.DAOException;
import tester.transfers.Group;
import tester.transfers.tests.ModerableTest;
import tester.transfers.tests.Test;
import tester.transfers.tests.TestTreeNode;
import tester.transfers.users.User;

import java.util.List;

public interface ModerableTestDAO {
	/*
	@Deprecated
	public ArrayList<ModerableTest> getAllTests(User user);



	*/

	public void addTest(Test test) throws DAOException;
	public void update(Test test) throws DAOException;

	/**
	 * @return All heads of moderable test's tree.
	 */
//	public List<ModerableTest> getRootTestsFor(User user);
//	public List<ModerableTest> getChildrenOf(ModerableTest test);
//	public ModerableTest getParentOf(ModerableTest test)
//			throws AccessLevelException;


	public void getTestVisibilityById(Test test) throws DAOException;

	public void openToGroup(Test test, Group group) throws DAOException;
	public void closeToGroup(Test test, Group group) throws DAOException;

	public TestTreeNode<ModerableTest> getTestTreeFor(User user) throws DAOException;
//	public TestTreeNode<ModerableTest> getTestTreeFor(int userId) throws DAOException;
}
