USE Tester;





INSERT INTO Algorithms (javaTemplate)
VALUE ('tester.transfers.algorithms.SimpleRandomizeAlgorithm'
);

INSERT INTO Algorithms (javaTemplate)
VALUE ('tester.transfers.algorithms.GaussByDifficultyAlgorithm'
);

/*
INSERT INTO QuestionTypes (javaTemplate, description)
VALUE ('tester.transfers.questions.OneOfSeveralQuestion', '1 iz mnogih'
);
INSERT INTO QuestionTypes (javaTemplate, description)
VALUE ('tester.transfers.questions.SeveralMultipleQuestion', 'mnogie iz mnogih'
);
*/




INSERT INTO Moderators(userID, testID) VALUE (
(SELECT userID FROM Users WHERE login='root'),
(select groupID from Groups where groupName='BelSUT' and entrYear=1953)
);
INSERT INTO GroupsConnect(testID, groupID) VALUE (
(select testID from Tests where testName='BelSUT'),
(select groupID from Groups where groupName='BelSUT' and entrYear=1953)
);





INSERT INTO Moderators (userID, testID) VALUE (
(SELECT userID FROM Users WHERE login='korol' and password='1111'),
(SELECT testID FROM Tests WHERE testName='MathModel')
);

INSERT INTO Moderators (userID, testID) VALUE (
(SELECT userId FROM Users WHERE login='korol' and password='1111'),
(SELECT testId FROM Tests WHERE testName='2sem_MathModel 3')
);

INSERT INTO GroupsConnect(testID, groupID) VALUE (
(select testID from Tests where testName='2sem_MathModel'),
(select groupID from Groups where groupName='ET' and entrYear=1978)
);

INSERT INTO GroupsConnect(testID, groupID) VALUE (
(select testID from Tests where testName='History'),
(select groupID from Groups where groupName='ET' and entrYear=1978)
);

INSERT INTO GroupsConnect(testID, groupID) VALUE (
(select testID from Tests where testName='MathModel'),
(select groupID from Groups where groupName='ET' and entrYear=1978)
);

INSERT INTO GroupsConnect(testID, groupID) VALUE (
(select testID from Tests where testName='MathModel'),
(select groupID from Groups where groupName='EM1' and entrYear=2011)
);
/* ******* */


INSERT INTO QuestionsGroups(testID, algorithmId) VALUE (
(SELECT testID FROM Tests WHERE testName='2sem_MathModel 1'),
(SELECT algorithmId FROM Algorithms WHERE javaTemplate='tester.transfers.algorithms.SimpleRandomizeAlgorithm')
);

INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(1),
(1)
);

INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(1),
(2)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(1),
(3)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(1),
(4)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(1),
(5)
);

INSERT INTO AlgorithmParameters(qGroupId, paramName, paramValue) VALUE (
1,
'sample_size',
'3'
);
/* ******* */







INSERT INTO QuestionsGroups(testID, algorithmId) VALUE (
(SELECT testID FROM Tests WHERE testId='10'),
(SELECT algorithmId FROM Algorithms WHERE javaTemplate='tester.transfers.algorithms.GaussByDifficultyAlgorithm')
);

INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(2),
(1)
);

INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(2),
(2)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(2),
(3)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(2),
(4)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(2),
(5)
);

INSERT INTO AlgorithmParameters(qGroupId, paramName, paramValue) VALUE (
2,
'sample_size',
'2'
);

INSERT INTO AlgorithmParameters(qGroupId, paramName, paramValue) VALUE (
2,
'mu',
'0.7'
);

INSERT INTO AlgorithmParameters(qGroupId, paramName, paramValue) VALUE (
2,
'sigma',
'0.01'
);
/* ******* */



INSERT INTO QuestionsGroups(testID, algorithmId) VALUE (
(SELECT testID FROM Tests WHERE testId='10'),
(SELECT algorithmId FROM Algorithms WHERE javaTemplate='tester.transfers.algorithms.SimpleRandomizeAlgorithm')
);

INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(3),
(1)
);

INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(3),
(2)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(3),
(3)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(3),
(4)
);


INSERT INTO QuestionsConnect(qGroupId, questionId) VALUE (
(3),
(5)
);

INSERT INTO AlgorithmParameters(qGroupId, paramName, paramValue) VALUE (
3,
'sample_size',
'2'
);
/* ******* */

