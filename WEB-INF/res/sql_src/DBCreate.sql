DELIMITER ;
DROP DATABASE IF EXISTS Tester;
CREATE database Tester;
USE Tester;


DELIMITER //

/* Группы студентов*/
CREATE TABLE Groups(
groupId INT NOT NULL AUTO_INCREMENT,
groupName VARCHAR (7) NOT NULL,
entrYear INT NOT NULL,
gParent INT NOT NULL,

PRIMARY KEY (groupId),
UNIQUE (groupName, entrYear)
) COMMENT = 'Groups of students.' DEFAULT CHARACTER SET utf8;
//

/* Пользователи*/
CREATE TABLE Users(
userId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
login VARCHAR (32) NOT NULL UNIQUE,
groupId INT NOT NULL,
userFName VARCHAR (32) NOT NULL,
userLName VARCHAR (32) NOT NULL,
password VARCHAR (32) NOT NULL,
role ENUM ('USER', 'ADMIN') DEFAULT 'USER',

FOREIGN KEY (groupId) REFERENCES Groups (groupId)
) COMMENT = 'All users,' DEFAULT CHARACTER SET utf8;
//




/* Список тестов и разделов для тестов*/
CREATE TABLE Tests(
testId INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
tParent INT NOT NULL,
testName VARCHAR (63) NOT NULL,
minCorrectNum INT NOT NULL DEFAULT 0,
testTime INT NOT NULL DEFAULT 0,
attemptsPerDay INT NOT NULL DEFAULT 1,
numOfTrue TINYINT UNSIGNED NOT NULL DEFAULT 0,/* 0:255 */
numOfFalse TINYINT UNSIGNED NOT NULL DEFAULT 0,
isTest BOOLEAN NOT NULL DEFAULT TRUE

/*UNIQUE (testName, tParent)*/
) COMMENT = 'All testStat and tests topics.' DEFAULT CHARACTER SET utf8;
//

/* Открывает группам пользователей разделы тестов для прохождения*/
CREATE TABLE GroupsConnect(
groupId INT NOT NULL,
testId INT NOT NULL,

PRIMARY KEY (groupId, testId),
FOREIGN KEY (groupId) REFERENCES Groups (groupId),
FOREIGN KEY (testId)  REFERENCES Tests (testId)
) COMMENT = 'Open tests topics for groups of users.';
//

/* Даёт права модератора юзеру в разделе тестов.*/
CREATE TABLE Moderators(
userId INT NOT NULL,
testId INT NOT NULL,

PRIMARY KEY (userId, testId),
FOREIGN KEY (userId) REFERENCES Users (userId),
FOREIGN KEY (testId) REFERENCES Tests (testId)
) COMMENT = 'User is moder in this testStat & tests childs';

/* Алгоритмы выборки вопросов для теста */
CREATE TABLE Algorithms(
algorithmId SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
javaTemplate VARCHAR(127) NOT NULL UNIQUE
) COMMENT = 'Algorithms of selecting ' DEFAULT CHARACTER SET utf8;
//

/* Списки вопросов для одной из групп вопросов.*/
CREATE TABLE QuestionsGroups(
qGroupId INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
testId INT NOT NULL,
algorithmId SMALLINT UNSIGNED NOT NULL,

FOREIGN KEY (testId) REFERENCES Tests (testId),
FOREIGN KEY (algorithmId) REFERENCES Algorithms (algorithmId)
) COMMENT = 'List of questions (with some algorithm) for real testStat.';
//

/* Параметры, передаваемые алгоритму выборки */
CREATE TABLE AlgorithmParameters(
qGroupId INT NOT NULL,
paramName VARCHAR(63) NOT NULL,
paramValue VARCHAR(255),

PRIMARY KEY (qGroupId, paramName),
FOREIGN KEY (qGroupId) REFERENCES QuestionsGroups (qGroupId)
) COMMENT = 'Parameters for algorithm in some list of questions.' DEFAULT CHARACTER SET utf8;
//

/* Описание каждого типа вопроса */
CREATE TABLE QuestionTypes(
qTypeId TINYINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
javaTemplate VARCHAR(127) NOT NULL UNIQUE,
description VARCHAR(127) /* Remove me */
) COMMENT = 'List of types of questions.' DEFAULT CHARACTER SET utf8;
//


/* Общий список вопросов*/
CREATE TABLE Questions(
questionId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
testId INT NOT NULL,
qTypeId TINYINT NOT NULL,
questionText VARCHAR(255) NOT NULL,
pictureId VARCHAR(127) DEFAULT NULL,
pictureSize INT DEFAULT 50,
prize INT NOT NULL DEFAULT 1, /* Popugai za vopros */
numOfTrue BIGINT NOT NULL DEFAULT 0, /*Statistics1*/
numOfFalse BIGINT NOT NULL DEFAULT 0, /*Statistics2*/
cust_answersInRow INT,
cust_answerAreaSize INT,
cust_answerAreaLeftOffset INT,
modifyDate VARCHAR(15) NOT NULL,
createDate VARCHAR(15) NOT NULL,
creatorId INT NOT NULL,
modifierId INT NOT NULL,
FOREIGN KEY (testId) REFERENCES Tests (testId),
FOREIGN KEY (qTypeId) REFERENCES QuestionTypes (qTypeId)
) COMMENT = 'All questions for all topics.' DEFAULT CHARACTER SET utf8;
//

/* Сопоставляет вопросы группа вопросов.*/
CREATE TABLE QuestionsConnect(
questionId INT NOT NULL,
qGroupId INT NOT NULL,

PRIMARY KEY (questionId, qGroupId),
FOREIGN KEY (questionId) REFERENCES Questions (questionId),
FOREIGN KEY (qGroupId)   REFERENCES QuestionsGroups (qGroupId)
) COMMENT = 'Connect questions and groups of questions.';
//

/* Общий список ответов*/
CREATE TABLE Answers(
answerId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
questionId INT NOT NULL,
answerText VARCHAR (255) NOT NULL,
trueCombination VARCHAR (511) NOT NULL,/*Some strange thing to get is true/false answer*/
numOfTrue BIGINT NULL DEFAULT 0, /*Statistics*/
numOfFalse BIGINT NULL DEFAULT 0,
pictureId VARCHAR(127) DEFAULT NULL,
pictureSize INT DEFAULT 50,

FOREIGN KEY (questionId) REFERENCES Questions (questionId)
) COMMENT = 'All answers.' DEFAULT CHARACTER SET utf8;
//



/* Статистика по тестам*/
CREATE TABLE TestsStatistics(
statisticsId BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
userId INT NOT NULL,
testId INT NOT NULL,
dayD DATE NOT NULL, /* CURDATE() */
timeT TIME NOT NULL, /* CURTIME() */
isTraining VARCHAR(7) NOT NULL, /*TODO: MAKE ME BOOLEAN!!! */

FOREIGN KEY (userId) REFERENCES Users (userId),
FOREIGN KEY (testId) REFERENCES Tests (testId)
) COMMENT = 'Test_s statistics.';
//

/* Статистика по вопросам*/
CREATE TABLE QuestionsStatistics(
statisticsId BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
questionId INT NOT NULL,
testStatisticsId BIGINT NOT NULL,
result INT NOT NULL,

FOREIGN KEY (questionId) REFERENCES Questions (questionId),
FOREIGN KEY (testStatisticsId) REFERENCES TestsStatistics (statisticsId)
) COMMENT = 'Question_s statistics.';
//

DROP VIEW IF EXISTS TestsStatisticsView;
CREATE VIEW TestsStatisticsView AS
SELECT *,(SELECT SUM(result) FROM QuestionsStatistics WHERE QuestionsStatistics.testStatisticsId=TestsStatistics.statisticsId) AS result
FROM TestsStatistics;
//










/*CREATE VIEW GroupsView AS SELECT groupId, groupName, gParent, entrYear FROM Groups;*/
/* TODO: Remove me, please ...   */
DROP PROCEDURE IF EXISTS getTestModers//

CREATE PROCEDURE getTestModers(grId INT)
COMMENT 'Return test_s owners.'
begin
DECLARE currId INT DEFAULT grId;
DECLARE oldId INT DEFAULT NULL;
REPEAT
SET oldId = currId;
SELECT Users.userId, Users.userName, Tests.testId, Tests.testName
FROM Moderators, Tests, Users WHERE Moderators.testId=Tests.testId
AND Tests.testId=currId AND Moderators.userId=Users.userId;
SET currId = (SELECT tParent FROM Tests WHERE testId=currId);
UNTIL oldId = currId OR currId IS NULL
END REPEAT;
end 
//

DROP PROCEDURE IF EXISTS getGroupParentsAndThis//
/*not work*/
CREATE PROCEDURE getGroupParentsAndThis(grId INT)
COMMENT 'Return this group Id and Id of all it parents.'
begin
DECLARE currId INT DEFAULT grId;
DECLARE oldId INT DEFAULT NULL;
REPEAT
SET oldId = currId;
SELECT currId;
SET currId = (SELECT tParent FROM Tests WHERE testId=currId);
UNTIL oldId = currId OR currId IS NULL
END REPEAT;
end 
//






/* Work! */
DELIMITER //
DROP PROCEDURE IF EXISTS getThisAndAllSubgroupsOrderById //
CREATE PROCEDURE getThisAndAllSubgroupsOrderById(IN base INT, IN vals TEXT)
BEGIN
DECLARE ids LONGTEXT DEFAULT '';

SET @parents = base;
SET ids = base;

loop1: LOOP
SET @stm = CONCAT(
'SELECT GROUP_CONCAT(groupId) INTO @parents FROM Groups',
' WHERE gParent IN (', @parents, ') AND groupId != 1'
);

PREPARE fetch_childs FROM @stm;
EXECUTE fetch_childs;
DROP PREPARE fetch_childs;

IF @parents IS NULL THEN
LEAVE loop1;
END IF;

SET ids = CONCAT(ids, ',', @parents);
END LOOP;

SET @stm = CONCAT('SELECT ', vals,' FROM Groups WHERE groupId IN (', ids, ') ORDER BY gParent, groupId;');

PREPARE fetch_all FROM @stm;
EXECUTE fetch_all;
DEALLOCATE  PREPARE fetch_all;
END;
//

DELIMITER ;
/*
call getThisAndAllSubgroupsOrderById(1, '*');
call getThisAndAllSubgroupsOrderById(2, '*');
call getThisAndAllSubgroupsOrderById(2, 'groupId, groupName');
*/

/* Work! */
DELIMITER //
DROP PROCEDURE IF EXISTS getThisAndAllSubtestsOrderById //
CREATE PROCEDURE getThisAndAllSubtestsOrderById(IN base TEXT, IN vals TEXT)
BEGIN
DECLARE ids LONGTEXT DEFAULT '';

SET @parents = base;
SET ids = base;

loop1: LOOP
SET @stm = CONCAT(
'SELECT DISTINCT GROUP_CONCAT(testId) INTO @parents FROM Tests',
' WHERE tParent IN (', @parents, ') AND testId != 1'
);

PREPARE fetch_childs FROM @stm;
EXECUTE fetch_childs;
DROP PREPARE fetch_childs;

IF @parents IS NULL THEN
LEAVE loop1;
END IF;

SET ids = CONCAT(ids, ',', @parents);
END LOOP;

SET @stm = CONCAT('SELECT ', vals,' FROM Tests WHERE testId IN (', ids, ') ORDER BY tParent, testId;');

PREPARE fetch_all FROM @stm;
EXECUTE fetch_all;
DEALLOCATE PREPARE fetch_all;
END;
//

DELIMITER ;
/*
call getThisAndAllSubtestsOrderById(1, '*');
call getThisAndAllSubtestsOrderById(2, '*');
call getThisAndAllSubtestsOrderById(2, 'testId, testName');
*/

/* Work! */
DELIMITER //
DROP PROCEDURE IF EXISTS getAllOpenToPassTestsForUserOrderById //
CREATE PROCEDURE getAllOpenToPassTestsForUserOrderById(IN usrId INT, IN vals TEXT)
BEGIN
DECLARE openTests LONGTEXT DEFAULT '';
SELECT GROUP_CONCAT(GroupsConnect.testId) INTO openTests
FROM GroupsConnect WHERE groupId = (SELECT groupId FROM Users WHERE userId = usrId);
IF openTests IS NOT NULL THEN
CALL getThisAndAllSubtestsOrderById(openTests, vals);
END IF;
END;
//

DELIMITER ;

call getAllOpenToPassTestsForUserOrderById(2, '*'); /* If it throw exception -> it not work. */
call getAllOpenToPassTestsForUserOrderById(1, '*');

/* Work! */
DELIMITER //
DROP PROCEDURE IF EXISTS getAllOpenToModerateTestsForUserOrderById //
CREATE PROCEDURE getAllOpenToModerateTestsForUserOrderById(IN usrId INT, IN vals TEXT)
BEGIN
DECLARE modTests TEXT DEFAULT '';
SELECT GROUP_CONCAT(Moderators.testId) INTO modTests FROM Moderators
WHERE userId = usrId;

IF modTests IS NOT NULL THEN
CALL getThisAndAllSubtestsOrderById(modTests, vals);
END IF;
END;
//

DELIMITER ;

call getAllOpenToModerateTestsForUserOrderById(2, '*'); /* If it throw exception -> it not work. */
call getAllOpenToModerateTestsForUserOrderById(1, '*');








SELECT DISTINCT Par.testId FROM Tests AS Cur, Tests AS Par WHERE Cur.tParent=Par.testId AND Cur.testId IN (5, 6) AND Cur.tParent != Cur.testId;

DELIMITER //
DROP PROCEDURE IF EXISTS getAllAncestorsForTestsOrderById //
CREATE PROCEDURE getAllAncestorsForTestsOrderById(IN base LONGTEXT, IN vals TEXT) /*, IN TEXT notBase)*/
BEGIN
DECLARE ids LONGTEXT DEFAULT '';

SET @parents = base;
SET ids = base;

loop1: LOOP
SET @stm = CONCAT(
'SELECT DISTINCT GROUP_CONCAT(Par.testId) INTO @parents FROM Tests AS Cur, Tests AS Par WHERE Cur.tParent=Par.testId',
' AND Cur.testId IN (', @parents, ') AND Cur.tParent != Cur.testId'
);

PREPARE fetch_parents FROM @stm;
EXECUTE fetch_parents;
DROP PREPARE fetch_parents;

IF @parents IS NULL THEN
LEAVE loop1;
END IF;

SET ids = CONCAT(ids, ',', @parents);
END LOOP;

/*	
SET @stm = CONCAT('SELECT ', ids);   Есть повторяющиеся симоволы
не должны быть, с при текущеё логике Java
*/
SET @stm = CONCAT('SELECT ', vals,' FROM Tests WHERE testId IN (', ids, ') AND testId NOT IN (', base,') ORDER BY tParent, testId;');

PREPARE fetch_all FROM @stm;
EXECUTE fetch_all;
DEALLOCATE PREPARE fetch_all;
END;
//

DELIMITER ;

call getAllAncestorsForTestsOrderById('1, 5, 6', '*');









show tables;
describe Groups;
describe Users;
describe Tests;
describe GroupsConnect;
describe Moderators;
describe QuestionsGroups;
describe Questions;
describe QuestionsConnect;
describe Answers;
describe Algorithms;
describe AlgorithmParameters;
describe QuestionTypes;
describe TestsStatistics;
describe QuestionsStatistics;


INSERT INTO QuestionTypes (javaTemplate, description)
VALUE ('tester.transfers.questions.OneOfSeveralQuestion', '1 iz mnogih'
);
INSERT INTO QuestionTypes (javaTemplate, description)
VALUE ('tester.transfers.questions.SeveralMultipleQuestion', 'mnogie iz mnogih'
);
INSERT INTO QuestionTypes (javaTemplate, description)
VALUE ('tester.transfers.questions.StringQuestion', 'string question'
);
INSERT INTO QuestionTypes (javaTemplate, description)
VALUE ('tester.transfers.questions.CorrelateQuestion', 'sootnoshenie'
);
